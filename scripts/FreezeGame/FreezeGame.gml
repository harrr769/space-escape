

draw_set_color(c_black);
draw_sprite_stretched(spr_tint,0,0,0,gui_width,gui_height);
with(obj_gun) canfire = false;
with(obj_enemy)
{
	hsp=0;
	vsp = 0;
	grv = 0;
}

with(obj_egun) countdown = 99999999; 
with(obj_ebullet) speed = 0;
vsp = 0;
grv = 0;
hascontrol = false;