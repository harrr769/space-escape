///@desc Enemy takes damage when hit

with (other)
{
	hp = hp - 1;
	flash = 3;
	hitfrom = other.direction;
}

instance_destroy();