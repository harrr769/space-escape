{
    "id": "eac2dda9-6416-40ce-93c8-adde9fbe7b2b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "3b7fa8e2-b084-4e54-b5ea-faea00a4c328",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "eac2dda9-6416-40ce-93c8-adde9fbe7b2b"
        },
        {
            "id": "7ca00733-d5f5-4642-95b2-265ef3838ca1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "eac2dda9-6416-40ce-93c8-adde9fbe7b2b"
        },
        {
            "id": "c6eb33b8-4f03-49f9-b458-9e2cec9e6388",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8395e1a5-8226-482b-bf17-c3de8ba54770",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "eac2dda9-6416-40ce-93c8-adde9fbe7b2b"
        },
        {
            "id": "0beb75d1-e887-4641-9fa1-59c312580e20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "eac2dda9-6416-40ce-93c8-adde9fbe7b2b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85a80af4-c801-447f-a2fb-6e11f747da75",
    "visible": true
}