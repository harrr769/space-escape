{
    "id": "e0e498ce-bdbc-471e-b550-1e5089aec076",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ebullet",
    "eventList": [
        {
            "id": "8afa23b8-6759-44a6-8689-b981a30dc463",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e0e498ce-bdbc-471e-b550-1e5089aec076"
        },
        {
            "id": "71f59ba8-4776-46d1-b594-45510e2fd16f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e0e498ce-bdbc-471e-b550-1e5089aec076"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "25b8156b-2ee3-47c8-8ae6-8deaa3f5e671",
    "visible": true
}