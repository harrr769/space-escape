/// @description Spawn player smoothly
obj_player.x = obj_spawn.x;
obj_player.y = obj_spawn.y;
with(obj_player)
{	
	hascontrol = true;
	endlevel = false;
}