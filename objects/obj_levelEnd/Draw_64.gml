/// @description Need key message, game over

//Need Key Message
if place_meeting(x,y,obj_player)
{
	with(obj_player)
	{
		if haskey = false
		{
			draw_set_valign(fa_center);
			draw_set_halign(fa_center);
			draw_set_font(fKey);
			draw_set_alpha(0.5);
			draw_set_color(c_yellow);
			draw_rectangle(gui_width/2 - 202,gui_height/2-27,gui_width/2+202,gui_height/2+27,false);
			draw_set_color(c_black);
			draw_rectangle(gui_width/2 - 200,gui_height/2-25,gui_width/2+200,gui_height/2+25,false);

			draw_set_color(c_white);
			draw_set_alpha(1);
			draw_text(gui_width/2,gui_height/2,"You need a key to go through here.");
		}
	}
}

//Check to see if game is over
