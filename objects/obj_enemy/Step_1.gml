///@desc Enemy death
if (hp <= 0)
{
	with (obj_player) score += 100; 
	with(instance_create_layer(x,y,layer,obj_dead))
	{
		direction = other.hitfrom;
		hsp = lengthdir_x(3,direction);
		vsp = lengthdir_y(3,direction)-2;
		if (sign(hsp) != 0) image_xscale = sign(hsp);
	}
	with (mygun) instance_destroy();
	instance_destroy();
}