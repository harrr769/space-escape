hsp = 0;
vsp = 0;
grv = 0.2;
walksp = 4;
hascontrol = true;

haskey = false;
maxjumps = 2;
jumps = 0;

hp = 100;
life = 3;

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();

pause = false;
canpause = true;

endlevel = false;
score = 0;

bonus_score = 4000;

total_score = 0;
endgame = false;
