/// @description Time Bonus
if pause = false && life > 0 && endlevel = false && bonus_score > 0
{
	bonus_score -=1;
	total_score = score + bonus_score;
}