/// @description Pause, Game over, Healthbar, Life counter
//Healthbar border
draw_set_color(c_black);
draw_rectangle(7,7,303,53,false);
//Healthbar
healthbar = draw_healthbar(10,10,300,50,hp,c_black,c_red,c_green,0,true,true);

//Life counter
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_set_font(fMenu);
var offset = 3;
var text = "Lives: ";
draw_set_color(c_black);
draw_text(gui_width/2-100 - offset,30,text);
draw_text(gui_width/2-100 + offset,30,text);
draw_text(gui_width/2-100,30 - offset,text);
draw_text(gui_width/2-100,30 + offset,text);
draw_text_color(gui_width/2-100,30,text,c_white,c_white,c_blue,c_blue,1);


if life = 3
{
	var text = life;
	draw_set_color(c_black);
	draw_text(gui_width/2-20 - offset,30,text);
	draw_text(gui_width/2-20 + offset,30,text);
	draw_text(gui_width/2-20,30 - offset,text);
	draw_text(gui_width/2-20,30 + offset,text);
	draw_text_color(gui_width/2-20,30,life,c_green,c_green,c_white,c_white,1);
}
if life = 2
{
	var text = life;
	draw_set_color(c_black);
	draw_text(gui_width/2-20 - offset,30,text);
	draw_text(gui_width/2-20 + offset,30,text);
	draw_text(gui_width/2-20,30 - offset,text);
	draw_text(gui_width/2-20,30 + offset,text);
	draw_text_color(gui_width/2-20,30,life,c_yellow,c_yellow,c_white,c_white,1);
}
if life = 1
{
	var text = life;
	draw_set_color(c_black);
	draw_text(gui_width/2-20 - offset,30,text);
	draw_text(gui_width/2-20 + offset,30,text);
	draw_text(gui_width/2-20,30 - offset,text);
	draw_text(gui_width/2-20,30 + offset,text);
	draw_text_color(gui_width/2-20,30,life,c_red,c_red,c_white,c_white,1);
}
if life = 0
{
	var text = life;
	draw_set_color(c_black);
	draw_text(gui_width/2-20 - offset,30,text);
	draw_text(gui_width/2-20 + offset,30,text);
	draw_text(gui_width/2-20,30 - offset,text);
	draw_text(gui_width/2-20,30 + offset,text);
	draw_text_color(gui_width/2-20,30,life,c_red,c_red,c_white,c_white,1);
}

//Score
draw_set_halign(fa_right);
draw_set_valign(fa_center);
draw_set_font(fMenu);
var text = "Score: " + string(score);
draw_set_color(c_black);
draw_text(gui_width-10 - offset,30,text);
draw_text(gui_width-10 + offset,30,text);
draw_text(gui_width-10,30 - offset,text);
draw_text(gui_width-10,30 + offset,text);
draw_text_color(gui_width-10,30,text,c_gray,c_gray,c_white,c_white,1);

//Pausing the game
if keyboard_check_pressed(vk_escape) && pause = false
{
	//This line is to maintain jump speed when game is unpaused
	pvsp = vsp;
	//This line maintains enemy speed when unpaused
	with(obj_enemy)
	{
		ehsp = hsp;
		evsp = vsp;
		egrv = grv;
	}
	pause = true;
}

if pause = true && canpause = true
{
	FreezeGame();
	audio_pause_sound(s_music);
	
	draw_set_alpha(0.5);
	draw_set_color(c_yellow);
	draw_rectangle(gui_width/2 - 402,gui_height/2-222,gui_width/2+402,gui_height/2+72,false);
	draw_set_color(c_black);
	draw_rectangle(gui_width/2 - 400,gui_height/2-220,gui_width/2+400,gui_height/2+70,false);
	
	draw_set_alpha(1);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_font(fCaption);
	var offset = 2;
	
	line1 = "PAUSE";
	draw_set_color(c_black);
	draw_text(gui_width/2 - offset,gui_height/2-200,line1);
	draw_text(gui_width/2 + offset,gui_height/2-200,line1);
	draw_text(gui_width/2,gui_height/2-200 - offset,line1);
	draw_text(gui_width/2,gui_height/2-200 + offset,line1);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2-200,line1);
	
	line2 = "Press Enter to continue";
	draw_set_color(c_black);
	draw_text(gui_width/2 - offset,gui_height/2,line2);
	draw_text(gui_width/2 + offset,gui_height/2,line2);
	draw_text(gui_width/2,gui_height/2 - offset,line2);
	draw_text(gui_width/2,gui_height/2 + offset,line2);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2,line2);
	
	line3 = "Press X to return to the main menu";
	draw_set_color(c_black);
	draw_text(gui_width/2 - offset,gui_height/2+50,line3);
	draw_text(gui_width/2 + offset,gui_height/2+50,line3);
	draw_text(gui_width/2,gui_height/2+50 - offset,line3);
	draw_text(gui_width/2,gui_height/2+50 + offset,line3);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2+50,line3);
	
	if keyboard_check_pressed(vk_enter)
	{
		audio_resume_sound(s_music);
		pause = false
		with(obj_gun) canfire = true;
		with(obj_enemy)
		{
			hsp = ehsp;
			vsp = evsp;
			grv = egrv;
		}
		with(obj_egun) countdown = countdownrate; 
		with(obj_ebullet) speed = 5;
		vsp = pvsp;
		grv = 0.2;
		hascontrol = true;
	}
	if keyboard_check_pressed(ord("X"))
	{
		SlideTransition(TRANS_MODE.RESTART);
	}
}

//Game over
if life = 0
{
	canpause = false;
	FreezeGame();
	draw_healthbar(10,10,300,50,0,c_black,c_red,c_green,0,true,true);
	
	draw_set_alpha(0.5);
	draw_set_color(c_yellow);
	draw_rectangle(gui_width/2 - 402,gui_height/2-152,gui_width/2+402,gui_height/2+82,false);
	draw_set_color(c_black);
	draw_rectangle(gui_width/2 - 400,gui_height/2-150,gui_width/2+400,gui_height/2+80,false);
	
	draw_set_alpha(1);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_font(fMenu);
	var offset = 2;
	
	line4 = "You died.";
	draw_set_color(c_black);
	draw_text(gui_width/2 - offset,gui_height/2-100,line4);
	draw_text(gui_width/2 + offset,gui_height/2-100,line4);
	draw_text(gui_width/2,gui_height/2-100 - offset,line4);
	draw_text(gui_width/2,gui_height/2-100 + offset,line4);
	draw_set_color(c_red);
	draw_text(gui_width/2,gui_height/2-100,line4);
	
	line5 = "Your score was:";
	draw_set_color(c_black);
	draw_text(gui_width/2 - offset,gui_height/2-50,line5);
	draw_text(gui_width/2 + offset,gui_height/2-50,line5);
	draw_text(gui_width/2,gui_height/2-50 - offset,line5);
	draw_text(gui_width/2,gui_height/2-50 + offset,line5);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2-50,line5);
	
	line6 = "Press X to return to the main menu";
	draw_set_color(c_black);
	draw_text(gui_width/2 - offset,gui_height/2+50,line6);
	draw_text(gui_width/2 + offset,gui_height/2+50,line6);
	draw_text(gui_width/2,gui_height/2+50 - offset,line6);
	draw_text(gui_width/2,gui_height/2+50 + offset,line6);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2+50,line6);
	
	//draw_text(gui_width/2,gui_height/2 -100, "You died.");
	//draw_set_color(c_white);
	//draw_text(gui_width/2,gui_height/2 -50,"Your score was:");
	//draw_text(gui_width/2,gui_height/2 + 50,"Press X to return to the main menu");
	
	draw_set_halign(fa_left);
	line7 = string(score);
	draw_set_color(c_black);
	draw_text(gui_width/2+185 - offset,gui_height/2-50,line7);
	draw_text(gui_width/2+185 + offset,gui_height/2-50,line7);
	draw_text(gui_width/2+185,gui_height/2-50 - offset,line7);
	draw_text(gui_width/2+185,gui_height/2-50 + offset,line7);
	draw_set_color(c_yellow);
	draw_text(gui_width/2+185,gui_height/2-50,line7);
	
	//draw_set_halign(fa_left);
	//draw_set_color(c_yellow)
	//draw_text(gui_width/2+185,gui_height/2 - 50, string(score));
	
	if keyboard_check_pressed(ord("X"))
	{
		SlideTransition(TRANS_MODE.RESTART);
	}
}

//Game complete screen
if endgame = true
{	
	draw_set_alpha(0.5);
	draw_set_color(c_yellow);
	draw_rectangle(gui_width/2 - 453,gui_height/2-323,gui_width/2+453,gui_height/2+323,false);
	draw_set_color(c_black);
	draw_rectangle(gui_width/2 - 450,gui_height/2-320,gui_width/2+450,gui_height/2+320,false);

		
	draw_set_alpha(1);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_color(c_white);
	draw_set_font(fMenu);
	var offset = 3;
	
	draw_set_color(c_black);
	text1 = "CONGRATULATIONS!";
	draw_text(gui_width/2 - offset,gui_height/2-290,text1);
	draw_text(gui_width/2 + offset,gui_height/2-290,text1);
	draw_text(gui_width/2,gui_height/2-290 - offset,text1);
	draw_text(gui_width/2,gui_height/2-290 + offset,text1);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2-290,text1);
	
	draw_set_font(fCaption);
	draw_set_color(c_black);
	var offset = 2;
	text2 = "You've completed my game";
	draw_text(gui_width/2 - offset,gui_height/2-200,text2);
	draw_text(gui_width/2 + offset,gui_height/2-200,text2);
	draw_text(gui_width/2,gui_height/2-200 - offset,text2);
	draw_text(gui_width/2,gui_height/2-200 + offset,text2);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2-200,text2);
	
	draw_set_color(c_black);
	text3 = "Thank you for playing!";
	draw_text(gui_width/2 - offset,gui_height/2-140,text3);
	draw_text(gui_width/2 + offset,gui_height/2-140,text3);
	draw_text(gui_width/2,gui_height/2-140- offset,text3);
	draw_text(gui_width/2,gui_height/2-140 + offset,text3);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2-140,text3);
	
	draw_set_color(c_black);
	text4 = "Press X to return to the main menu";
	draw_text(gui_width/2 - offset,gui_height/2-80,text4);
	draw_text(gui_width/2 + offset,gui_height/2-80,text4);
	draw_text(gui_width/2,gui_height/2-80 - offset,text4);
	draw_text(gui_width/2,gui_height/2-80 + offset,text4);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2-80,text4);
	
	draw_set_color(c_black);
	text5 = "Your score: " + string(score);
	draw_text(gui_width/2 - offset,gui_height/2+50,text5);
	draw_text(gui_width/2 + offset,gui_height/2+50,text5);
	draw_text(gui_width/2,gui_height/2+50 - offset,text5);
	draw_text(gui_width/2,gui_height/2+50 + offset,text5);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2+50,text5);
	
	draw_set_color(c_black);
	text6 = "Time bonus: " + string(bonus_score);
	draw_text(gui_width/2 - offset,gui_height/2+110,text6);
	draw_text(gui_width/2 + offset,gui_height/2+110,text6);
	draw_text(gui_width/2,gui_height/2+110 - offset,text6);
	draw_text(gui_width/2,gui_height/2+110 + offset,text6);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2+110,text6);
	
	draw_set_font(fMenu);	
	draw_set_color(c_black);
	var offset = 3;
	text7 = "Total score: " + string(total_score);
	draw_text(gui_width/2 - offset,gui_height/2+230,text7);
	draw_text(gui_width/2 + offset,gui_height/2+230,text7);
	draw_text(gui_width/2,gui_height/2+230 - offset,text7);
	draw_text(gui_width/2,gui_height/2+230 + offset,text7);
	draw_set_color(c_white);
	draw_text(gui_width/2,gui_height/2+230,text7);
	
	if keyboard_check_pressed(ord("X"))
	{
		SlideTransition(TRANS_MODE.RESTART);
	}
}
