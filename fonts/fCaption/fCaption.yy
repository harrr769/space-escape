{
    "id": "1338f718-8d7d-4f7b-8f8b-b0d5e59d92d7",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fCaption",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Droid Sans Mono",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8d05e925-ef21-455f-b0ab-a125844c84e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1a1633c5-8b2e-4ee2-a262-cfe83f03e767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 42,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 147,
                "y": 90
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "403d3d97-9471-4c35-95da-cfa2e036610b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 42,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 134,
                "y": 90
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3aa76e30-718a-4ec0-a0b2-85188ec9ba38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 113,
                "y": 90
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a9782251-7b6a-4c18-bf5b-ab11dabea878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 96,
                "y": 90
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "83b685b5-5320-4792-90d1-d54fcb4a9893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 74,
                "y": 90
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "78c8ead4-dd24-40af-b60f-b636e17b83c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 52,
                "y": 90
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4d74370b-6406-4022-a2bc-3c513c40fbb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 42,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 45,
                "y": 90
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "24f29d83-414c-4abb-8602-542412d2b82e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 42,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 32,
                "y": 90
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b74be14c-6ef2-478e-b15c-8a63a6be2afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 42,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 19,
                "y": 90
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7ae66af7-7176-4bd0-b2c9-aedf754b3533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 154,
                "y": 90
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6d0d973b-d7d6-411c-90d4-2aa5c919daf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 90
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6ce91c25-2e55-4716-af01-4252c02be91a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 42,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 475,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "beb33800-7f05-4c18-8de7-8f52f51ad507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 42,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 462,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8ca6c59d-59ee-4629-8c02-0e510d92667a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 42,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 455,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3b1f94a8-caa6-4038-bd7a-add3c16f130b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 440,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "48586b91-fbc2-4982-9ead-d945e9b06181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 423,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "29593a4d-d126-4f7f-8191-6b91c89a0b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 9,
                "x": 412,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d06161fc-428c-4ba8-9414-97daa097eee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 395,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e40d6f5e-b335-49b5-b7f8-b361c707aa64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 378,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f834e1b0-f5e2-4bc4-8bfa-86e084bdb9de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 358,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "87627c88-3d73-4ae4-a7f4-6035cd65dd96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 484,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "37d63099-fc71-4300-ab0c-9c47d1b5de38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 172,
                "y": 90
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e72ea9f1-a405-4ec1-bca3-6e935919a832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 189,
                "y": 90
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "afd4cdc8-bc37-4b2f-8a4e-7bf90e12a5ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 206,
                "y": 90
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "af384863-36ad-41d7-9f2e-b551550f4ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 51,
                "y": 134
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ff758e4c-5d7b-48b5-a301-9a4ce2c51e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 42,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 44,
                "y": 134
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f120e628-b6da-423a-9f5f-2aa5ea122cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 42,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 36,
                "y": 134
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "229806f2-87fc-499e-ab93-27d5cc459361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 134
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1950a580-be97-4409-bd37-b6e631c1c2fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8c346105-b2e0-416f-a93b-773a339175f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 490,
                "y": 90
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "52cd5273-c877-495a-aeda-e770176b6997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 473,
                "y": 90
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6381c91c-5cac-459a-8a54-476660d90509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 452,
                "y": 90
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6743adaa-e394-46bc-8678-37d4253c91d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 431,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "42625577-990f-4190-b824-5790369e5602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 413,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ee9ef148-e0d0-48b4-80fc-cdc79ea40e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 394,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e6b4728a-d734-4c2d-b8ce-2524dc49a6d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 376,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2bb3e138-8b3f-43bf-9e92-f8d2c9854583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 360,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7ac811df-4d00-4cb8-b2cf-4e4962c4a66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 344,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "330d1c36-edc6-492d-a3bb-2ec095f89b5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 325,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "766fe0bf-3fdb-4dc2-86c6-5678db724776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 307,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f34dd459-85f9-4b2d-bc68-28b0f9d0e254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 292,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "41336b90-9714-4571-8b3b-d07ea1b5e44a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 276,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fe957310-c2d5-47df-a147-d5e10e8e354e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 16,
                "x": 258,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "444ebb31-98f9-41db-95d3-cd07c0df3a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 242,
                "y": 90
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b4d683ac-59f4-4f69-8284-b1c38b1096d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 223,
                "y": 90
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0144cb95-fecf-4224-9faa-922742775d93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 340,
                "y": 46
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "386a55d6-b9ad-4076-8860-98288362cab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 321,
                "y": 46
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "59dcbb15-608e-4e02-b878-2ba9fb6dae2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 303,
                "y": 46
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8f67f372-e2c3-4526-aeb7-cf3670b4c402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a64d0a69-1c35-4f20-80d8-b173551a7be0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "84cfffaa-84be-4086-a08f-c21e1b4c7087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 347,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9572838a-7cc9-4e7c-af88-e79677ed239f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 328,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a35abaae-5684-4cce-ba3d-0b8beb794f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d5cf94e5-771e-4fde-81c9-6b5398d4aefb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f412fb40-06e4-4a68-a0b6-8f88e9fd0910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 266,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f6d243c5-fc57-4187-a7c4-c252d647a6e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "460cf039-f539-4cb9-8be5-134ed386f8d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 42,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "917d3b83-92f3-4370-a494-1a4e4c47b765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c43bade5-aea7-4c9a-8138-49b44c601c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 42,
                "offset": 6,
                "shift": 19,
                "w": 9,
                "x": 383,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "96b2a574-c5c3-4098-af60-356def9830b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f1603ec3-2fc7-4ba7-b482-ad7b27bee43b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 42,
                "offset": 4,
                "shift": 19,
                "w": 9,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ceca4b9e-6010-40b4-b87c-ba70d5f13475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "276dd709-0255-470b-a769-4e4724e620aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 42,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1c6d6236-c2e9-436a-9428-416316b172e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 42,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "90d8a56c-3e3b-4514-83dd-0f2cbd02f928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d18aaa58-83d9-4cd2-b440-ab4239da5c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a7bb6c6c-fb9f-40f8-8b44-142f4b30230d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "253a9536-e2eb-44f6-a332-a8b49715b013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "70476ad3-c65e-4d3e-a2d6-39dacc85f394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f3f98e8d-9b08-49af-bbc1-883883e09b4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9efa115a-d572-48b4-bb92-5dcee8eaee02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 413,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "12fd8ee0-1845-4aec-a5ad-aaa4ee1876bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 99,
                "y": 46
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "89cddcf8-b7b7-4a32-88ce-144bf9634e56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8bf511cb-7751-4c5f-ac92-ae5627e83713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 12,
                "x": 273,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "eadd6ac6-d1af-4bc8-b520-c4cc8061a5e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 16,
                "x": 255,
                "y": 46
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1bef54d7-8b63-49fe-892b-cb7c9cb50701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 238,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9e25e0ad-342f-4ab8-a6e4-aa1cc7e056cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 219,
                "y": 46
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "47195723-135e-47a2-a6a8-6b10a4ae2c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 202,
                "y": 46
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1512977c-c7a7-4769-9253-03cf0f1722ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 183,
                "y": 46
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "330255db-4a2b-4106-a507-72c1dfb3a480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 165,
                "y": 46
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "759761e3-76b0-4d9c-bf7e-be17bd6c4927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 148,
                "y": 46
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5c840ea3-be22-4b25-8518-d9fc8f56476c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 42,
                "offset": 4,
                "shift": 19,
                "w": 13,
                "x": 133,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4944cb4f-45bf-4980-a7ba-1a87b978d7cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 287,
                "y": 46
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bec4fcd7-7463-4458-8f80-e094a7b68b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 116,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8ca51c97-98f9-4777-8ee5-77ae96d1225f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 82,
                "y": 46
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "24c8cfa2-5713-47be-895f-2b7d1f346cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 63,
                "y": 46
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "50aa39ef-7338-4c80-acf1-2e45d21ec210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 42,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 40,
                "y": 46
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "efb4c0d0-e16e-4aaf-bb82-9e61a11c22d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 46
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5d4a19f1-83ae-4df0-a19b-8fe7cec3342b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 42,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "494ed4a2-315b-4cd1-9e54-1e8b52a51b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "174db248-7368-44d3-a1cc-a6feae522dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 468,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b7a25c48-1c84-48e0-87e5-8dd358bf976d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 42,
                "offset": 8,
                "shift": 19,
                "w": 3,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a77c42db-7d16-4298-90b3-ae368467bced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "47edd6a9-71c4-49e6-819c-4c22879f35e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 42,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 68,
                "y": 134
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4fdcc46d-6966-413a-8070-aa36a0bdab64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 42,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 85,
                "y": 134
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}