{
    "id": "e4190870-d997-4208-b14b-3924b12e6654",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fKey",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Constantia",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "76c6157b-9b68-4b51-a786-59ea96502a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "36a59d9b-f743-4eae-b0d8-387408e6b9e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 169,
                "y": 95
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "415945fe-bbea-4bf9-b762-248ca695c643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 160,
                "y": 95
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "543940e8-d245-41bd-8b48-ce12d1a9abda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 145,
                "y": 95
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cefbdf24-c92a-4221-bca9-f3ba991cc370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 133,
                "y": 95
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "990a51a8-5105-48e7-85ff-229352318c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 113,
                "y": 95
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ac30588d-cba3-4a19-8dc2-fa9ae13662b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 95,
                "y": 95
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "00341870-0fa6-41c4-84df-c43a57ac531a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 90,
                "y": 95
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9272a30e-1425-4204-9623-16161921f03a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 82,
                "y": 95
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e883e68e-d250-40c0-90fb-95b03ca82102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 74,
                "y": 95
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f89c8097-882d-4d0e-823d-acefd3a483e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 175,
                "y": 95
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0edf25ff-5d3d-46d1-8889-b1bd8ff24f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 60,
                "y": 95
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "706b47da-5ced-49f7-bf83-2341adf47044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 41,
                "y": 95
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1839d518-5282-464b-af39-a65352130a8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 95
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cce75162-47ab-4dce-9933-9a92adbf6c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 26,
                "y": 95
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c140a819-1eaf-4c61-87f8-c775cb8af86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 16,
                "y": 95
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "dc9020bd-88f7-4cbf-983d-26509b63fabd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6868c0f1-c397-42ad-b964-1a8b81133139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 242,
                "y": 64
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "86e04a77-0f6b-408a-ad4e-cc4ee8148e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 64
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a1b61a8c-4cd5-441c-95f0-aed514a05cdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 216,
                "y": 64
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4dc123ec-36db-4fe1-b2a8-3ca4c2a4a32e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 202,
                "y": 64
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6644437e-5248-4952-a52e-00596d9f00f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 47,
                "y": 95
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "896495b0-aa93-4ae2-b984-f42b4488be14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 186,
                "y": 95
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bd67a49c-698d-46ae-a8cd-db9d31bc526f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 200,
                "y": 95
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7335b645-730b-4e6d-9b79-476371f98038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 214,
                "y": 95
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7e2a9914-4c79-4302-90f3-293c75835d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 28,
                "y": 157
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4ced3843-ea5f-4936-86d2-f831646d0a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 22,
                "y": 157
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4e3fcd10-da3a-4e43-9c9a-694bb7722b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 2,
                "shift": 6,
                "w": 4,
                "x": 16,
                "y": 157
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a8bb22ac-5f54-4ab4-a3e8-af01c923f4d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 157
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "15171101-9fca-4bac-b1b1-12c2b123f2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 232,
                "y": 126
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3bc4e96a-26f0-4716-93f8-62c4dd095b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 218,
                "y": 126
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9880d82c-10d2-4c92-89fb-0398a46060cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 207,
                "y": 126
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c7592bab-06f8-4868-8552-6cda7ba25f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 187,
                "y": 126
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c93f75c4-ea8d-4629-b74b-b02d21c01269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 167,
                "y": 126
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0584e4ac-e284-4914-99b6-2c8f44455f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 152,
                "y": 126
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "84c518c8-98f3-4a4b-b228-b399bf65dc46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 136,
                "y": 126
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "60943642-3f8a-40ee-bf98-a26660219b45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 118,
                "y": 126
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ea187897-8218-4dee-a754-23c50a9ba866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 104,
                "y": 126
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b1b6f1c9-b0e5-4bb3-8afd-bd32e8b63d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 90,
                "y": 126
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cd6a1c76-cfae-4ccc-a5ca-98796820bceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 73,
                "y": 126
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "04d72c6c-8cd4-4c43-a0dd-7e39739297c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 54,
                "y": 126
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "22ce5526-a323-4f20-9a21-3285faee0b37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 45,
                "y": 126
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "64a21e5c-b3dd-4acb-9159-feb40de6a3e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 34,
                "y": 126
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4fd5fcaf-f0a3-41ac-a567-d9cf22588d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 17,
                "y": 126
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0198d1c5-ed72-4118-882e-65e53c7a3091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f6885fc2-e476-4371-90a2-3f8d85532589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 228,
                "y": 95
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f21843f4-0c74-4252-9b54-a8fedec26ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 184,
                "y": 64
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9537aa35-77eb-4eb8-a154-5d19f9dc34f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 164,
                "y": 64
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8292746f-069d-4676-9585-5a35aa3d3282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 149,
                "y": 64
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "28166e81-f584-400e-ae91-0dad3bd464a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 75,
                "y": 33
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd5c9e10-e058-4b24-b784-d78a59711de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 52,
                "y": 33
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e06f5a42-042e-4c15-9bb5-827ee42abec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 38,
                "y": 33
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "991e6b30-e240-47c7-ab0e-d501cbe44f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 21,
                "y": 33
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8a07610b-bed1-451a-9ec8-24587a806186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ed1ef2bf-9d5b-497f-be20-2973fe418f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e197624b-a90f-470c-a929-f0b2bd551ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3712de7d-04a2-4e71-81dc-0afdaa66185f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "78530673-e81e-42d1-a0b7-fa81dd588f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "834ede91-ede7-4887-a990-54ccde24892e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "089d50b8-8bda-409a-ac30-f6fdcb05e42f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 3,
                "shift": 8,
                "w": 5,
                "x": 68,
                "y": 33
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2361f5be-7504-464c-b79d-1f1997348e8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "84c9e4be-a9fe-4b73-9330-a991978777b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6a3d9d53-f5f1-4f71-befa-273189c75a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5f456c48-10dd-4a9e-9cd0-5af196934955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c38eadf7-5a42-4952-b880-1d2d2316f613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "73bbc390-d9fe-4267-ad37-12293f5fa9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c228d5ae-4fed-471d-b09b-a5475a9f3e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c5edf0ab-3b09-45a2-956f-3d64b0c3a394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dd637de6-9d4a-404a-a8ee-5a03afada796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7f979cc1-1fdb-4c6a-9a86-34b70995a749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "04b098ab-b941-4abe-ab8c-be9a5d29ff23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 10,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a5e390f9-f321-43cd-aa70-a5105de47452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 95,
                "y": 33
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "963ec303-05db-45a1-b036-06ea2dce5c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 234,
                "y": 33
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "03d4302d-9c98-4ca1-b777-11bc9837e5c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 109,
                "y": 33
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2bd4ce48-df73-4571-8a5d-95abb99bc01d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 129,
                "y": 64
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d1540605-8c90-46e9-8e82-b91aba291c57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 114,
                "y": 64
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7aefbc49-55ae-4b17-9a58-2d7da63c6c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 106,
                "y": 64
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7f609d35-d3a9-48cd-a0af-5f27154631cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 83,
                "y": 64
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d612a52b-cc9f-4005-8dd2-1a8ebcd533f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 67,
                "y": 64
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "64f2d7da-92e0-4cfa-a4ed-f122c39f61b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 53,
                "y": 64
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6dd519ba-dc5d-4b64-a5c1-a9b49ac1b136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 38,
                "y": 64
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b3f190fe-ef34-48ae-a1ca-eb6be2494ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 24,
                "y": 64
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c83e6fb4-93ab-4f98-ad0b-225c92641c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 64
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7eb204ae-9d62-4055-b0eb-0d5684ea3ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 64
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b123ebe5-95c9-409e-95e5-d3dd5a2757d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ff08e143-1ebd-4ebd-bf26-41acba15a7b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 218,
                "y": 33
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6f0167d4-76d4-4cad-9301-a3757a7bbf4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 203,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4ac917b3-1de4-4c70-a82b-ee3c7113af71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 182,
                "y": 33
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "93953a44-04f2-448e-b55e-a1c5ddfd99ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 168,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "124723ad-0670-4d96-8ff6-ce33099167d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 153,
                "y": 33
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "584d7378-ae4c-49bd-a116-7f84735deec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 140,
                "y": 33
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "70d70e64-2eab-4c48-8443-f6b67e3d4e92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 33
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "296b24b9-ff97-4e77-bc34-b57eb5a760ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 4,
                "shift": 8,
                "w": 1,
                "x": 127,
                "y": 33
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "21203a3c-db96-43ee-86bb-e2503f44c2e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 117,
                "y": 33
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "85292010-898c-4d3b-b282-8cf4d7a89e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 42,
                "y": 157
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "60726b58-830e-41bd-beef-33f1a7745c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 29,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 56,
                "y": 157
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "eb311c81-8ab3-4326-a3c4-1cdb974fc872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 86
        },
        {
            "id": "25bdc5e8-4da1-404e-ac6f-86e51aa355fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "b8305108-b0ba-4e99-a563-2799bc3afa21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 97
        },
        {
            "id": "376d82e9-846c-4ad9-a2c8-dadd3094bc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 99
        },
        {
            "id": "865e6af3-e076-48b3-a8b0-ef073212e69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 100
        },
        {
            "id": "c3a191ee-9d11-464b-a74a-fe2aef69cad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 101
        },
        {
            "id": "eebd7179-3d70-4e58-a7d5-5ce59c9e059e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 103
        },
        {
            "id": "b02c9ff0-c2de-4294-91eb-fdd9dee09d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 111
        },
        {
            "id": "5014b1b0-95b9-4352-ade0-85c0308eaaeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 113
        },
        {
            "id": "bcd1b846-2f3b-4693-b7cb-f74cb038cdf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 118
        },
        {
            "id": "08dbb4b6-ce76-4e6c-becc-2bffc21e3267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 119
        },
        {
            "id": "e055f969-9c95-44de-9724-ccd70d1d140a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 120
        },
        {
            "id": "f48a4e17-8a59-4072-93c2-6ab549bcb778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 121
        },
        {
            "id": "ef62f8c9-d03e-416b-afc8-7f34d1184d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 221
        },
        {
            "id": "8cd4cc67-4e80-4574-8819-c905343a9734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 224
        },
        {
            "id": "1b883f8a-dced-40ff-a526-828e0418d01c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 225
        },
        {
            "id": "9fdd4127-1cd1-4d75-95f5-56a298b7871c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 226
        },
        {
            "id": "179e59df-b189-4be6-a397-d46cc990318c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 227
        },
        {
            "id": "dfed6698-33f3-471d-b7a5-f7b54190b7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 228
        },
        {
            "id": "e182dc52-c3f0-464d-95db-b2466a8c176b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 229
        },
        {
            "id": "400a815d-463d-4e75-b67e-1b1f33ad9b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 230
        },
        {
            "id": "97c9e461-d582-4009-a808-e3de5627e089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 231
        },
        {
            "id": "e151a5ab-96c9-4a22-8550-4c6bb74025e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 232
        },
        {
            "id": "a69706bd-f124-4a3a-9ba3-c7872d71e215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 233
        },
        {
            "id": "dd1ef1cb-c6ac-4ef1-baf4-dca2eebff535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 234
        },
        {
            "id": "3958b638-887d-452b-b267-39bbd8ef65f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 235
        },
        {
            "id": "dc1d52c2-a0a6-4dbc-a12f-9d9c82de5b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 240
        },
        {
            "id": "f38d7228-bf5b-43e7-9d48-a0e7c59b67a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 242
        },
        {
            "id": "35ef468b-5d48-44d8-b4cb-d86ac589874e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 243
        },
        {
            "id": "feadbfde-1e40-463b-aa31-e82c209b9eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 244
        },
        {
            "id": "cd134c26-3e4b-4048-a483-5c3787723c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 245
        },
        {
            "id": "38614447-1b46-48bc-a8ba-993265ff4516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 246
        },
        {
            "id": "936fe007-2c25-4cae-8290-5d08603c1725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 248
        },
        {
            "id": "ab089260-fe3a-4122-9138-334b291676d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 253
        },
        {
            "id": "2e95ef4a-5471-495e-b84e-2aa3c0b83a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 255
        },
        {
            "id": "36a4526d-9638-43e5-9a23-7c2dc30043ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 257
        },
        {
            "id": "385918fa-32b9-4f08-aed4-d8acf625243f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 259
        },
        {
            "id": "94684881-0f37-4ef1-a9e0-5a90599a6ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 261
        },
        {
            "id": "4ca4d219-dc14-4153-ae46-2d82df7fe989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 263
        },
        {
            "id": "0745761e-dda9-4368-a903-4a63f8e520d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 265
        },
        {
            "id": "3502278b-5e97-48e1-85f2-4e53e2db56d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 267
        },
        {
            "id": "8e1ab2b2-c0d1-4713-a449-24f59737eb37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 269
        },
        {
            "id": "50e5f649-d821-4eaa-af3a-ee4bb7454178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 271
        },
        {
            "id": "9c791116-7c07-4836-ba31-c0832a38e14e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 273
        },
        {
            "id": "c3b8032c-d0b2-4b84-ba49-faa356c6719e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 275
        },
        {
            "id": "2f58b512-a874-4525-95f1-663b17998a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 277
        },
        {
            "id": "56bb69da-ae5e-4919-85a4-18109846f1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 279
        },
        {
            "id": "377a619b-92d9-4a68-b761-6ea69f2bab12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 281
        },
        {
            "id": "c22c7217-9b5c-4e1a-94a7-43b386e6d00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 283
        },
        {
            "id": "da2ffd01-944a-4fbf-a6b0-a3cce6f1fb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 285
        },
        {
            "id": "28da4f3e-9744-4022-8af9-527780efb108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 287
        },
        {
            "id": "60640f59-c923-4ca9-97fe-a11b233b8645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 289
        },
        {
            "id": "f3003d61-799b-4d05-bebf-ea61ac05acec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 291
        },
        {
            "id": "e8db9a5f-a49b-4ec4-9615-1099ef18580e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 333
        },
        {
            "id": "ed46485e-48f5-4db9-a3d5-b1c9f369b508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 335
        },
        {
            "id": "5707a0eb-b9a5-46e1-a94e-13f0fe9ae11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 337
        },
        {
            "id": "8f6ec7ef-bb25-4fa2-9a58-ae3bf76953bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 339
        },
        {
            "id": "0856b654-4f49-433d-8e6e-331a5252ea49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 373
        },
        {
            "id": "4104bf3e-5088-49a7-ab10-ec797394d029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 374
        },
        {
            "id": "ae098ce1-b06f-49c2-a58d-d9adb68d5bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 375
        },
        {
            "id": "335e5352-c6b8-4587-bfbb-a3312a8cf33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 376
        },
        {
            "id": "eafc52b6-ff44-44f8-8764-4d371787ebc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 417
        },
        {
            "id": "bb01ef08-1898-4ed3-98cf-64d37fcb6797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 507
        },
        {
            "id": "3438e7bf-d03f-4b6c-9177-3f141af25415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 509
        },
        {
            "id": "d803d395-9c6e-41ed-9b8c-f665373bbe34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 511
        },
        {
            "id": "180c8c31-5d4f-4600-b294-71b7507080ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 910
        },
        {
            "id": "f5f17905-bf1a-4d30-9c79-63df91cf5627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 931
        },
        {
            "id": "a3f692bd-c4a0-4c7c-9daa-cb6b18db56a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "0e6ade67-507c-4a13-883c-f8486491892e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "d1e89d36-3d5f-4c95-ba30-90f4d670d036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 940
        },
        {
            "id": "f0b072df-200e-4a66-89c8-35f8114fc0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 941
        },
        {
            "id": "a91e9c3f-c128-41ac-9302-61d1ad60c7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 945
        },
        {
            "id": "59fba4d1-25cf-4c3c-abb9-da84e56c7b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 947
        },
        {
            "id": "5faa954f-3fc0-4668-9861-2ceff2e8cd77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 948
        },
        {
            "id": "e0a1e227-f478-428c-94d4-e42c4bce6f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 949
        },
        {
            "id": "0a026880-c24d-4737-be30-da21ce2f0b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 950
        },
        {
            "id": "2bc66534-b835-4659-bba9-a9e02552a759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 955
        },
        {
            "id": "f33bc2d7-4e50-4781-b147-ad635d7a3628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 957
        },
        {
            "id": "38e9ccfe-bfbf-4095-927f-50df1069f0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 958
        },
        {
            "id": "fb9fca26-65d7-4b93-877e-77e9d58f0a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 959
        },
        {
            "id": "4e3884a4-2ab0-4348-bfc0-4e3f06819c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 962
        },
        {
            "id": "931e6967-7403-4204-a25a-1e27d31cffe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 963
        },
        {
            "id": "3958e199-8517-4450-b46c-fb12659938af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 964
        },
        {
            "id": "7111ef23-3d21-42ad-a233-7026cc91aa0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 966
        },
        {
            "id": "2b9fe196-11a1-4d1e-a8c8-4e1d5807b5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 967
        },
        {
            "id": "9f221deb-9cce-4ef7-be32-c5eedd0193a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 968
        },
        {
            "id": "0b359e65-db0f-46b2-8b29-51d227cfcaa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 969
        },
        {
            "id": "0b892672-b262-47f7-abef-cc7289e706e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 972
        },
        {
            "id": "2ec0751e-8ac0-42f8-bf00-f919d23adf8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 974
        },
        {
            "id": "1359408d-b3f9-4bc4-b2fc-0a02b5db9c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1033
        },
        {
            "id": "fd0052d4-99a7-4594-b6f0-65f5a1fc0be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1038
        },
        {
            "id": "64289121-90f3-4473-86bc-15448070ac8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1044
        },
        {
            "id": "4eea1830-59c0-478e-a6be-7e3b2d7a7f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1051
        },
        {
            "id": "c395e1e9-d8a0-41ec-bc84-fe665bfc6e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1059
        },
        {
            "id": "c246ad42-5441-446c-bd56-cc1c8fc48b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1072
        },
        {
            "id": "19329634-74a0-42fd-b987-936fc60e3a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1076
        },
        {
            "id": "f12b80b0-50dc-43e7-822f-c3ccd89a3298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1077
        },
        {
            "id": "e50de7cc-8726-4704-b655-3f33bad28ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1083
        },
        {
            "id": "c59ad403-0844-4bdf-85a3-6a60339b70f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1086
        },
        {
            "id": "fae07532-bca3-4e12-a182-476080ca322f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1089
        },
        {
            "id": "54922b08-76e7-4bc1-9f33-deb79d22190b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1090
        },
        {
            "id": "041ee4a4-7201-4ad2-941c-be7e6388362f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1091
        },
        {
            "id": "d870736d-d11a-4fe2-ba05-02bd4fbbf34f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1092
        },
        {
            "id": "e93aa1f8-06ef-4894-990f-ad62d14ac880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1093
        },
        {
            "id": "2d6d1a7e-50fc-4a90-bfa7-e904ab1d8276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1095
        },
        {
            "id": "6004751e-2501-401f-a5ac-799d47cd1596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1098
        },
        {
            "id": "8a7d9881-ad69-4141-bf4b-159141965075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1101
        },
        {
            "id": "641b612f-7bc4-48a7-a32e-2a3e94446684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1103
        },
        {
            "id": "523fe20c-766c-4cab-b77d-b496698ef59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1104
        },
        {
            "id": "e3155777-70a7-4f22-8aaf-baef8d7e3b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1105
        },
        {
            "id": "100043e0-c6e3-4e25-90d6-42fd827f2065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1108
        },
        {
            "id": "aef64d40-908e-4457-a345-16c5faa8aca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1113
        },
        {
            "id": "1ac8ebab-7f5a-4d50-ab85-c6787705f53c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1118
        },
        {
            "id": "fb49908a-f114-418f-ab60-902abb270717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1139
        },
        {
            "id": "0f2281f9-2757-4118-baec-94043d59863a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1140
        },
        {
            "id": "f0671df6-2e2a-460a-8434-fdc818564704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1141
        },
        {
            "id": "07a69396-fa56-454a-883c-4d04dc50060e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7809
        },
        {
            "id": "063b606f-5118-4316-9cfe-b0672921e2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7811
        },
        {
            "id": "2a536631-22c2-442a-8197-6f9d7caab35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7813
        },
        {
            "id": "bd315356-75ac-41f2-9272-3e448f255e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7841
        },
        {
            "id": "d93081f4-585c-431a-a5eb-842670d0c142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7843
        },
        {
            "id": "60383cb1-9341-4c3e-9372-097ddd969c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7845
        },
        {
            "id": "c55c4003-c2e3-4052-9437-71d5b0873791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7849
        },
        {
            "id": "dcd4cedd-9588-48c9-90cc-3ccc51a155b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7851
        },
        {
            "id": "79e9c469-d541-4fd7-9eed-77f62d6c976e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7853
        },
        {
            "id": "0310fe1f-b97e-46a0-9f28-ff2137900cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7855
        },
        {
            "id": "771e9396-3f1e-4f03-bb54-6e4b13894aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7857
        },
        {
            "id": "66952a9b-4e33-4a12-95d6-823714f321d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7859
        },
        {
            "id": "ea8f6f3f-b1f4-42c6-9dc9-b22b712eff6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7861
        },
        {
            "id": "6d75f36b-dce5-499b-bcc1-8699f2c15a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7863
        },
        {
            "id": "a391f3bd-0b52-485f-9b2a-f4e9602f357e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7865
        },
        {
            "id": "84503bb2-efa3-4c36-a9fb-7297abbb2881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7867
        },
        {
            "id": "a53702f1-2e7c-4c3d-b611-496e29b890f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7869
        },
        {
            "id": "2e98dac5-4703-4c6a-adc1-ab2ea3f2de80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7871
        },
        {
            "id": "cc956d14-cd40-44ed-bc10-7c71d39d19cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7875
        },
        {
            "id": "e98974ba-3251-4e9e-a1eb-19c612bd5d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7877
        },
        {
            "id": "c3946937-5600-4596-bcab-e8ad603f062e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7879
        },
        {
            "id": "af24dcff-8d59-4ea3-a83f-17260a242f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7885
        },
        {
            "id": "8b0360a8-1ce7-4316-9428-0fe56c1f8016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7887
        },
        {
            "id": "202d6886-0f26-44cc-adfc-8171cc9c89f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7889
        },
        {
            "id": "c9b80d4f-0276-4673-bc72-f7b29b94bae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7893
        },
        {
            "id": "8a071c09-1ed7-4985-a5e8-fb02b1672e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7895
        },
        {
            "id": "a9f94655-d0db-4edb-b2eb-0aa3e81ed825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7897
        },
        {
            "id": "5cd3f0af-4012-467d-94c5-4c27fe81d990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7899
        },
        {
            "id": "65809de2-9be0-48d2-a189-1be3e6d7d531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7901
        },
        {
            "id": "2df88fdc-ace0-4fa4-9f8e-f61bf8b3b597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7903
        },
        {
            "id": "efe257e0-d342-4736-bc3c-96589138b97b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7905
        },
        {
            "id": "2bf371e0-8ca7-42a1-b981-1bd54abfbc9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7907
        },
        {
            "id": "2affe94f-65df-4ea0-991f-f3bfcc432f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7922
        },
        {
            "id": "be6e4ea1-0682-4e0b-bc07-e29383cd5c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7923
        },
        {
            "id": "e2371a22-1bfe-48c6-a178-96798e8fc28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7924
        },
        {
            "id": "35cbaabe-6637-4c9b-a2cc-f3eb3325fc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7925
        },
        {
            "id": "f5b42603-9ef8-4b22-9a3d-908c896b3142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7926
        },
        {
            "id": "5b8ef627-b3f7-41a8-a419-77be653f4e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7927
        },
        {
            "id": "55ab1bc7-0479-4e03-8e70-a90233662e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7928
        },
        {
            "id": "353e2758-1d03-44f1-ac12-587fd29c1c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7929
        },
        {
            "id": "a88fec72-22c7-4376-8040-7d2c861c2cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 42
        },
        {
            "id": "21825d57-d637-412a-980a-8451fff5fde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "cd272666-eaab-4309-9a6b-0cac044ca7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "4681c29e-e2c8-46b4-8840-cc89b5ad5503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "8412d02c-0f94-4cac-aeab-27366a2b2f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "65a3dd95-8312-41c7-8e71-3aebbba8e807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "90dde116-5e89-4328-8914-6c27b073a750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "f927d426-86af-4b93-b264-b780307eee37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "48d7a113-7af5-484b-bcf5-a315e5614d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "45418c3a-b49a-44ac-ae37-ebf7bf3f3b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "47d01728-2f85-42d6-882c-90c951704a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "4d8e2b8a-dc75-49dd-85cb-5b3382aec571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "19c9824e-3e86-4cea-864a-fcefdc4e1b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "d774489d-35c4-47d2-9749-fc642799fcb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "f2a21cd8-b04f-4cd2-9a92-746c5863f7a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 174
        },
        {
            "id": "2d330b03-39a2-4471-9376-5382b2095016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "11e9dd72-9fb9-4748-a729-18c52285c34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "cdac6283-b1aa-41ab-80be-7a5efc2c3952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "c899e590-b87a-4b84-952d-c171965b2189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "25fa6e6c-efcc-40a6-a5a4-58667bfbe767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "2727f894-735f-4f72-8359-911ee2781561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "87c60a26-0cda-41fd-85d0-0d4036bc4f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 216
        },
        {
            "id": "54c26fe1-ac98-43d2-a8db-f371afed3723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 217
        },
        {
            "id": "0c737507-3498-46fb-98a6-e62399979f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 218
        },
        {
            "id": "b9174649-a6d3-4808-9e52-a36fe0219438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 219
        },
        {
            "id": "43ec63f0-d067-4605-87d1-e87c9637a8b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 220
        },
        {
            "id": "1e9ffa58-8906-4b3e-9807-925cd5e59de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "14cebcc0-6b54-4307-8cb5-981791c94050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "a1be86e0-5443-4ddb-afec-4bc69236b300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "06606b64-2b0a-4f60-98e8-b145f94418ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "409db577-5085-4b13-91fc-111f221b0ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "d3830c97-7479-4817-8cec-5a3ec393483c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "98b31671-ed66-428f-b463-63f021c0227d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "85b325d6-5817-4e4f-8d29-b60afad82e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "623ae5c8-cb6d-4fc4-b72e-ed4c36708b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "d4153a3b-f0fc-4664-b029-64d2eab9067a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "2ef7e740-6a99-4951-9ce9-3889a1f709f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "7d12793b-cafb-4f25-a9bd-c693a85be561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 329
        },
        {
            "id": "9d0d5bf4-2946-47cd-9eb0-98ff0078da2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "8d189797-de33-42a0-b4f6-230a2b841364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "de0a390a-a75a-4800-93dc-973ac07ea820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "7d2cb538-492d-4638-bf8a-fa592b94a6aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "6f96d200-d8c8-4f80-9b43-658d09515af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "afeaa781-3608-48f2-a249-0cba16a85d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "4679f909-13eb-4ec5-a0f6-e922daf6771c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 358
        },
        {
            "id": "32efae6f-bfcc-495f-9c2a-8aea6adbe736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 360
        },
        {
            "id": "7f543773-b269-4812-b28c-6cab76fb5928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 362
        },
        {
            "id": "c44b3853-65fa-4dd0-903a-41762d749bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 364
        },
        {
            "id": "d4b034ee-942a-4c3e-82aa-29fc0f113b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 366
        },
        {
            "id": "73928e74-00bd-4c7e-960d-37357ac6c93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 368
        },
        {
            "id": "b16c73ba-1c6c-4939-8dbd-e0cfda6b21a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 370
        },
        {
            "id": "7c5b96eb-b1ce-4c4d-b109-4c408c6cb41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 372
        },
        {
            "id": "5c387db8-75b6-4245-afbf-0721d2e43528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 373
        },
        {
            "id": "005ef869-8082-4995-9fd5-f5e90f1fcdce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "c3db2e6a-909a-4c9f-9e61-b483d04e8e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "a3106aa1-3500-437e-9804-f6acc98693d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "e8c61b2b-9e01-4abd-83c7-3a193b9ea1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 416
        },
        {
            "id": "5884f978-a35e-4c51-be50-905cb9bb7fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 431
        },
        {
            "id": "c4c20fff-0656-4e97-ae7b-eafb664e92d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 510
        },
        {
            "id": "8cdfa2fb-cf6e-4433-98fd-825576b4fbcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "f1c1e647-7256-4583-a76a-e76a7605b9e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7808
        },
        {
            "id": "1616994d-6572-4abd-a50a-b7a43dc019c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7809
        },
        {
            "id": "f763d57f-1a4b-4d53-801e-4332641da408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7810
        },
        {
            "id": "435f5896-a495-4000-9225-49b7f8eb4b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7811
        },
        {
            "id": "63b65604-db88-4802-afc8-9296971b1813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7812
        },
        {
            "id": "48af3103-bc4a-4d74-921b-a7c859125796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7813
        },
        {
            "id": "7a670e71-f8b9-4897-8808-50f12c4bb3d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7884
        },
        {
            "id": "7391bd84-4119-4783-aa48-984531d58fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7886
        },
        {
            "id": "71be9cf7-06b4-441a-86ae-eb1e1188d80a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7888
        },
        {
            "id": "8453e561-86d7-40cb-a836-766bf19c4acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7890
        },
        {
            "id": "ba3e73cf-f51f-4df5-8b8c-e121a6201b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7892
        },
        {
            "id": "1131d4e3-3312-494a-95e2-d0f75fde8628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7894
        },
        {
            "id": "79a90d55-a43d-4fd8-b7b9-a644179b9ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7896
        },
        {
            "id": "79b38df1-bbd2-4165-b09f-20ad05f16408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7898
        },
        {
            "id": "d8f885d8-0464-470a-b742-e7fd349656c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7900
        },
        {
            "id": "1c8980e5-c489-4efa-875e-d7361dcb7280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7902
        },
        {
            "id": "eab32f4e-890c-44b0-9f3e-0e10725fa898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7904
        },
        {
            "id": "a250c5a7-8dcc-4745-8a6f-9795694a1987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7906
        },
        {
            "id": "5f16a44e-7972-477c-83b8-63b9952dcba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7908
        },
        {
            "id": "4593721c-d2c1-42f2-a74a-cbc7d1acc26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7910
        },
        {
            "id": "95a2c5c9-3f29-429e-a70c-59fceda1670f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7912
        },
        {
            "id": "8020c5cc-a454-4538-ab19-35f5ddcd1382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7914
        },
        {
            "id": "da515760-912c-45a0-9c17-754addd02526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7916
        },
        {
            "id": "97ea9585-c220-401f-bddb-8a57effe9b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7918
        },
        {
            "id": "82537b39-4408-4ead-be3e-b8a3f595d698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7920
        },
        {
            "id": "dd9466fd-1283-407c-a4b3-8c2f2ca69a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "c7e6f016-a6b9-44cf-8de3-0dc7a7055cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7923
        },
        {
            "id": "6b55c390-c9e6-4d4c-883e-a6fb58a51658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7924
        },
        {
            "id": "1599445f-dee0-431d-8943-53c5ba7e2caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7925
        },
        {
            "id": "6b9cbbeb-1b62-4c43-b533-0e97a4a29f78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7926
        },
        {
            "id": "2a7a0096-aff8-4229-b2e2-38d823a9fc77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7927
        },
        {
            "id": "af33ab57-4b7a-4b53-88f1-0e01e3571941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7928
        },
        {
            "id": "bac45f42-8c00-4065-95c6-e8c32f67d3c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7929
        },
        {
            "id": "f8685442-fd7b-496a-b3d6-2f289fba269d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "0288cbf3-aade-48cc-80df-4a1f85bab0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "defc0eec-258c-4c28-8191-b2db67de6919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "1f413359-8bfb-452e-9f1e-16e7ea137ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "43f7641a-5622-479f-907f-4d39979f8e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8482
        },
        {
            "id": "10647e90-be37-42c2-905b-ca434c51ea90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 44
        },
        {
            "id": "221a1db4-181f-4249-a5e4-03c70f796195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 46
        },
        {
            "id": "3cd4e366-0908-4475-96de-dab28d9c93a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "839abe1e-567c-4024-a817-e4bc9573aeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "4a83297b-3f0a-461f-8644-054375dc6e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "8a854e03-787f-4bd1-8833-da42db22e9c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "2daadf93-75e4-4049-a5b7-d8095b517845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 372
        },
        {
            "id": "9a9fdd77-8073-4106-afed-122de9193cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "35e6d611-3513-4992-9d3f-bd0138f06be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "d5ad5af8-bf40-4e8f-a8d8-2ea4b256388c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7808
        },
        {
            "id": "b5554d15-1f67-4a34-8176-1c67038d4b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7810
        },
        {
            "id": "31949daf-ebd9-405f-b13e-6b346e9e0bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7812
        },
        {
            "id": "493060ad-a3d0-49f0-b891-9888bdde52a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "fe66da3a-9e04-4cee-aeb7-f54dbf0c6bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7924
        },
        {
            "id": "56381c90-5044-4ab4-9e31-9a0c94ae21d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7926
        },
        {
            "id": "eafcfb07-1926-4afd-a0ca-86f1568ad771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7928
        },
        {
            "id": "892bda08-90c5-4bc4-8443-aaaee49c9db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8230
        },
        {
            "id": "eded3661-0665-4efb-9ee3-b77011560d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "aab6202a-600c-4e76-a966-2ba9821c7a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "227c41cc-b81c-4a48-b203-21372ee7220c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "4bb82cfb-ed31-405f-b8e0-1efb38038a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "00b99411-2989-45c0-8cde-66e608b14752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "850d6fbc-0281-4b27-b41a-cb800c2bfdbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 173
        },
        {
            "id": "e99786ab-9089-4e2f-aef7-4ee4e006c71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "1c2a4f6a-8996-4c54-bef5-786f0db8be03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "c1cc76cf-25ab-4dcd-857c-c3f3b38f8eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "99eb1772-93a1-49f1-a8d3-515b19e47611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "8b7c0695-3eed-4b69-9aa4-32ff993f4f30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "29566685-bcfd-4b88-8cd6-578da59dd38d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "07d03628-6dc5-483f-b75e-3575dec8ef47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "bac2ffaf-9a8d-417d-b1b4-fd013f0d6c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "6c23190c-02b4-40cc-83ec-1b6f7270b7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "13dc322f-12cf-4fde-9c98-cd751952bbe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 266
        },
        {
            "id": "43b7d851-9f04-461a-b387-b5b476276232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "2a110a5c-516d-4263-a48b-548d37dbeff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "e99c57d9-dd52-4ad8-98bd-1fce87a2ed4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "77282cb9-1dca-4be2-a5c2-73273ab7b8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "a0d54c4d-50f6-410d-bd12-a438e05151ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "4e475bc8-79c1-4797-85df-098298497c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 332
        },
        {
            "id": "68b31f60-f790-4884-894c-9f56d3ae9ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 334
        },
        {
            "id": "0c7aa5c1-4c28-4372-a104-213ac4082ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 336
        },
        {
            "id": "67dcfd01-f00b-48de-846d-a794ce0b157f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "171908a7-4621-48dd-bce1-0be2bfd3d842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 416
        },
        {
            "id": "fb398556-3b81-4306-bb94-aa187cc771a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 510
        },
        {
            "id": "a332c4d0-3150-4547-bb6d-8a0555f294ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7884
        },
        {
            "id": "fa181a4b-a2cd-4c56-9eb4-d47ffe6067f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7886
        },
        {
            "id": "add7f0f6-7326-467c-b936-a32f1f248646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7888
        },
        {
            "id": "c3d57077-821a-4b61-80dd-a4b9574133a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7890
        },
        {
            "id": "4caa98ce-a7b1-420f-9791-d5bb2451b20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7892
        },
        {
            "id": "20afa705-1996-4f55-b32a-a62abda30e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7894
        },
        {
            "id": "9a175e16-5de5-42c6-8351-9a8d2888d774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7896
        },
        {
            "id": "76e6c25f-94fb-4d7f-98ac-f85fc6f1fdc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7898
        },
        {
            "id": "bcbcc698-ee74-474b-96c7-afbd1cc33678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7900
        },
        {
            "id": "3759d0d8-2f17-4d5d-a4b0-d960a8fd4823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7902
        },
        {
            "id": "2920e6d0-c4c3-46d1-8703-0528d2da94e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7904
        },
        {
            "id": "995a6677-ceb1-4c59-825f-755943e9cdac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7906
        },
        {
            "id": "e06dc19a-788a-4804-8d46-50ee1ee445d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8208
        },
        {
            "id": "4b956974-4cd6-4968-8b90-50f65421d5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8211
        },
        {
            "id": "b80610b2-c267-455c-beaf-6517d63cf854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8212
        },
        {
            "id": "feb81f86-3d8f-4ba9-84da-46909ad7dd6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8213
        },
        {
            "id": "a8919916-462d-4baa-a96a-a497f21eac9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "31657dbf-e5ad-4f42-a208-defacea00bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "0e8c2ee1-1827-41f6-bd33-03b07f808076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "d65712a5-5dbb-4cb1-8ca8-aac10f8080c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "2cedd0c4-8a9d-413f-bb6b-431f8aa1a944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "125b751c-4b11-407f-8408-a19193ba21a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "7ba13ccb-4610-4d65-9f80-622b0f853839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "97c17697-1069-4249-8574-208b65e62f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "6e265c82-5f74-4c6d-ba1b-3bb28b06fe66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "8564eccf-5060-4c6f-8e2b-7e604217d3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "524c4fa4-e44a-4892-8245-c2404573629a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "7442f347-fa38-491b-8169-0f171f386370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "02f3fb71-df59-4329-8424-7215b7ff9bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "f9d3a4f2-a27b-41e7-a0d1-955ee5e3f521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 198
        },
        {
            "id": "9bc7ffdd-a79f-428f-af2a-840c1bdc4899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 221
        },
        {
            "id": "87552fb8-0e65-4b8b-82ae-ecc1dd9ea895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "4481d3e9-ad38-45f5-9818-4dfd70cccf14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "2791fd6a-5e73-4040-be6a-84ae23da1e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "ee03a2d8-f652-4b76-b007-405a63dc7305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 372
        },
        {
            "id": "dc26217e-8091-4abb-82df-78e5c697101b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 374
        },
        {
            "id": "7023700c-ffab-4d83-a17a-e76603023efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 376
        },
        {
            "id": "2f3b986b-3e41-44c2-b419-3d035591f52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "78cece79-e25e-40c1-a10e-4da8f350e0cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 508
        },
        {
            "id": "d4196d49-cb5f-467c-b678-c5f1dc43d232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7808
        },
        {
            "id": "88343579-60ba-45e4-872d-c357e69b4178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7810
        },
        {
            "id": "9bd63741-c677-4929-b6a3-1e85aeb4d5af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7812
        },
        {
            "id": "26c75d33-8669-4953-9e41-aa017ac9c90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "cfba5809-185c-4ae7-a287-b1d0485a2950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "8dd3f398-f4bc-4c83-9d88-83bf74618195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "65d8edd4-fdaa-471b-a24d-3ee70e4eed70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "2690bb1d-e82f-40da-9394-c15200f8f90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "41f499b6-c0ea-4e88-8232-20acfbe48f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "d313a334-2c2a-4886-b26a-a311ffa1f847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "d8c27111-13fe-48cc-858c-fc9b50f56de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "e5655c60-2594-4b72-9917-320fb4841c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "749854b6-ab0d-4214-a63e-19df57510b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "c896c61f-76ce-40e1-878c-56032c5bbfa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "c5eda729-95c8-4d0d-8451-412867380f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "b711e2bf-76d5-4e06-a08c-1454e8f7c680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7922
        },
        {
            "id": "b51c864b-c281-44ac-ab7b-98314038e1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7924
        },
        {
            "id": "cada0e04-fa39-403b-9b04-7f34aecf5b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7926
        },
        {
            "id": "bb5d450f-5d05-4164-b983-6bcefd8eaae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7928
        },
        {
            "id": "bade0c9b-41cc-4d0f-bb5e-ce912626f273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8230
        },
        {
            "id": "4ea1f642-eaeb-4bbc-a5a4-5f261251768a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "07579d7e-1260-4bbe-b411-c9f0ce15140c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "841b3cda-1690-4a36-b717-a53bee3e4a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "90da7f1f-fe51-4f2e-a64c-016b79ad3969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 253
        },
        {
            "id": "0629af2a-6bf8-4137-906a-288a0772a2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 255
        },
        {
            "id": "51048c0e-a390-40bf-b2b9-8e38bd77cefe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 373
        },
        {
            "id": "5de46ecd-7e5a-40a7-96f4-a02e95197909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 375
        },
        {
            "id": "33068e8d-cd40-4f0e-9984-af7fb0114446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7809
        },
        {
            "id": "7cffef4c-3fd8-4f40-9854-afea2a776f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7811
        },
        {
            "id": "40e2e6cc-91c3-444c-ae0e-f5d4c0aa3273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7813
        },
        {
            "id": "3d54bea8-df40-4041-ab45-009d74a64297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7923
        },
        {
            "id": "c0690b74-82c4-48da-973e-cf2bc97d90c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7925
        },
        {
            "id": "09ef1e2b-9d7d-46d4-8b1c-ffd1a6dc944b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7927
        },
        {
            "id": "cb5e8e4b-5e46-49e6-9207-7bb1f62517e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7929
        },
        {
            "id": "5b543ce6-3b02-4f29-a4dd-73909c74c67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "96590267-e4a9-44cc-bdfb-0894db96ec26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "304859a2-ef45-41d7-9efe-b74bc3776109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "cf329280-0b55-4753-896c-e193297dde5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "5faa42a8-dbd6-468d-929e-366c6c84c1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 67
        },
        {
            "id": "460148ae-57ad-434f-a925-1e5b40b1f5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 71
        },
        {
            "id": "dee3a503-f58d-4d38-bd48-1383e5da316d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 79
        },
        {
            "id": "bde1eb48-c746-4f92-ac5e-29f5ca08e4dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 81
        },
        {
            "id": "d0dedc16-f97b-408c-addf-59f069c145f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "09e029a5-f34c-46de-b331-4cc9a9e168f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "cedec4f9-52bc-4d09-bf91-ae7095ab7deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "6b588141-1055-4b06-8a61-37ef834e9803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 114
        },
        {
            "id": "a2ee1de2-3ce7-4c15-a7d8-0e30d468a01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 173
        },
        {
            "id": "2cf02318-a4c0-4757-97db-238bcaaff8ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "09e5e34c-0ce9-4824-b754-1724c1313662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "5633f513-994d-40bd-8406-9ada8be8c975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "8e54827f-8bd6-4109-9e2a-d4b02e0e7e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "94b164f5-b56b-482c-9f2b-102d96b06774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "8391b214-b6dd-4766-9bd5-b6678e3926a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "89adb79f-933b-44c5-bffd-d5dc0764d761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "b77e078d-7eec-467d-bb52-6ddb0bd46a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 199
        },
        {
            "id": "3cb67de6-d54f-4490-ad7b-d22b3a7b45c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 210
        },
        {
            "id": "5c90e12d-7d69-4fe9-9f62-6aadedcd5ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 211
        },
        {
            "id": "9c694d4c-b11f-4693-a07f-d13aeb4e6332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 212
        },
        {
            "id": "1c6c56fc-3738-4ca8-be31-19384dedde63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 213
        },
        {
            "id": "1175a128-51eb-41b9-b208-5853113f46f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 214
        },
        {
            "id": "b350f285-c248-4b0d-8ccd-527e70e7fa3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 216
        },
        {
            "id": "3062803c-b388-4a71-9739-239828ca0ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 224
        },
        {
            "id": "68c2769e-1277-47b5-8ad7-f0c347cb23fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 225
        },
        {
            "id": "f4646b6f-5031-480e-b55d-d2daa9987ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 226
        },
        {
            "id": "a02a6bb1-f197-4891-ad3a-5f014721c498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 227
        },
        {
            "id": "7c827b18-2783-4265-ac27-2d68cdf5fb43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "53c116b0-8fc1-4789-8af0-cb995eb94e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 229
        },
        {
            "id": "83fe7204-ecbe-4eff-b59f-9e1082fde906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "5086c41e-cb6c-4832-bcea-bd1de65c650b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 232
        },
        {
            "id": "30aa111e-4254-4c00-b1db-953a8a041b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 233
        },
        {
            "id": "b697b9f7-3f35-4e1e-822f-7670a9885353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 234
        },
        {
            "id": "e967ab9b-7853-4f87-8520-ea62a3bd7ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 235
        },
        {
            "id": "369bff3a-82c8-422c-a879-d14604182da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "19eff6a5-f4ed-417b-a033-8636c5072bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 242
        },
        {
            "id": "8da630f2-653c-44de-8f54-33cc21beafe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 243
        },
        {
            "id": "4a4066ab-cf14-4ec1-a11d-d5fa0aab6ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 244
        },
        {
            "id": "07449151-f953-415d-8f49-52f468602a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 245
        },
        {
            "id": "beee631a-2e6e-4371-a58e-f013a45a98db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 246
        },
        {
            "id": "9fb15040-e145-4e73-8aec-b79fd24ca53d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 248
        },
        {
            "id": "31dc2018-b812-490a-81d3-45fcd7ce3824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "6c81a897-430b-438c-94ea-f27a2e04a68b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 257
        },
        {
            "id": "16e7b33f-48e3-42bf-b497-e6370b9dcf00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "c21ae4ff-c7b6-4d85-84a9-06002da616de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 259
        },
        {
            "id": "3b4c7980-1757-4c9a-9c57-9438d29cc61b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "12b3b9dd-d55a-448d-80f3-4e81491f68ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 261
        },
        {
            "id": "1cedcf82-0011-4848-a9e5-d0d5ad1f5e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 262
        },
        {
            "id": "8a3ef441-cafd-47a6-b1ad-f5bd771abf0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 264
        },
        {
            "id": "4267abdb-2cc5-45ac-91d4-d42de5982769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 266
        },
        {
            "id": "33f362b1-4d92-4ef2-a8cb-67ff799baf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 268
        },
        {
            "id": "8e53bb87-1b66-489d-8020-8add61782d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 275
        },
        {
            "id": "297c2737-0957-4bdb-b99a-26c5889e64ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 277
        },
        {
            "id": "4e14770b-750b-40e2-9b04-9614228950c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 279
        },
        {
            "id": "2b08f9a8-ebf2-4fcd-9825-4dcdd41511d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 281
        },
        {
            "id": "9283071b-e4e1-40d9-ab56-6d3ca0ecf5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 283
        },
        {
            "id": "4781767a-fc07-4987-9509-e66b7b3d046f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 284
        },
        {
            "id": "d2c06b57-75ea-4055-9f54-4ec847b3bc6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 286
        },
        {
            "id": "c273678e-0868-4fa2-85ed-2d5dcf09946f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 288
        },
        {
            "id": "69ff441a-f86b-45b2-b33d-f8bd9b81eb0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 290
        },
        {
            "id": "424a733b-5667-48a2-850d-720447716885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 297
        },
        {
            "id": "d91a39eb-cec8-494c-8fe8-42276c488e8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "4cc678ce-61d3-4d42-8569-b4af9a4517aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 301
        },
        {
            "id": "2cd5e539-0229-4a16-aedd-2f095a9a43b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 329
        },
        {
            "id": "7458db3a-476d-4947-a08e-175276271daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 332
        },
        {
            "id": "5ad5dc94-ddb8-4772-9a98-39436622222f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 333
        },
        {
            "id": "bdbb9b44-73fd-4e39-b05b-193c5c42f680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 334
        },
        {
            "id": "a2eb96c6-a3ca-46cd-b616-96d5828a9dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 335
        },
        {
            "id": "bb95f9a2-df32-4657-9a89-38123d328d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 336
        },
        {
            "id": "f3aff3a0-3371-4d20-bbcc-dff84b3cadc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 337
        },
        {
            "id": "a096ac6f-9914-404a-b454-43263b1e7e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 338
        },
        {
            "id": "dca68123-050a-4310-8558-64096fad9a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 339
        },
        {
            "id": "abd1abc1-da79-460a-8b5a-b535b28e0637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 341
        },
        {
            "id": "76265914-1d05-4dac-a618-1f6797ea3adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 343
        },
        {
            "id": "c9d457a8-a0a5-4a32-a8cc-3690d749ba48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 416
        },
        {
            "id": "2708a86c-b388-4216-8f83-56eeae690de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 417
        },
        {
            "id": "25c354d7-e223-49cb-a3af-7aea03c1eb7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "658ba3a2-ad8f-4f33-a144-a12d975669c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 507
        },
        {
            "id": "885596eb-d525-4499-8c2c-43eca2a0e14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 508
        },
        {
            "id": "8154cd49-30bd-49ce-b085-2e62bc9afea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 509
        },
        {
            "id": "9bd61c7a-1cc3-464f-a214-2eef1b1b42a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 510
        },
        {
            "id": "22a2d16c-734c-4774-a674-83e9a9397cad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 511
        },
        {
            "id": "838925f1-e9ba-4737-8a35-eece726bed30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "1049f2ed-9517-436d-a699-cd223d94a1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7841
        },
        {
            "id": "b668810e-ab12-4c67-8ae5-e1b189b4780a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "6198202e-b72c-4015-994a-faf8d4a098b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7843
        },
        {
            "id": "3eb8a926-197a-4488-98b8-4d57ba40b6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "9f28bb7b-18b8-4373-8c1e-cbdcf40684c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7845
        },
        {
            "id": "228a2427-3e88-4ce0-8585-8f978f8487bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "e91c2be0-4067-44c7-a713-f6bf161a45b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "6cab36f0-48f0-4908-b356-157d69e6b856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7849
        },
        {
            "id": "082278c6-64e9-4478-bc0a-5086e590f8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "163ae11a-9687-4279-95a8-16ad319e7f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7851
        },
        {
            "id": "3f20552e-4858-42da-be7a-544c31b71c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "d940ee0d-7239-46b8-9d9c-4f0959cf4480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7853
        },
        {
            "id": "4bbeea16-bc7a-48a9-aed0-8375f65088c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "58e42668-2556-4987-8cf9-c3cb54e23170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7855
        },
        {
            "id": "3414d907-6a0c-4c47-8ea3-d01653bec579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "9e321ca8-5c88-44e3-9b07-4033f7be26a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7857
        },
        {
            "id": "a81cb668-ceef-4055-a4e8-945e81a7c645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "37639fc7-efa6-4740-b3d2-41e87bfedcc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7859
        },
        {
            "id": "92ec3fba-fb31-4f47-a023-77b77c0b766a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "20ddc117-2ca3-43af-a511-2509eac22948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7861
        },
        {
            "id": "041d940d-abcb-4aa5-91a8-dd58f6137043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "23bbe046-2b22-44df-9284-2a2a9ca37ec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7863
        },
        {
            "id": "a680f7e2-a01c-4f0f-96cf-70dcd12cc4de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7865
        },
        {
            "id": "a0dd4a25-3ec1-4dbc-8e6b-3cba14ad68b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7867
        },
        {
            "id": "e59ec618-b19d-47e7-a223-0d8ffde124b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7869
        },
        {
            "id": "e26b123f-d5fa-4e85-bc5c-a1949261bacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7871
        },
        {
            "id": "f6f0f64d-ff61-4dc0-9e8a-d7b602f4f6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7875
        },
        {
            "id": "f24d8145-a558-42fc-8155-f67982d2d1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7877
        },
        {
            "id": "bdd93a87-45f6-4e73-aa5b-952c600bb387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7879
        },
        {
            "id": "91f8ac59-4e51-4c83-85c0-c9f6387cdc54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7884
        },
        {
            "id": "a7c8d214-7432-4a06-b3b5-2c93b00c697d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7885
        },
        {
            "id": "e2b3d4d3-51d4-42ff-8276-05bcbab371cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7886
        },
        {
            "id": "feb826dd-7443-41fb-914a-258b142e0872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7887
        },
        {
            "id": "bf394d9b-68d5-498a-ab3d-9f182d5e3adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7888
        },
        {
            "id": "9830550d-2816-48f2-bad1-9736b5536213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7889
        },
        {
            "id": "7e40668d-f6ce-458e-bf25-a29c88e96735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7890
        },
        {
            "id": "8caa25ef-7d98-4bf6-8e16-29b8bb47038b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7892
        },
        {
            "id": "7e7e54da-3462-4e24-a9dd-442da5b7e80a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7893
        },
        {
            "id": "01745c3a-6169-4d68-8796-639c1d9456f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7894
        },
        {
            "id": "818cb9d4-1162-4c4f-9051-c45fd900a7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7895
        },
        {
            "id": "25ef04bb-3e29-4b07-a7a7-60a80029e454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7896
        },
        {
            "id": "1d294604-c669-48f7-bd84-bdf7e91e9230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7897
        },
        {
            "id": "a0f212a1-3184-453d-b45c-0685753e7dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7898
        },
        {
            "id": "31153bdb-c4c1-48c7-9178-d874f322115a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7899
        },
        {
            "id": "ddfc4188-a243-4e90-bee5-0d43f31ac9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7900
        },
        {
            "id": "729377cb-ded3-452f-a59e-386e68223101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7901
        },
        {
            "id": "6ce22f73-0021-40e2-8ab0-8e1d41d84005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7902
        },
        {
            "id": "00061aaf-05c2-4983-bdc6-e0481bcb1852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7903
        },
        {
            "id": "37055d60-c13e-4349-a8a3-4ee067cb92b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7904
        },
        {
            "id": "23361142-a74c-40de-b34d-c669a01184d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7905
        },
        {
            "id": "2d0a203b-24aa-4ec0-a925-689d399526c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7906
        },
        {
            "id": "9cb4ea5c-2553-404f-a66f-291b2c1549fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7907
        },
        {
            "id": "142b3de1-644c-494e-bdb7-c5ab0f59b304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8208
        },
        {
            "id": "7d9a8878-cefe-4e42-9da1-dc81a15ee433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8211
        },
        {
            "id": "bda59fa5-da37-40ed-891f-ae2901dc8fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8212
        },
        {
            "id": "c3873301-4bdc-423d-88ed-37c0459c5d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8213
        },
        {
            "id": "4fea023c-71b5-463a-b98a-cd9b4dbb6d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 8216
        },
        {
            "id": "fc584a39-7119-4d1a-8cc5-0987294a0a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 8217
        },
        {
            "id": "29699121-6448-4e43-91df-e05438f6976b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 8220
        },
        {
            "id": "9659322f-af05-4aa8-9a8a-b61f623cb59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 8221
        },
        {
            "id": "75607e4b-4b3f-4844-bf88-f20a75384183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "150c3fa7-be3e-4ccb-862d-821a97560d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 45
        },
        {
            "id": "46d5b9b5-db83-4645-a763-2cf044a5592c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 173
        },
        {
            "id": "3334c29d-7378-4fe7-8e17-e16ea2aa2b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 8208
        },
        {
            "id": "0a162e94-9382-4251-841d-636da935776d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 8211
        },
        {
            "id": "8c414fe6-84c8-47e0-b2ee-66ac6c2bbc73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 8212
        },
        {
            "id": "efe2a662-e7dc-4ef0-8559-39fc082b47c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 8213
        },
        {
            "id": "952b3efe-270e-4038-b6b7-b59a2806ab74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 111
        },
        {
            "id": "02ab83d4-f14c-45bd-99c6-9bb6a543adc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 117
        },
        {
            "id": "f0f95ce5-33f0-40aa-b262-40ba21eb6a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 242
        },
        {
            "id": "ec55b351-f867-4c30-914b-825aa481fd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 243
        },
        {
            "id": "61ee1013-7f4a-4eef-8b2e-b4261c71e78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 244
        },
        {
            "id": "cf83afc2-6e77-4be0-9a81-3acd2650113a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 245
        },
        {
            "id": "ff71fe0c-8610-43fe-92d1-8ebc53c48331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 246
        },
        {
            "id": "ef9fcda9-d0df-44c9-8581-f0328da677ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 248
        },
        {
            "id": "aa2a76d9-e07b-4feb-87e6-52b1233cf95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 249
        },
        {
            "id": "d69ac995-f97d-4430-8458-906a67c71e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 250
        },
        {
            "id": "f692b6a6-c002-4048-9cdc-341ebdb941db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 251
        },
        {
            "id": "44c8a6aa-b417-4c9d-8343-ae34d7c6b9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 252
        },
        {
            "id": "293d9fcd-e218-46df-8fca-30886a22091d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 333
        },
        {
            "id": "8b41ead4-2435-423a-9663-401b9ddef28d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 335
        },
        {
            "id": "79bcc9a5-fd8b-4b6a-a530-6224f28b3088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 337
        },
        {
            "id": "0921cb85-966a-4ecf-8c4b-5996ed32450b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 339
        },
        {
            "id": "8c4f26b8-b351-4de8-81ca-56b1814685df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 361
        },
        {
            "id": "be54e6bd-1b50-40bc-933b-2bd965ba568e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 363
        },
        {
            "id": "ef8e4e7e-75dd-435c-bf17-2a31c865c83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 365
        },
        {
            "id": "dca35f6a-a71f-4b55-b672-b2c2b515029d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 367
        },
        {
            "id": "cd55789e-2129-4df2-956a-2b17fc113422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 369
        },
        {
            "id": "4cf5588d-0778-4734-b5ba-1cbe54a28312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 371
        },
        {
            "id": "8c0ba180-ab21-42f7-8ba3-e919dea50815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 417
        },
        {
            "id": "56b06b94-8978-412e-bf0f-0762a0ee6807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 432
        },
        {
            "id": "52f9a472-3dc3-44fa-b8a2-4d84eaea9ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 511
        },
        {
            "id": "81baef29-1080-4ca4-8cec-07af4eedef81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7885
        },
        {
            "id": "38a0e241-fc5c-4936-ace6-5c5807a3c9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7887
        },
        {
            "id": "daa040b7-116e-4b5f-812f-8eb1074897a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7889
        },
        {
            "id": "dab9c4a0-dbe0-4458-a5da-ad8687bad708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7893
        },
        {
            "id": "ca4f660b-3aed-43a4-9f68-00cc640e6909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7895
        },
        {
            "id": "058c6907-b81b-455d-a5a7-b7e4f1c7c62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7897
        },
        {
            "id": "1d7b3727-042f-4c9f-8df8-4c4faefa24ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7899
        },
        {
            "id": "f8388725-7ddb-476b-a026-403686cd1195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7901
        },
        {
            "id": "b755e35b-840a-490b-9d62-71b2d5a4c244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7903
        },
        {
            "id": "3fb839d9-9a30-4656-97df-90a0f3690f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7905
        },
        {
            "id": "ea47fccd-aa34-40ef-bd7d-39011b0c30b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7907
        },
        {
            "id": "32470ec8-301e-42ce-8b2a-5984130f553d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7909
        },
        {
            "id": "c153b928-a4fe-4bf9-86a8-54bbd55d0b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7911
        },
        {
            "id": "dea08aec-67c9-43de-be6a-1752f856bed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7913
        },
        {
            "id": "77d7748f-48bc-4eda-8408-e064eee97917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7915
        },
        {
            "id": "35a0e8f4-02e7-4ccb-8068-a7b2a88f36a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7917
        },
        {
            "id": "2abacbd2-1ad8-4fdd-8ed8-f56bd7e41774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7919
        },
        {
            "id": "fda9fe85-b224-45d3-a796-575259cea75d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 7921
        },
        {
            "id": "42478cd0-7482-4643-9fc9-c27ce44597eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 111
        },
        {
            "id": "31ce898e-1344-4942-a1e3-c8f7a4e9a4b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 116
        },
        {
            "id": "ff7c7c57-b5c6-4f75-8901-49ffecc50137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 242
        },
        {
            "id": "cbaf0a86-97ce-4cad-821a-23278ab1e4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 243
        },
        {
            "id": "6fe3f232-4a8e-405f-a918-85b33b675f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 244
        },
        {
            "id": "15ede2e6-9dc0-4133-b95c-ec195a593dde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 245
        },
        {
            "id": "02b39a71-2b36-48d3-af80-4749096ddbd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 246
        },
        {
            "id": "7d6a04c8-d5fd-41a5-b598-9ea3049014da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 248
        },
        {
            "id": "97ca8071-56d4-4108-ac5b-932e7b41c71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 333
        },
        {
            "id": "364d3f9e-997c-4213-a31b-95c35ce1710b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 335
        },
        {
            "id": "7482ec2b-16d4-4e99-bd5d-5c1f0a77bceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 337
        },
        {
            "id": "12136122-0c60-490a-a463-a1b5505c6e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 339
        },
        {
            "id": "cec16606-13e0-4b73-a10f-3a0164b1e55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 355
        },
        {
            "id": "734a0d69-b6b9-4b49-83c5-13d1e72f2ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 357
        },
        {
            "id": "9ed34bcd-a38d-4f73-b3f1-0eb3187033b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 359
        },
        {
            "id": "709cc010-2abc-46c8-afc1-bea860a1e257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 417
        },
        {
            "id": "a4ca08c2-8645-451d-a49d-e0e4d711d369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 511
        },
        {
            "id": "d0329cfe-e6d8-4f42-935b-dcc3fb28a4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 539
        },
        {
            "id": "265da3de-a762-4682-8769-5064329e57b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7885
        },
        {
            "id": "c4b97453-8ef7-4d89-a540-1227ba3644a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7887
        },
        {
            "id": "c50df04c-93c3-4357-9ec0-026d0c84f465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7889
        },
        {
            "id": "f63dad38-7f7e-4837-9825-177137e71d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7893
        },
        {
            "id": "e2e78ea1-b988-47f4-af25-7235b5882117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7895
        },
        {
            "id": "23507040-5b0d-45fd-9730-a3cc48e310c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7897
        },
        {
            "id": "50bfbf6a-ef3b-4dab-b316-b362963ec4cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7899
        },
        {
            "id": "a3f3be59-756a-4855-bcc2-88f28a01711a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7901
        },
        {
            "id": "c7bc795a-266a-453a-a152-bd29a429f7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7903
        },
        {
            "id": "bbbe53ea-8805-4e77-b9d9-36d9b6fd8842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7905
        },
        {
            "id": "8f2b8faf-5f8d-4654-abf6-e41638a4c996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 7907
        },
        {
            "id": "33106656-f7e5-4709-8526-6fd36ec42b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "73b882df-ddf8-4550-aaca-1debb6e6c9fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "70537284-5942-45b3-907e-cd7af05923c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 239
        },
        {
            "id": "334310ab-157b-4a3f-a9be-307142843a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "a1c84863-7706-4c66-a00e-8dc01b5f1c5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "9bfaf0c1-e7d1-4a3b-bcb2-c5bd124c9a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "f6ddc50e-24a6-48ee-b657-c057e96dec58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "a4580cd1-0428-4369-958c-91a7e6daf472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "019646dd-de0a-4838-82ac-f53993712d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "6082d400-429d-41d1-8267-a4c286bbc882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "4642f2a3-228a-4c1d-93d6-8830b87936cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "bc44552f-2f9d-41af-b19d-a9d8dc080103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "dd6c1c29-564b-4014-9216-28d1db5dc8ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "9d5eb2a7-e301-4114-af5b-78afc94734c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "1734e4b8-f55e-4841-b5b7-8eddac22914e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "5133f2c7-6f59-4ba1-84d7-3ca7b6601a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "eea05ded-bf32-4076-a40f-5ad210327e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "6ce05622-c508-4310-b1b5-d6da633c22b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "39182b46-a220-4d69-b7d1-46b8eb8f59cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "c841048a-8726-4f14-9444-aef620d78756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "6670b97c-8a7f-454f-8662-cfe1dc45eeb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "59f99fb0-5a86-4bbb-8a6f-4cd7a7ea6e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "e8f032e6-439e-4d14-b101-e2113ae8c601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 242
        },
        {
            "id": "cde22c4c-d350-4a7d-8335-64128f2f8744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 243
        },
        {
            "id": "29c85091-796e-4092-abbf-4c8848346bc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 244
        },
        {
            "id": "5673cb42-d4c0-4b21-bbd5-719463ab3a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 245
        },
        {
            "id": "5c519b77-d676-4274-a425-9a39ccaed0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 246
        },
        {
            "id": "7f1a7643-765f-4699-96c5-af8fe519992a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 248
        },
        {
            "id": "4f8e9859-59ab-4a77-89c6-7d4f3c8f9aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "5b224098-37a0-4613-9f9f-aa6457d641cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "042dc5cf-0d2c-49e3-b2ac-d8580549492f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "d912ebef-614a-4398-bd2b-8124f70430e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "42c6330e-bb56-4d9b-bec1-08d1cfb0ee28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "09dd7ce5-b4bb-4e68-9184-06c325c37edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "5a7f2283-68d1-4f29-9a3b-609200959538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "b6c168fe-d375-49fd-8922-2725db556445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "66a2c34a-1b87-4364-8ea9-e592c525b1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "c7c66327-f659-452c-a0b1-b0ddb62fdcff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "06aed270-283d-4972-bc87-0a0589932fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "18e3f2b7-d73c-4b98-aaf2-78015d397c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 333
        },
        {
            "id": "38fc5e95-f60c-43df-b314-7277a2163375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "73dd7290-6d5c-4fad-929c-373fae68579e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 335
        },
        {
            "id": "8a0e2ef0-0254-4bc3-b5f1-d981bb8735f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "e833d5da-198a-4547-ba05-3274b399c3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 337
        },
        {
            "id": "84934bc3-7c98-4dea-b03d-c90f6d47b758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "e982824b-72f0-413d-b8d0-495aa355da52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 339
        },
        {
            "id": "cb7382f9-bd9a-45bb-bc2d-760965109b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "5b256ce2-c548-4065-ad9a-315718fe1ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "b65ea467-b194-4550-8952-b43be9fbde41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "52fb4820-292c-4391-b5b5-7f7677951989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 417
        },
        {
            "id": "e067387d-680e-45c8-a8db-3d419b46e3fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "08b8c282-9204-4b00-9035-d5497f1e02fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 511
        },
        {
            "id": "3b01f7ab-7dbf-4ddc-81b8-ad0c2c33949c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "a78485d6-3a49-424d-b869-36a37a545a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "dbae4f41-fa0b-4a42-8316-789a262263eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "da591d7f-a784-4d5b-9790-3f2219988932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "f17926a3-c11b-4703-bb7e-356994c0b598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7885
        },
        {
            "id": "c2e1fa00-03fd-4438-84f5-2a476e8e2964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "0556f627-42c6-48df-b77d-6ea2e9d441a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7887
        },
        {
            "id": "4c4c16fe-9913-4763-a76a-ee86e5ca36cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "3f29d307-504d-4112-943b-e05cbb256458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7889
        },
        {
            "id": "0eb28289-fd64-4ca6-8102-9757646d4562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "4a28ce10-5bf6-4561-9ffe-5885b8a7e090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "8eabf111-b50a-4ad9-9c58-b7c73b5548d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7893
        },
        {
            "id": "53ad894a-bad5-47b2-af4e-8dff58e1cd49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "c004346e-d24d-4025-aa75-875c4d62eb03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7895
        },
        {
            "id": "21cc9000-97ed-4fcb-a890-7d1fd9b3f02a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "6d5189f8-0631-486a-a586-d07d254de000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7897
        },
        {
            "id": "b4f95013-bddb-4ed1-8ab5-9c6130d08a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "e2449a49-8ce4-49b9-ad20-4e34eb62ef2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7899
        },
        {
            "id": "e57d93c5-60ff-4d57-851a-f05656b94189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "8fd69211-556d-485d-a3ef-bfa1e0537868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7901
        },
        {
            "id": "3df48a5e-5912-4211-bd6b-ae448e68bd16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "c7f99253-9457-414b-8c74-6fcf37da6788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7903
        },
        {
            "id": "67284e51-0fa9-467c-9214-2cfc9397f4f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "c77531a6-73c7-442a-b9c7-1031cf3e7fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7905
        },
        {
            "id": "c1645f7a-1281-4219-9040-0d1f6328051a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "ff3fdc56-3ecf-46a5-a4f1-69c498916646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7907
        },
        {
            "id": "829b39e4-af7b-4317-99d7-47308b6ba01c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "92c56420-d9ee-4cd1-af84-2aa87e2f80f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7925
        },
        {
            "id": "45e4a158-13be-4923-a41b-8ec1626b2e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7927
        },
        {
            "id": "8ef1d3f8-ec65-481b-b935-5a574a975183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7929
        },
        {
            "id": "669e7964-8bbb-4b18-b92d-18d13a6556eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8208
        },
        {
            "id": "6002bb3d-1223-43e5-afa0-2ca809553e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "52df1461-2112-4406-b4e3-75927f0f3574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "d8f52fe0-65f1-4eb3-bd0b-3bf85b81768f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8213
        },
        {
            "id": "251b1c28-a07c-4f8c-98b5-e29a01272a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 42
        },
        {
            "id": "3d874e6a-0757-4fe5-8877-f5af7ff81417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "8f9e4233-dafd-4dde-9ba3-ec5e1528248a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "63a1e07c-628b-40ab-8941-26ef65aa744e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "b951565e-95ba-4881-8595-465cb7882fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "f0468d4c-2a57-444a-9eef-1ec9b21914fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "87608d4a-b84f-4027-97b1-e7a742a9b53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "f1f7c433-cd59-4e7f-90ab-9df140dcf71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "b545f78b-ff0c-45da-b1ff-280b115136e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "0feadfd9-c36d-45d3-8cb3-066e959f3695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "2a7e5e8f-f17f-4a1a-89f8-d6564c2f7514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 174
        },
        {
            "id": "6362491d-a811-421b-b47e-5ba40fbf15bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 192
        },
        {
            "id": "f733cc53-2bf9-49e1-9e2d-62b83382f835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 193
        },
        {
            "id": "885d301b-5a4a-45b5-ac92-edade8edc505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 194
        },
        {
            "id": "fe139aac-37ea-4e04-b998-6aa4601771e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 195
        },
        {
            "id": "d1a12914-211c-4075-867c-eb54a7fa0587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 196
        },
        {
            "id": "9723af98-549e-4411-b775-c6e3ff010460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 197
        },
        {
            "id": "39ad171e-41e8-452d-a63b-dff2fb696da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 198
        },
        {
            "id": "6e1d95ab-75c6-4477-8753-729f97f517e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "28730640-3f60-4094-96bc-bd4d8322d77b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "acce60c3-6d10-4c32-aebd-c0934cd02e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "7cf4f7db-7943-4d08-a493-9e4a00eba68a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "3260e7e8-6443-45ae-ac04-5f5d23457c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "a1f1703c-953c-435b-862d-bfdb4758a31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "2781e627-b6d5-446e-ac17-1ed8b926b424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "13bf3ec0-8daa-4d09-a386-0ee3222aa8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 256
        },
        {
            "id": "9038cc91-5cf0-467a-8531-f6c276c5819b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 258
        },
        {
            "id": "937d608d-67f6-40a4-a51e-80a124d5f09f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 260
        },
        {
            "id": "6687dcb6-db01-40d1-89d5-d3573e351850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 329
        },
        {
            "id": "c6ecf356-0b2f-404f-9df2-d2b2695b6c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 354
        },
        {
            "id": "a048d9b1-34e1-4151-8d3a-20a38d3f21ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 356
        },
        {
            "id": "5762f4ec-f7b3-4c46-9dfe-002e17570776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 358
        },
        {
            "id": "1401d453-4778-4d1b-a2a2-5917d80ae715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 360
        },
        {
            "id": "a7637eb4-842c-47bf-97fa-a2dc777d53d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 362
        },
        {
            "id": "9a53f2d3-0b7f-4571-8b06-64f927a41e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 364
        },
        {
            "id": "58bd4263-8e5b-4311-a53f-dc9c794dc6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 366
        },
        {
            "id": "a9b694b9-d675-4c86-84ce-a1c2cb8ed051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 368
        },
        {
            "id": "1fb429eb-d752-4370-af64-b34ce70cc335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 370
        },
        {
            "id": "421608c1-7791-447a-a954-6a83b3cb71ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 372
        },
        {
            "id": "7421f3ce-eea9-4de4-a659-f35fc22532df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "ca6bce7d-869d-459c-9c43-35352c9b19fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "3e045a50-5542-4d1f-b8e5-953dc3d12cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "886582a7-4aad-4f1b-ac4f-09abbecbe040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "8708375f-7546-48a7-aa8e-6e9c32bfbf82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 431
        },
        {
            "id": "597be1f5-fa3b-4204-9d42-a6c61dccf75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 506
        },
        {
            "id": "79b5c902-d18f-4037-8d3a-dfdca3fba5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 508
        },
        {
            "id": "79f89d99-8bb4-47f2-aa8b-67512a05ed2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 538
        },
        {
            "id": "8e5660b8-7ace-4fd6-8c96-880e2c3f1914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7808
        },
        {
            "id": "7f6d455b-dcc9-4404-b528-0b87da1e49fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "6a6c83a5-cb6a-47f1-ba50-26b0dc5cf515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7810
        },
        {
            "id": "617053f8-eeb7-4764-a83d-915b1c5029c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "58f2e576-ea78-4469-b928-613bc1397637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7812
        },
        {
            "id": "2b9f05f1-745c-4f20-bd73-bc98b707bb62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "12b33d3c-7070-4c97-bf71-42018c6facae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7840
        },
        {
            "id": "97bcc644-68ba-4c09-b4fd-93247978daae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7842
        },
        {
            "id": "37d56e8c-cee1-44ea-ba24-6d75073ce9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7844
        },
        {
            "id": "827f5ec2-73de-4819-925e-7bbe7c6c6019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7846
        },
        {
            "id": "d1749c88-4db4-43ec-9a11-a40654181aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7848
        },
        {
            "id": "81b8bfa6-2a6b-4b5f-918c-efb27bc28395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7850
        },
        {
            "id": "48322f39-4da9-4932-9b9a-6a922fd9adcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7852
        },
        {
            "id": "ecced3f4-a94a-4c30-a76f-3aa65ea24da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7854
        },
        {
            "id": "2f6ddaee-1e66-457c-84d5-b31019ce1519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7856
        },
        {
            "id": "316af6d0-0fc0-45c3-ac6a-b440b8e20cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7858
        },
        {
            "id": "42982c43-e272-4821-acdc-a5e74f8b89f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7860
        },
        {
            "id": "9cc62d93-2d54-4fc9-a5f9-941667e49560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 7862
        },
        {
            "id": "0c12ee62-f524-4c5a-8032-80da170bd0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7908
        },
        {
            "id": "97ab50ff-52d1-4072-95b7-936d07afa678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7910
        },
        {
            "id": "97415bfd-1827-4c0e-837b-b2b999a373bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7912
        },
        {
            "id": "53cd9898-643d-460d-a97d-e4e3cd5b778b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7914
        },
        {
            "id": "d863f002-7e37-4dd2-bc59-2f1308aec7a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7916
        },
        {
            "id": "363094e1-79c6-4d67-8b68-4861698162b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7918
        },
        {
            "id": "a6eae768-7cb7-4105-9c6a-80ee3195785c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7920
        },
        {
            "id": "27c72f87-9e04-4082-b84f-1b06f30c7aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "a3f5f3eb-f372-492d-bc4e-b09bb98ba3d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "65ff84b8-e781-4951-ad31-212c3168f5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7924
        },
        {
            "id": "a37dd165-5279-42f5-bfdd-1376d317c28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7925
        },
        {
            "id": "8a38d21b-7ff8-41fb-886f-460c86d1fa48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7926
        },
        {
            "id": "2f01f5f1-493c-4a76-b146-16384b3d2b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7927
        },
        {
            "id": "f374140e-848a-45b5-8db6-9aa4d93263c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7928
        },
        {
            "id": "372f81a3-3160-4ff1-b17c-a97368c357f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7929
        },
        {
            "id": "bea43a3d-3c2a-4233-bc49-fbe20254b6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "696cb50a-ffd0-45a3-ad3f-a792ec835197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "cc5777cf-d2ad-4b39-81d7-40744e48703d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "363baa95-b732-44a9-9e82-f000186121fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "2798c9ac-fd99-40e9-ac54-b26d5632de84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8482
        },
        {
            "id": "bd0c06bf-7abb-48d1-8e6c-64dcc79c5b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 117
        },
        {
            "id": "0aaa827b-00a0-4002-beb7-91f82cb4929a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 249
        },
        {
            "id": "5c10b975-be7f-4621-9d11-6240a1d6fe4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 250
        },
        {
            "id": "3fd5aa12-be77-4fb1-9e33-5d960fb599d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 251
        },
        {
            "id": "0b0feacf-8086-4a3d-b4a7-9ed86aad2723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 252
        },
        {
            "id": "7eff936d-7892-47ff-8e88-a3b86b9a294d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 361
        },
        {
            "id": "315d46b8-6c14-4deb-a916-57312d0758c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 363
        },
        {
            "id": "571df78f-61a6-425c-bcc2-274cba54bd71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 365
        },
        {
            "id": "f64140e6-1ef6-4595-9796-d7df1df15b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 367
        },
        {
            "id": "f569e85f-c7a6-42a9-b6aa-ab01cc3e2dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 369
        },
        {
            "id": "7007fb3c-3de4-482f-a811-a80e1109ea8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 371
        },
        {
            "id": "ab38e0c5-6875-4481-8daf-e5f12430545e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 432
        },
        {
            "id": "0741ad00-f3b9-447c-a111-0d60fd513b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 7909
        },
        {
            "id": "dbed09b5-0bed-435d-91eb-b94f1fd0118a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 7911
        },
        {
            "id": "78664aba-69b3-4ead-9ae6-e64519d3d909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 7913
        },
        {
            "id": "af6fad17-989d-426a-ba46-90a87edba8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 7915
        },
        {
            "id": "04b85d1c-2ec2-404e-801f-727e35f52c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 7917
        },
        {
            "id": "18d6a1a7-dd07-49a0-9ade-00acc3380261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 7919
        },
        {
            "id": "efcf71fd-8232-4785-acc5-96d15d7d36de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 7921
        },
        {
            "id": "05c667fc-3920-44a3-a62b-407e51cff53b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 44
        },
        {
            "id": "52e63846-d05c-4706-9aef-2636e3a1bf2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 46
        },
        {
            "id": "c8515d1d-9df1-4c30-99f4-30572c774be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 8230
        },
        {
            "id": "9f99842a-dbd5-46ff-8736-5d608423099a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "508c5b17-691d-417d-9b4a-027d9a57ab60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "c10ff5dc-f422-4434-871d-436b56bea54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "90456856-4718-4a1f-bd02-e4d2c5532809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "375e8aee-77e1-400a-b54a-e50f3cc84615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "067f904f-33c9-4abd-a8e0-23f5b99870ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "93a2f127-1bb5-4782-919b-3959a488c56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "55cad731-a002-4870-937a-3b0540538920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "a3fcb10a-2daf-40f9-87c7-11520fb9e1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "5709de2a-34b8-4937-91c5-145279580da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "c82dcd92-fb58-497f-8b8a-6a310024f786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "0fe08544-1f49-429a-b708-e7c3a99c0622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "1994e87b-2f91-436f-8460-25951fa0dd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "f5d136fc-7d24-49cf-894d-cdb7ae22ddfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 198
        },
        {
            "id": "7569f8ba-cb75-4ed2-bd3f-094c504d1360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 221
        },
        {
            "id": "4e10d936-65e7-493f-80b1-33af28835f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "8244402f-0413-4809-aa1b-7e2a2e7ed9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "f4876dc7-8b01-443f-b30e-5eef4d77a52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "4bc12fd9-4487-42d7-9834-7b739e55d7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 372
        },
        {
            "id": "d829b47c-83a9-4652-a703-0750c63afe47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 374
        },
        {
            "id": "e5399129-4138-4c39-83ac-6f3b2de15efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 376
        },
        {
            "id": "9b47bb89-1ab2-4b95-91d8-4e0ef2445a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "0e5ce6c7-91cb-46a7-b30f-3eb0473b0976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 508
        },
        {
            "id": "ce56c9c6-0418-49ed-86f4-8623821240b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7808
        },
        {
            "id": "195c4e19-89fd-4eeb-87c8-9cfcb76abe80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7810
        },
        {
            "id": "09c74ba7-5bb1-408b-aad5-2cee7a7a5d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7812
        },
        {
            "id": "0b5323c4-c003-47b7-a484-b802f7125144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "8939316c-b374-4f9e-bf5c-e26342ee7307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "b05b0f18-c454-492d-bdb6-255ce2e636eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "9e5b0e1b-c850-419f-81b9-4a9c50a9b3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "3f26f4e3-e2b7-49f8-9528-72b1b581a18f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "ef7410b0-66f6-4375-a0c8-97840470dab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "1741b07f-fb49-4a94-9834-b5a35f740173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "d1f13ea8-b449-4e75-ad72-af0fc623c058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "fddc806e-537a-4cbb-97d3-9660166529e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "2e03bc63-161f-4fc0-933b-c42247d007ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "7c0029a1-c507-4b29-aa7b-df64c24a329a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "69e6445e-e192-434e-8194-49e918b85846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "35bd598f-3de3-4c90-b0f9-e3ab7aca2ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7922
        },
        {
            "id": "316aba27-7f4e-4a93-a81f-ebfa802d2b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7924
        },
        {
            "id": "a439cfe8-7940-4884-85e4-77a186fd5391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7926
        },
        {
            "id": "099a7643-9b82-4833-8164-b28ef8954e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7928
        },
        {
            "id": "be612873-ff33-4201-bd97-cb60808cbca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "d1622aa5-7433-44ce-a5b7-28ecd0145718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "edd05707-9bcc-418b-adfa-e478e6b21401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "43d61c43-628a-43b6-871f-1cbeb428ebb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "1eb9ce5b-3db6-4320-ba41-998f610c71a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "4a835be5-4e68-4287-a440-f8f7da700c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "1596cce2-1f64-4e54-9765-3ce95a881b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "f6bebb53-646a-4b0b-a754-61ee80a70ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "b23f1612-21d5-43e3-8a78-ae0f04accdf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "31903c8d-8515-45a1-b339-9a1cae8754cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "8a2ba560-2324-48df-b5db-7d815b31a66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "af41dc69-42dc-4d6c-9861-e1172979b426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "c7d15721-cc83-4035-8f25-c58c48731c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "f3b456f8-15f2-4005-be85-e8b969669adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 198
        },
        {
            "id": "0a030baf-41ab-406e-9563-df25e41f1c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "e808dfef-5edf-49ec-8609-4f03c24bbd20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "269c0ba9-c8cb-4e7d-9fe8-574e7665e26e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "b1548cfe-dca8-4289-936d-51cf4aec13bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 235
        },
        {
            "id": "03db991a-7e6c-4b5b-9a4d-049a1c17e708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 238
        },
        {
            "id": "31a073a5-7c09-4a8d-a998-4437d47c9694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 239
        },
        {
            "id": "59cb9028-ed12-44f2-b272-c53ba3e6c39e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 240
        },
        {
            "id": "bac2a55a-25c2-4da9-8895-2858d1e68c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "52ce56bd-5024-4ceb-b62b-290e7c27183c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "ed355a1c-7f0e-4143-a631-fffd316e6419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 244
        },
        {
            "id": "f46b6402-0577-4a9c-9017-b3ffe0a05c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 245
        },
        {
            "id": "145705b6-3664-4512-8f40-5e9d8bba5cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 246
        },
        {
            "id": "5a72b5bd-49e1-4139-9d72-421a89682eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 248
        },
        {
            "id": "2c64a819-8cf3-4027-aa5e-6cc0957e04ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "bf5814c3-ddcc-44ed-81c7-e22f6a313b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "00c0a7f3-abb4-4561-8bda-ddf83f447d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "a20ec3d3-a8c2-4091-a61e-3a588d6aa7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 271
        },
        {
            "id": "97818d30-3991-44f8-8afd-81cde315dd7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 273
        },
        {
            "id": "b2dc14e8-ac29-4430-8b4d-b2e9a2d808ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 275
        },
        {
            "id": "7df7d0b1-a15b-435c-90c4-6a09adc9426e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 277
        },
        {
            "id": "302704d3-17ba-4fe9-a466-8969f75faba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 279
        },
        {
            "id": "8d0e47ab-83e6-4586-a311-28c6f7db7c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 281
        },
        {
            "id": "acd1901d-791f-4629-90fb-9735d66e6a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 283
        },
        {
            "id": "437cf383-e2cf-4b1d-8a5a-b78b40b8d8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 297
        },
        {
            "id": "de2a934d-4080-4955-889a-f3a08f7acfb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 299
        },
        {
            "id": "747a332a-c0aa-4fef-90d6-d292902c700d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 333
        },
        {
            "id": "ee48cf91-488c-43a7-a2f0-235832790b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 335
        },
        {
            "id": "0ce04f51-1292-4045-8610-275ac7d72967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 337
        },
        {
            "id": "4fd377a6-332d-4669-a1d0-77241bde1b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 339
        },
        {
            "id": "51b365fd-f1d0-47b9-8ef6-6b2ada615e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 417
        },
        {
            "id": "34df866f-d4e6-494b-af12-03f9c8622014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "98b684ef-d39b-4851-878a-f5b6c8a5d6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 508
        },
        {
            "id": "120d4d26-49ee-42b0-a734-e4550ade13e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 511
        },
        {
            "id": "48bb04a6-e162-41d9-96df-5e78035266b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7840
        },
        {
            "id": "510f10e8-9e7d-4b52-a534-77030b06a08d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7842
        },
        {
            "id": "7e137849-108a-468d-89ae-356c98cca26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7844
        },
        {
            "id": "d7372557-fd82-41fc-8143-245b676aece7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7846
        },
        {
            "id": "e88aa6d0-9d88-48dd-aa8e-fd428727cec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7848
        },
        {
            "id": "cac2d7c0-8739-4512-add2-5f7ee361f4a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7850
        },
        {
            "id": "99a9ae92-00ad-406c-90b7-98c05d04c123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7852
        },
        {
            "id": "abde865e-eb21-45fa-a8d9-3c8e52dc2443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7854
        },
        {
            "id": "e24bd7bd-ac29-4d8c-ba43-6856c009c772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7856
        },
        {
            "id": "cb1b9153-0e67-45f9-9a85-f079c9dcb70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7858
        },
        {
            "id": "8bdd88d3-9662-40a5-bbd2-c0d9b4d0df8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7860
        },
        {
            "id": "1b748540-6406-46d0-896c-2594d2eceb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7862
        },
        {
            "id": "2a195edd-e25a-4460-86db-497d8ee606eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7865
        },
        {
            "id": "1925b40a-c101-4da9-8dab-247b4ca049ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7867
        },
        {
            "id": "44724d8e-5bc7-4be8-a0c9-ead827846ad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7869
        },
        {
            "id": "7e5841b7-6bf6-46f3-a9cb-0c120b79ba2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7871
        },
        {
            "id": "df881c65-e7bd-4283-affd-9460706aaa43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7875
        },
        {
            "id": "9d42c392-7068-4197-ab56-46629b4caaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7877
        },
        {
            "id": "25310f03-6023-4928-9891-0eb61de9d3d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7879
        },
        {
            "id": "e4a9ec9b-0a9c-4c2f-95e1-707f46f80428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7885
        },
        {
            "id": "933b88f9-f841-4553-9223-44d0e29be8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7887
        },
        {
            "id": "63fa2dca-4b4c-4c6b-81e4-1888266f59b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7889
        },
        {
            "id": "49ba6a88-dcdd-4b3d-a7af-4e3a40fa1d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7893
        },
        {
            "id": "8b7c68aa-ccbf-4fed-882f-a5b25301b869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7895
        },
        {
            "id": "92bd8b12-38ba-4b58-a205-de0c88a850a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7897
        },
        {
            "id": "cf5c8359-3bd2-4733-867f-46be0caa16df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7899
        },
        {
            "id": "7adcd012-1cf7-43cc-935d-a3ece8a2c4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7901
        },
        {
            "id": "4b41cb85-e993-4d0e-9312-fcbfd61e1902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7903
        },
        {
            "id": "d4e4a008-4fef-4c14-a980-a61ad9b4f578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7905
        },
        {
            "id": "ae0645fb-3645-4c48-af58-5639245d62e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7907
        },
        {
            "id": "5398d92b-6aea-4e0b-a193-9808a4953918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8230
        },
        {
            "id": "5351e8c1-7dd9-4c06-bb63-6eb8820c0916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "df95b29c-b449-4268-a51d-80cbb3e8dcc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "7c03fbf4-aac9-4ddc-9eaf-5d06084af711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "2a2130c5-344a-41d6-a5e3-72972df7a094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "d5a3787c-76b0-4af0-8e01-f32e2d282b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "fbb6f329-4bf6-4019-a083-d399806484e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "f253f851-956a-4be2-b902-51c36d923a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "c843b8b0-ae47-48a8-895c-8fb1b78e44a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 192
        },
        {
            "id": "136c15e8-bb7d-401f-806d-806e4fe18508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 193
        },
        {
            "id": "054cc399-426d-4d72-be1c-9485a67ec1e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 194
        },
        {
            "id": "29c4f733-4382-4e10-ad98-fb221951a37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 195
        },
        {
            "id": "0dbf4b77-60d2-4b78-b75e-61ab9c65c5d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 196
        },
        {
            "id": "be610b45-651f-4b93-bb8e-80f0e04c96d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 197
        },
        {
            "id": "9a143d0a-4575-489a-a413-daf45011b034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 198
        },
        {
            "id": "d417aede-dbe4-44ac-b200-9442b62d4846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 221
        },
        {
            "id": "b27482e4-01f8-4773-9bd4-8ea85c608be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 256
        },
        {
            "id": "3744a2ee-f146-4f07-a76c-622dd4b2fe7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 258
        },
        {
            "id": "1c038724-c019-40a9-86a6-222e9c9df6eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 260
        },
        {
            "id": "464949d2-5ecb-4348-9635-f8c220dcd5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 372
        },
        {
            "id": "f67e4936-1bbb-49fc-af86-6af06115d5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 374
        },
        {
            "id": "432b1ff1-dcce-4bb9-bc7c-3bcbe7e4f716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 376
        },
        {
            "id": "9256c5ac-e89b-48da-86b0-6ea5475ea806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 506
        },
        {
            "id": "7ba820a8-3b5b-4a06-a8a1-5d836aa8422b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 508
        },
        {
            "id": "99b816d0-474c-4331-8342-b813a1425dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7808
        },
        {
            "id": "7c72c7f6-cc4a-41a8-bfe2-52090a48dcaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7810
        },
        {
            "id": "b328863b-47a0-420e-8f22-22b0dfcfdb0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7812
        },
        {
            "id": "09dbfacf-7690-4fad-a73d-e36944c8553c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7840
        },
        {
            "id": "c675cd3e-46f3-410a-b14c-2aad030c07bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7842
        },
        {
            "id": "0aa8054c-eb6e-4f7e-a532-d86c2ee89e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7844
        },
        {
            "id": "55f71830-b51c-49d7-b1f0-7514e3a65158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7846
        },
        {
            "id": "ff04391a-d6db-4497-a3f3-bf81d285b5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7848
        },
        {
            "id": "734929b8-c42a-4db3-ab0d-c0f415b03d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7850
        },
        {
            "id": "b8432b2d-5ef5-4264-8dc7-8816ea6dcdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7852
        },
        {
            "id": "08442b80-8fd3-459b-881a-f96b66267d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7854
        },
        {
            "id": "61e5a1dd-158a-49c7-8cf4-7354cfcde861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7856
        },
        {
            "id": "84ebe55d-61b7-4ebc-ab70-c804396b102a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7858
        },
        {
            "id": "6c36b2df-df69-4245-b146-5b6187fb626a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7860
        },
        {
            "id": "1a313d16-13fc-42ff-95d5-9d75a34ec4e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7862
        },
        {
            "id": "462fc6e6-03a6-4a41-9d26-3d6a293633c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7922
        },
        {
            "id": "b339c781-4fc6-44d9-b737-f506105d3a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7924
        },
        {
            "id": "d1fd0ea5-adc6-47eb-bace-16155965e6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7926
        },
        {
            "id": "72e43180-1aa8-435c-9654-ad4c1da0f8fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7928
        },
        {
            "id": "bd5e8552-96d1-4164-854b-e04579d11e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8230
        },
        {
            "id": "5e3fd840-a1c5-4d20-83b0-1e7fd12c1a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "00502d87-4381-434d-ba84-b91ce64bd23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "66cebc70-500e-48cd-8a0e-80c0bdf877e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "8919c169-f158-4af4-aee8-701177414642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "cef0734f-a3b7-48d5-a84c-ee71c1af8e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "139e6383-be0b-4fae-83e2-a3d254e62e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 85
        },
        {
            "id": "3e5cd629-8e19-48e0-a02a-7cac0e447736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "d20f679f-c499-4746-bb71-acfcc0b38e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "6105c7bd-a0f8-4a3c-9b94-031720e70bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "76ac316f-2882-4a3b-b7a6-5a9314590eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "62f879dc-477a-4381-9dc5-e5d54558301c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "24c472e4-45a1-4e20-a256-81a2b8cdbce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 199
        },
        {
            "id": "83e3ccd8-38b5-4cd1-bba5-e98ded4c32ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 210
        },
        {
            "id": "0644f86d-2a4e-4518-aa98-1bd3b438d93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 211
        },
        {
            "id": "f5239162-d668-4115-b077-f3ed0f561fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 212
        },
        {
            "id": "95ffc3df-ca56-47da-a094-ac329e5d1da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 213
        },
        {
            "id": "66fe27ef-bbfb-4a11-b58a-e37778a4bb80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 214
        },
        {
            "id": "cc802b81-c110-4c82-beee-e6366a3e8d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 216
        },
        {
            "id": "d039dc68-e43a-4bfd-b83d-f7f5bb8e5e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 217
        },
        {
            "id": "e5c4800c-bd05-4874-8a6e-0a9223a4b3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 218
        },
        {
            "id": "7e30da28-cace-4f15-9a17-86ab7df3177a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 219
        },
        {
            "id": "af68f2cb-4ece-472a-ab58-a05d22d1a16c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 220
        },
        {
            "id": "5f7a2ba4-2f60-426f-8933-8f86ead161a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 221
        },
        {
            "id": "dcb6fd47-c798-49c6-a90c-75d4a738c884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 253
        },
        {
            "id": "2484c582-4fca-4d80-a540-85c5d0b2acde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 255
        },
        {
            "id": "1799583f-5b14-4a3b-934e-3e8efb9f27c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 262
        },
        {
            "id": "407478d5-9817-4c05-bb2c-dfd3c6a2e7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 264
        },
        {
            "id": "bc7f6e8e-66d3-48ff-abeb-e45cebadd8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 266
        },
        {
            "id": "aedbe336-45b1-4e70-b0cf-03f69092380d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 268
        },
        {
            "id": "c3f3b6c1-040e-4b18-8c58-502c954d9b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 284
        },
        {
            "id": "a9784209-90a8-4778-b581-826148b5e2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 286
        },
        {
            "id": "7a953965-e94e-4fe0-acbf-a5ccca239018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 288
        },
        {
            "id": "49284985-dd47-4130-bcfb-510a17594ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 290
        },
        {
            "id": "1f837fde-37ef-4732-95c1-93546571ba5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 332
        },
        {
            "id": "d5162cee-fc8a-4a2b-956a-eb1584a1d9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 334
        },
        {
            "id": "ae653b28-271b-433d-a3ee-e36e045db148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 336
        },
        {
            "id": "2dc0ac27-369b-4fb2-9ace-fbc1b2952ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 338
        },
        {
            "id": "8d0c404a-89da-4812-902c-d6e54ebe11d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 354
        },
        {
            "id": "89b9a4ae-f17a-4635-9f60-e955d1bf0cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 356
        },
        {
            "id": "7677bd35-2700-44d1-9cae-2acb496f3ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 358
        },
        {
            "id": "e04f395b-90ba-4586-9ee6-b3a8c1ff5aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 360
        },
        {
            "id": "e996ab5c-2ceb-44ed-9a53-a37cb0b3dd99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 362
        },
        {
            "id": "b716ae33-1641-49de-bd15-8a67bdf4a96d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 364
        },
        {
            "id": "c466a84a-cdf0-4b4c-b9f6-67d8b2ee20ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 366
        },
        {
            "id": "157377bf-8c35-4c6c-8aaa-25cf7058a6cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 368
        },
        {
            "id": "95cb8167-d354-42b0-99a4-10c99581a7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 370
        },
        {
            "id": "4342d736-40d8-4a04-a634-d060ffcf954e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 372
        },
        {
            "id": "556c5acf-df3f-485f-ad8f-dbf722bf5522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 373
        },
        {
            "id": "394d3a12-c8f1-444a-b086-0a21c05751a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 374
        },
        {
            "id": "0a71cc6b-38c5-4f15-8903-c8f99a0c9233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 375
        },
        {
            "id": "c06f29e5-edc6-4ec4-a698-cef2a84788de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 376
        },
        {
            "id": "e5ace943-8c72-4723-a72c-546a366bfc04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 416
        },
        {
            "id": "68678de7-70ca-4851-9f6a-4e262140d152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 431
        },
        {
            "id": "3a8583eb-0dd2-463e-9062-0aa4b660cfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 510
        },
        {
            "id": "69caf7b0-b67e-4329-8335-131e9de6ca5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 538
        },
        {
            "id": "327e5a1e-42e6-4d44-aa28-d8346cbc245a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7808
        },
        {
            "id": "d764c70e-2014-4bd3-aa5a-b9407fc7e0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7809
        },
        {
            "id": "f8e79cd1-43d5-431b-9b6b-b8ba98c70cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7810
        },
        {
            "id": "733deffc-1c4d-4a8f-83b2-c45f084367b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7811
        },
        {
            "id": "ece4ac90-db2e-42b9-a148-3bdd62f9fc72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7812
        },
        {
            "id": "508bdc4b-9338-4146-99db-4172ba2383cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7813
        },
        {
            "id": "614c2c55-ff49-4205-a819-77e25ce5220c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7884
        },
        {
            "id": "da681ce3-53be-4ebc-bae5-48de2cb3d129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7886
        },
        {
            "id": "d72f801d-de2b-40e7-9bd2-059665f18348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7888
        },
        {
            "id": "d90a6e97-deae-4de1-b7d4-24ee95b3ac53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7890
        },
        {
            "id": "c766b9c1-2d9f-4663-b058-e4345e24fbb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7892
        },
        {
            "id": "783c5e3a-47dc-4638-a4ec-a19936a5a9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7894
        },
        {
            "id": "dcb93045-f07c-4aff-8da2-e6b51112f070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7896
        },
        {
            "id": "110eeadb-e872-4f01-be20-01743d091f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7898
        },
        {
            "id": "f48b9725-339f-4dee-9468-7d9866a06fb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7900
        },
        {
            "id": "15a53ab2-6a4c-43fb-aab4-69d86286bb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7902
        },
        {
            "id": "e585a409-8f0a-4f7e-a19f-e64e485f3432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7904
        },
        {
            "id": "351a601c-0a0c-406c-bf81-78623054d969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7906
        },
        {
            "id": "49354504-e962-4c60-bc37-8781cf9817a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7908
        },
        {
            "id": "95aa291d-c729-494c-b947-0a64367ff261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7910
        },
        {
            "id": "31f15dde-f3eb-4f34-a0af-d3ddac60aca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7912
        },
        {
            "id": "318fd774-23bc-4e6f-850f-b7688386d139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7914
        },
        {
            "id": "7d8c2175-293e-482f-a02e-bdaee857e8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7916
        },
        {
            "id": "52b5cb5d-f515-4b57-b78d-30abaad1bae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7918
        },
        {
            "id": "2753158c-be1e-47dc-83b7-76b5350f2c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7920
        },
        {
            "id": "77410c47-5e6e-458d-889f-cc826e331556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7922
        },
        {
            "id": "ada1a8c0-659b-4b9d-b18c-22058b007427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7923
        },
        {
            "id": "80a3b8b2-d1a9-4ae7-84b5-7f45a962218b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7924
        },
        {
            "id": "2f55b642-070c-4a87-9029-a1cb581ad3fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7925
        },
        {
            "id": "731e95e8-fdf9-4480-ad2d-5a1286f75359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7926
        },
        {
            "id": "8ed53a3b-4426-40c4-a33e-56fe1a0d44f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7927
        },
        {
            "id": "83f165e0-1549-43c0-b003-906752455da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7928
        },
        {
            "id": "c1929511-af16-40e5-af65-58abe57c1995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7929
        },
        {
            "id": "793b6e91-7502-4699-a9fe-cf6265e95bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8221
        },
        {
            "id": "01ce23b3-80af-4050-b28c-73628dc07107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "d640db30-43be-430c-aa4e-3a1e7f45f806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "087addb3-bb9a-4550-bbef-afc141d47477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "472cd237-be7c-4340-abd0-2d5d62967d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 253
        },
        {
            "id": "6e691b0a-a431-4361-9f45-699eefe1f0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 255
        },
        {
            "id": "7de8cf72-cb47-4d33-a6a4-894964027107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 373
        },
        {
            "id": "5b20cca0-bf5d-4006-aa30-c1f607e134be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 375
        },
        {
            "id": "c7da5eaa-2b3b-43d6-83e0-b91ff44df707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7809
        },
        {
            "id": "097df53d-4129-4b2d-84a5-915a06cb9bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7811
        },
        {
            "id": "cc556123-e25d-465b-904c-c43c4fa0c0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7813
        },
        {
            "id": "f07f1670-f34a-4342-b4a3-5bc38375fb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7923
        },
        {
            "id": "1d2915b1-9900-4706-ac3d-50439828eab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7925
        },
        {
            "id": "d4cd1109-747c-48ec-be9f-e0fef84ec250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7927
        },
        {
            "id": "353a55aa-24df-4be8-af5b-1b2727258233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7929
        },
        {
            "id": "e623ce75-5b71-4be5-a323-eff0beacc6e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "5439f925-cf03-45d5-95cc-a4d6f3654050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "959e838f-9d6c-4a99-bc35-f5ba3adbf7bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "5a5699a7-d038-4afc-bed9-e9c2d2b57716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "26c10e14-15d7-4f83-a0c3-a06127348520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "6f2882cc-7705-49b7-9d50-8c56a73a868e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "6f9c5214-2789-4ac3-b13b-32e9dc375da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "07df087e-d429-4ad4-9571-e339770c379b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 89
        },
        {
            "id": "a933618d-23c3-4dbf-964a-d7d07e87ced7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "38ffa8f4-0c90-4fea-81e0-2a988af10649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "32666f8c-c707-459c-8cff-0fc97df5c936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "6911b977-fe8b-46d0-b821-e82a4c5e3ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "3156f629-4098-4a5f-8b88-b903b383992f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "c7c191be-c3e1-45c8-b5a8-f66cd72e335b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "67b22181-a2a0-4d67-8b2b-2482857da931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "d4673ee8-7e76-4bb2-8450-e7d1207dbf03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "46b7eb5e-a79e-4de5-a3ff-543538f7b76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "9921b9af-4cf8-4b5e-a69c-a2bf04d764ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "0fa96809-1888-4941-b32e-1d1a74e3bf17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "1e0bdf2e-0fc1-4e8c-b80d-06e1a345a40c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "8c095685-e234-4fa4-bff3-cbecf10afa6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "a77c28dc-e8ed-43e0-83b8-7f956496c153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "59031785-7479-4ac3-a377-f7b39a6a11f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "e8ae774c-4d21-448e-8812-9fbf2b6f6892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "ee7e8107-593a-4bdb-87c5-93e99d450072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "69beccba-bc2b-43c6-8e0b-dc478a33d261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "8cfbebab-9f14-4801-83d7-73b85d59b4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "88b91c17-d839-4bbb-a5f7-9c37edddb9a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "11a7d70e-a265-499a-91b2-0ae0c6359806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 221
        },
        {
            "id": "591c3327-682e-44ef-baa9-2ce370dde047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "1e10f0f8-49c0-45d9-a9dd-78e803cdb7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 225
        },
        {
            "id": "184b4e07-322d-4734-b60b-c8ad7a9aa3e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "0ca8af20-d6c5-444e-a539-30f2257fa5a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "65702527-5397-4c95-93be-631b431d4dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "ab85e79c-403d-46ce-8add-0128438e3178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "bdb208c2-8728-42ed-a765-59e31c67655e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "5bb4d913-dbc7-4b8b-9f55-5a0b71baf272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 231
        },
        {
            "id": "273e9163-4f18-480f-bd95-8c7f7043ef8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "17a63d36-fe8e-44f7-a0d7-29cefaf33bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 233
        },
        {
            "id": "7b9d186f-3d88-465d-9fe5-94770ffbf3a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "99ed4165-72cb-464d-8662-46f5d81b8994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "4c874cb9-a091-4740-87f2-aacc00770fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 236
        },
        {
            "id": "31b9bda1-fe51-4762-8d5b-bb3317c7a626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "d94b0fac-48af-4ef3-8654-9dd873a84871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 239
        },
        {
            "id": "7f273fa1-564b-410a-8fc4-a4f8a5c210e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "69cca078-4447-41b4-8360-eaed713656dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 243
        },
        {
            "id": "aa92403e-89ed-41b3-9c68-55d6edd4f3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 244
        },
        {
            "id": "74fdac9b-5514-4ef0-8958-060a05477541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "9b433eb5-5787-4f8d-88fe-e4bc186a2e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "caeafe11-c12b-486a-bcae-deb98389a702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 248
        },
        {
            "id": "d6f6b3f3-01f0-4af5-8778-ad601b74ba49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "dc5f1c40-73c4-4e88-9745-15236eb90f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "f5edd5b3-9ad3-4e14-a036-d58279a57bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "76b82c07-51fc-45d9-ae49-a38f838f360d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "7191b4a0-a5ce-4494-997e-be6e4d70017d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 253
        },
        {
            "id": "c297537f-6af6-4e02-ac1e-240e98a54e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "f29a9637-0a60-4e5f-ac64-93340b7003e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "69ad215e-3edf-4c9c-bbb7-e819433801e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "a421e316-d892-4cd3-958f-4c1d5029b136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "b67998ab-5b51-4d2e-82cb-3d3309b66710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "e4687066-8087-42ae-b8ab-8802e410d287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "40333275-66f6-4452-9bf4-11036af37772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 261
        },
        {
            "id": "cdbf0892-d75e-4775-9edd-f2ebdb0ff4e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 263
        },
        {
            "id": "dc55b36a-3ef1-4023-a408-61ac43ada01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 265
        },
        {
            "id": "1755f901-6008-4e45-a614-3c9649ccd9e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 267
        },
        {
            "id": "141e6b2b-3eae-4cc9-ba7c-82848f95dd68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "73418739-5c40-4956-b809-dd147bb9a3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "2cffcbe3-7233-4e01-8c9c-f7f8f2304525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "aa2c8058-986e-43ca-ac7d-8fdbaa90d5c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 279
        },
        {
            "id": "d824ae91-68ac-4ed1-87a8-96252a43d74a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 281
        },
        {
            "id": "44af5445-0a00-4950-83c7-e6db9ca61429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "4bc309da-8b98-42c4-a9ef-19ef8e47f134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "d8b4efda-31b5-4ec7-aa32-700c7546b738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "e131c69c-28a3-4ad9-9ded-cb39126734f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "f3108d1c-ef13-406b-b359-5e5d0adb42ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "b21838a0-974a-48ae-8815-3977d8fa7c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 329
        },
        {
            "id": "c1d1e994-a28b-4e05-a642-f7aa2e88bdb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "97d98afd-95fd-48fd-9a60-4c6d6fa5869c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "4102ba6f-5bda-401c-9ce0-ff24dad12534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 337
        },
        {
            "id": "c0d1302b-4a57-4079-8578-29f7835431ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 339
        },
        {
            "id": "baceba83-39f0-4575-89c6-74f9407438c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "0b00c9f3-871c-4dbf-bb7f-9d7df96115ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "b34dc7a0-7ede-4a8d-9747-46453739f326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "5e746ade-96b9-4dc6-8ef9-d6928a66f930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 349
        },
        {
            "id": "80614168-7c6a-460d-860e-52a24f14d311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "c5d7669d-1fbb-44f9-b8cb-dc563b50a176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 353
        },
        {
            "id": "97cde369-2ebf-44f4-99eb-82341e227f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "2e7f8e6c-f23f-4aad-a4bd-33bbb98b2727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "f643e0ac-8bca-4005-a887-18863eb09246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 365
        },
        {
            "id": "3f7aa232-3603-4728-be7f-6291216a7988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 367
        },
        {
            "id": "cb5bc4a0-75cd-46ea-aec7-27ea4a8d53e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 369
        },
        {
            "id": "aecdd532-02b5-49f1-8e6e-3e892f7bba40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 371
        },
        {
            "id": "e7e150b9-8aef-4d53-a225-5cad46b7aa4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 373
        },
        {
            "id": "595acceb-728b-4a5b-bc6d-365d59eb70f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 374
        },
        {
            "id": "97579bfb-a792-4a6b-ad09-91857886c0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "00fc2a4b-ab81-430e-b24c-1945871bb4dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 376
        },
        {
            "id": "eeb959f5-34b1-4fc9-89f0-fbb7e7195bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "b3c28bec-89cc-45cf-9856-cdb961853c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "ac7a4f89-595f-4d7c-96eb-017e57feb609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "f0fb4bd6-70ff-44ce-8ed1-bb9f739aeeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 417
        },
        {
            "id": "44811568-1e1e-424e-9795-fb882803a69f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 432
        },
        {
            "id": "77452a1f-754c-4efe-ba63-831459053176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "4bf2a34d-c32e-4e21-8209-da32cf7d8f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "917ac3b1-c292-4c9a-a718-d64d3a1a47d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 508
        },
        {
            "id": "c82286c6-0343-4a89-bcef-d21d7de28307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 509
        },
        {
            "id": "c9193070-cabc-40f5-aa26-e4e43ddebe16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 511
        },
        {
            "id": "5af950a3-6fe0-4199-871d-1beedb62af49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "659d0027-03a5-4e55-b656-1a381f2a745a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7809
        },
        {
            "id": "1b71c3a5-386d-468c-88f1-a4d92b106b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7811
        },
        {
            "id": "80ff1c1b-7954-4da7-9c51-611f59de0851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7813
        },
        {
            "id": "8d44bd1e-82d1-4491-a5c8-0cfb4beb70d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "c23945c9-897f-44c7-809a-5d2878bd17bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7841
        },
        {
            "id": "5fd9ea97-54f1-482d-860a-c2dbbc088138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "eebbe173-368b-4c26-a993-938a8cbe8634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7843
        },
        {
            "id": "c6f145b5-8981-4d05-82dd-ce99be83250e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "21ce581e-c564-4425-b50d-78dde540ad3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "9809e859-dd4e-4e10-a7e4-8f8326a6560e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "7ee23932-1209-4519-b999-85d6bc98929b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "000d6a15-8e11-43b2-8882-7a2c2b5568c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "4fcb8519-c215-4aa1-b867-acf11d6d4fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "7b1b9cfb-f4cf-4df0-8abc-922d7d0e0917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "454fecc1-461f-40ed-9be9-1e54875a0da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "303dad55-1011-47c3-b27f-3c88d8ad349b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "c76953e1-1e4b-4b1a-9215-b5de051b4588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "bde41acf-5e4d-4b25-8910-37963e881d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "c384f5ec-a05f-4011-b772-31a4310627c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "5a748368-d775-49f7-b5e1-89812664d277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "42b8618f-496c-43b5-bc2b-534242423b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "4d1ccd49-9f23-4fcf-b4bc-ffa8d5007de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "bf709011-ee8d-47a7-bf28-277bb47c92ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "005ff9da-b190-4c40-9b61-15903a1551de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "6d206b9c-c43b-4307-b84c-8f6cc258b289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "0f6dd482-03f8-4979-a49e-5706575e1a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "2bf0861e-7f69-4886-8b85-6b0a6e395fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7865
        },
        {
            "id": "f1b56ffb-7339-4273-8468-181be83eb49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7867
        },
        {
            "id": "2996d6d1-a05d-46e2-81d9-eb841bfa177f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7869
        },
        {
            "id": "0a284207-78cd-4bb7-98a2-83a432cf36af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7871
        },
        {
            "id": "3a5ea246-3001-44e0-88c4-8f3abd532d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7875
        },
        {
            "id": "67acd74f-dbc5-480b-8bc8-1ec6f8f8b779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7877
        },
        {
            "id": "a0f99f81-9fc9-4622-a040-b500517ccf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7879
        },
        {
            "id": "c741032f-03e8-44c2-92f0-3ea31fb528de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7885
        },
        {
            "id": "5a6ed8f7-9fbf-4c04-b9e4-f36a9ce5e439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7887
        },
        {
            "id": "95cf2dc9-ca4d-4079-9782-feda13568c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7889
        },
        {
            "id": "2781383b-c51d-4e61-917d-fe67d935a87f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7893
        },
        {
            "id": "f71ce815-29f8-4e98-85ee-0776fad3c950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7895
        },
        {
            "id": "7ef963c0-7ad8-4119-b78f-5eaa9e9f452e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7897
        },
        {
            "id": "52d235f4-fdb2-4eff-a152-da39feabfea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7899
        },
        {
            "id": "23709d86-3414-408d-9d71-a8372a888370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7901
        },
        {
            "id": "779564d8-4c87-4525-ae90-752b4fb0a4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7903
        },
        {
            "id": "7cbc8326-168a-4455-a09c-55a69ca585ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7905
        },
        {
            "id": "77d7b495-f69d-4551-a241-21b3ec6d96d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7907
        },
        {
            "id": "9fb4c6e8-a3fc-439d-baa3-1b2dab9bc757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7909
        },
        {
            "id": "7c4a838d-4bad-4b2c-9d6c-f16bf4042084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7911
        },
        {
            "id": "a5057a3b-abc4-44cd-a729-8583214ec390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7913
        },
        {
            "id": "784c9c78-50c5-4e7b-b8a1-b9820fcfc045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7915
        },
        {
            "id": "92d00488-0d38-41b6-8486-ba366f9b9f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7917
        },
        {
            "id": "0f4ef738-abf6-4118-892a-c3b0f47b4c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "79b258ce-4d84-46b9-b903-ca68849b4f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7921
        },
        {
            "id": "3db0c5a0-62e9-43c9-9188-1f250ff5cbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 7922
        },
        {
            "id": "3528f549-ebf8-44fd-8316-d9dd43573346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "75607ad1-ae39-42aa-8de3-add0f7007f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 7924
        },
        {
            "id": "7a8e1f47-4523-440b-a5c6-fbafda527950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7925
        },
        {
            "id": "536a4e6c-d5bb-4bcc-9504-f2c584de7790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 7926
        },
        {
            "id": "40fa4b7d-357f-4477-9805-7716aa491059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7927
        },
        {
            "id": "9e7bf4b8-3744-405b-b8c6-8044082dc5ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 7928
        },
        {
            "id": "ebc278b4-45c4-42d1-9ce7-708b77d43d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7929
        },
        {
            "id": "34fb0875-c547-4e7b-84b9-1e3c939f7892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "dfd4c6a1-5a53-4618-8e38-5ab24ab0c5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "eb02ca1a-e2cd-4254-b822-d714c02c5106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "ce08d9f4-3ccb-47b6-a911-c85360ea0096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8213
        },
        {
            "id": "9e5095ca-5051-474d-bce3-89b7b52d271f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8216
        },
        {
            "id": "d916df49-5cb3-4d99-b113-4009a608e25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "562448ca-2561-4296-9a02-4f7aad8acd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8220
        },
        {
            "id": "7bb5f1f9-ffee-42c0-8b06-98138d59cf4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8221
        },
        {
            "id": "f39f0922-3bed-4310-9935-ac2f4d0477f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "204dbfae-33e3-4236-b4bf-d8bafaaf1dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "55f8ddae-5def-4ef9-8571-a81a4a3bac76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "c5135575-fa95-4147-b8c2-37a4ed3de87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "1dc24334-4228-4a4c-ad75-088ca514fbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "50363d80-0198-4013-8cc3-dcfe9e15f400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "36bb243d-93ac-4e45-ab72-97040ca2ddc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "7d152dac-4a5a-4af4-a1c0-901457eb0f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 192
        },
        {
            "id": "567115df-d274-4d22-ad12-0a837b6eb673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 193
        },
        {
            "id": "ea7fd3d8-0024-41db-918c-a049b440f21b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 194
        },
        {
            "id": "f8e7d2f2-818f-4564-a1ad-61e432f0be6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 195
        },
        {
            "id": "ca1d7c2c-708d-4436-bc82-3ff1bbf71ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 196
        },
        {
            "id": "560b4ffc-2a9b-4710-a91c-a6a26ef32419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 197
        },
        {
            "id": "99831476-a51e-4cd4-908b-ad5c224a27b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 198
        },
        {
            "id": "1111f198-ca2e-41d0-b972-32d3ac18463f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 239
        },
        {
            "id": "f13acc04-c050-41c7-bcd5-a711ec9923b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 256
        },
        {
            "id": "963fbaac-db8d-4064-bf65-e657a15ac601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 258
        },
        {
            "id": "c4f7ceb8-6821-427f-94ec-71ff21ef2a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 260
        },
        {
            "id": "401aa455-0628-46d9-9aaf-59db6c09102e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 378
        },
        {
            "id": "d616389b-5964-4a5c-a199-39a719c32fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 380
        },
        {
            "id": "e2dcb077-b19e-484c-9fb8-d376214f5bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 382
        },
        {
            "id": "dfa245a1-86bc-48f0-ba49-7ee47be9ea51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 506
        },
        {
            "id": "a8d7ff3a-10c8-4b37-bfcb-0af039fdaac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 508
        },
        {
            "id": "afb08114-8b6c-4613-810e-038d8b0980fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7840
        },
        {
            "id": "cfa8697a-be0d-4e37-a810-633b3afb3e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7842
        },
        {
            "id": "9c012404-66b9-4582-8263-53dbd09c81ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7844
        },
        {
            "id": "7c3e4bb7-fe28-48ca-8a49-b0c480026e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7846
        },
        {
            "id": "397d0c5e-70a7-47e7-9580-0f21d17c5cad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7848
        },
        {
            "id": "0034723a-2ba3-4069-ab75-a6370275f000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7850
        },
        {
            "id": "65d010ab-7b17-4e58-8bc7-b100e163d272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7852
        },
        {
            "id": "d77c3741-1394-4f90-b161-cda5c76bcb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7854
        },
        {
            "id": "f402d68a-ee41-4fa0-8b57-5e72c35120bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7856
        },
        {
            "id": "fcb7dd0a-7f98-4b01-a0f3-c1f2e0bb1bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7858
        },
        {
            "id": "805ba027-467c-4e29-865e-797e6e260ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7860
        },
        {
            "id": "73b8f0f3-2048-4156-a8ed-f6a19ad7ee29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7862
        },
        {
            "id": "0e0e8dd2-1894-4363-97db-a4eafa73a730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8230
        },
        {
            "id": "fe2dd542-acef-4fe6-8164-f9742974baa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "ede43893-6ea5-4a7c-90ae-ab437903083e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "63a729cf-0537-4bb8-9806-a3e94bed370f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "a7d48255-1477-4122-9383-28678117ada3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "e0ca65d8-04af-426f-b515-d854ad9c946e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "92a97b5f-57e8-48a6-9018-cd78d91b9e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "b17ccdb9-b6e3-4f9e-bbf1-bab29e74703b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "fa0943ed-a643-47c0-88f7-a18a4e1a8b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "3a9cdfbc-4aea-492f-8fc1-7b2fe216b0a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "1afaa17a-5761-4466-9453-1ab940826d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "4de3a05f-adff-41f4-a7b9-45069dd06986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "fc547313-82fe-4470-bc29-fb1c0deb5944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "50fa486f-f640-4a9d-8690-c140645d64f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 100
        },
        {
            "id": "8eb3ea5e-2f25-4a3f-9795-56aebf0fd1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "dd8a12ff-1e28-4b3c-a403-601a855c6f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "a41e6f3f-3222-48e6-8ec0-41452404b4c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "9366ae04-6046-469f-8338-13ce312dac06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "dfe3d8d7-cfa6-46ae-8202-61da7b6de4bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 116
        },
        {
            "id": "2941d1f6-dbc6-4e32-9163-85ccaf13a929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "7a6068f6-9040-42dc-8b39-c12c7369a34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 122
        },
        {
            "id": "d2a38c83-9eaf-4f28-ae87-0823ddd5e262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "7aa3666a-738c-40be-af66-ed88f928dec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "b842dadf-0cd6-4b66-98fb-bd93a3396e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 187
        },
        {
            "id": "5e05ad4e-401c-435e-9ee5-1367dd9764ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "67ba3b42-d8dc-499f-a326-bc7392ac118c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "670aa5bb-7e6d-498c-bd92-8da41a242204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "9bc041eb-c23a-4e43-ab77-c3181357886d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "359ef3fc-b5fa-424a-899b-0d885c65528b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "1c9d7b28-96fe-499c-a2f0-9737ba380fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "4481e27c-241f-4488-a078-a411f794f866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 198
        },
        {
            "id": "473008f6-08ce-4cfc-b2e8-b6fc943a2a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 199
        },
        {
            "id": "d950dd99-ad1c-4ccf-bd3d-aba288393f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 210
        },
        {
            "id": "24f3d237-54fb-4b01-9d8e-5570b62162c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 211
        },
        {
            "id": "fd18fca6-7f50-4213-af24-95b000648fa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 212
        },
        {
            "id": "e6a92a4c-3efa-46b1-a321-3bc918416c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 213
        },
        {
            "id": "5d802481-d0c4-47b2-bdc2-c8bae9c12b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 214
        },
        {
            "id": "4c4fdb43-6f87-445e-be05-5ba138041b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 216
        },
        {
            "id": "b31f2bce-14ca-4ce3-982c-026da80b1238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "72f4449f-778d-4a23-927f-fcb14aee7ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "5539c335-255c-42e4-956d-248fb4e79b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "93c3a363-5190-43c7-a112-345d65a23c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "4da3783c-b343-40ba-894d-8a93721c7686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "f76b3931-0e40-47cb-8286-a923518be37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "66ff45d2-3014-49b4-a72c-a3b4df7f239e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "6bc76d90-7ea2-4bd9-a125-91d2fa6d46d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "78d4c8db-bb3e-4dd2-a98e-49f3577bbcc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "f66cf4df-a56a-4187-beb9-95ebac662e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "d81d72f2-31a5-4002-b1f2-f3a0c605fe71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 236
        },
        {
            "id": "7ca79bf2-630a-497b-be0e-a2134fc5339b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 238
        },
        {
            "id": "76744394-ed7c-4791-abd3-251c05529ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 239
        },
        {
            "id": "c2ee2bdc-31b0-4285-ba19-7a529fb7f28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "a118c88d-3cce-463a-8ad2-f4346ca7934c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "49a26eee-30b1-49d2-ab0c-fdb9da9b1766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 244
        },
        {
            "id": "25cff670-739d-4814-bba8-6c6662a0c535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 245
        },
        {
            "id": "c70f97c7-2623-4d67-af5d-596f97e56720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "899d0e4f-0f13-4ee3-80a1-30dfb6cd4391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 248
        },
        {
            "id": "474718c2-cc8d-4313-be99-d479f46718c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "5e7c798a-fabf-4fbd-ad2e-e22aad92f9de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "5567a9b9-00db-4f38-a7d0-ce84610de971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "2c5c5215-cdd2-4f37-a7a7-b94db578e2aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "539881a3-8874-4ef9-9080-5c37036a5b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "1acc6372-85ff-444e-af1e-0183c871c83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "8368b6ad-f475-4d3e-8f3d-b8d7aaaec4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "ffef461c-de49-4312-910a-c0707a453eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "2c45cea9-fcf4-4449-b61d-41e7bf192f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 261
        },
        {
            "id": "269ad9ae-e6b6-4d38-ba32-c6b8a1ef5627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 262
        },
        {
            "id": "caba4576-ab37-47ad-8a01-550552355a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 264
        },
        {
            "id": "dc24547c-5031-4b3d-96b7-d31afd902d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 266
        },
        {
            "id": "84caa5a1-1872-4f47-b3ba-4de1dae33cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 268
        },
        {
            "id": "3f67094c-0c0b-4656-ab3a-532b36ec12d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 271
        },
        {
            "id": "f87124ee-0e37-4346-a080-9ef2f41d7c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 273
        },
        {
            "id": "cbcd5618-0fc6-432e-954e-5322d815ef43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "c9e5c078-0037-4b83-bf6f-8265b689919a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "94502bd8-5949-462e-98d6-3a734ef96359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 279
        },
        {
            "id": "0d00c6c6-ca95-4d86-89c3-e703418deb2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 281
        },
        {
            "id": "aec92c40-ac69-4f24-8fa6-277912ca4546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "12665587-cc6e-452a-984f-06a23440bd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 284
        },
        {
            "id": "1d95a50c-164f-405b-839f-7a3ed96bffcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 286
        },
        {
            "id": "301511da-e298-4033-b6fc-555378762108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 288
        },
        {
            "id": "d10b88df-01bb-44be-838e-0f707720ccff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 290
        },
        {
            "id": "cb89b6ba-6cb3-42a9-8ba7-50354067ff77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 297
        },
        {
            "id": "66c676ec-87f8-4edd-83b0-412cc932ca69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "400e548b-96ef-4af6-a86a-8931c6313492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 301
        },
        {
            "id": "63530c81-ca7a-4d20-8db9-ec8bdc777500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 305
        },
        {
            "id": "37d65366-0c0e-45c2-8717-b1a731abf5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 329
        },
        {
            "id": "d74e24ab-2bdf-4a21-b7fe-9887522dc1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 332
        },
        {
            "id": "5315f5ce-3b19-490b-b5d4-dbede4b6534e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 333
        },
        {
            "id": "a86a14cc-078a-4103-b94f-7bf2f1392c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 334
        },
        {
            "id": "d54ad2a5-f4e8-4e5a-bdd7-9ef54b9c15ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "f82fce1c-23c2-4791-aaea-6c01e93bd864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 336
        },
        {
            "id": "4d5cb749-2fd1-4e86-8493-72c49155eecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 337
        },
        {
            "id": "f21f88b7-b5e2-4bec-a62b-1d19dcfd6c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 338
        },
        {
            "id": "01ef4e63-1d33-4dfb-be4c-5bd176606399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 339
        },
        {
            "id": "2581798e-20e7-40f6-9096-8702ab8af1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "bb345727-1c1e-4be2-b6d9-2a54910a6af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "17f035ce-3a67-4364-a999-073f71cad153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "c87a6a7d-4893-497b-bfec-b0fd992343ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "4642b5b8-cbfc-477a-8028-fe35b7f6dc4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "6214a083-22af-401e-8e35-412fb1f8a6dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 354
        },
        {
            "id": "37a18c74-494c-4dcc-9252-9476aa3357a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 355
        },
        {
            "id": "b2d3e368-0b37-4701-81cd-f697cd13c820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 356
        },
        {
            "id": "9ed62232-8c0a-4dd9-941d-e257fec43e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 357
        },
        {
            "id": "be6b50e3-53ea-4ce9-84ce-049ee821f5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 358
        },
        {
            "id": "8106c27d-10ec-4b88-af89-e4de535e5b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 359
        },
        {
            "id": "cfc3a9d8-81fe-4555-89da-77922b27f845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 361
        },
        {
            "id": "c99d6143-0a8b-4b63-b9e7-9208d5489ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 363
        },
        {
            "id": "40697d26-83ed-4bcd-93a4-9c85337bba9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 365
        },
        {
            "id": "486a2bcd-78e0-43f9-96c5-96e001010c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 367
        },
        {
            "id": "7bb831b4-0aa0-425a-92a6-a663d247f755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 369
        },
        {
            "id": "6fd0b177-40f6-4211-a60c-56ca48089b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 371
        },
        {
            "id": "22a89e90-d7dc-4930-bf34-5f2a4c9dfd40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 378
        },
        {
            "id": "900b6347-3c06-411e-8075-c9fddd4b9248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 380
        },
        {
            "id": "054b4c74-b182-494d-af6f-f2660aecac19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 382
        },
        {
            "id": "1f47aded-4e42-425c-ae4d-f484f8172115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 416
        },
        {
            "id": "3e0ec513-b2e3-4865-b78c-3223c80f81f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 417
        },
        {
            "id": "063e3efd-5696-4349-99d4-a0a2ade09d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 432
        },
        {
            "id": "3f1e10fe-8cd0-4b78-a461-0ea98d24dccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 506
        },
        {
            "id": "dea5b2a0-a959-49eb-be88-5b272d2796bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "82ec27a4-127f-4763-8dec-a5527f214170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 508
        },
        {
            "id": "e876981f-4ff9-44d0-80b8-a5e671720bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 509
        },
        {
            "id": "3f06fced-1aaa-4ad0-91ee-411fc83afb13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 510
        },
        {
            "id": "827f6fbe-a0bf-403c-89d0-da5c9e6af079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 511
        },
        {
            "id": "04e01190-1622-442f-a5a4-d4a3859d9547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "a08c3295-9db7-4ae1-903d-25c5488c876d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 538
        },
        {
            "id": "7aa3ca10-7dc2-4597-a5e6-194557bfd96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 539
        },
        {
            "id": "c257e61f-e891-4d2d-b417-cea497276ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7840
        },
        {
            "id": "6c03d29a-4bf5-4c95-b2b9-6c85619288b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7841
        },
        {
            "id": "a16e5d92-fd59-489f-9e80-e377101cf098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7842
        },
        {
            "id": "734fd941-6df3-40fb-b80e-38d6bbf0475d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7843
        },
        {
            "id": "fd34bae2-2171-4d4a-83e1-e041daa3e883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7844
        },
        {
            "id": "959219eb-aae8-4110-b378-e1fa31eb5593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7845
        },
        {
            "id": "9d51acee-2f91-4174-b6fa-96dd0f271bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7846
        },
        {
            "id": "7d64ba0a-d665-4c3c-b5ac-785e788c4962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7848
        },
        {
            "id": "7ce8fd53-5383-4377-9f0a-e7c54c823bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7849
        },
        {
            "id": "0593fa60-f3c3-4bc0-9084-0cf983c64033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7850
        },
        {
            "id": "22f549a5-77ed-4ade-8966-b3d3d2d3295e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7851
        },
        {
            "id": "dd1c9d05-ae49-4d8e-8a57-a7f4c1df11e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7852
        },
        {
            "id": "98774b94-c91d-4e00-b920-1e93c4b69561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7853
        },
        {
            "id": "3bc1e0f5-d640-4285-826d-8f1d798da753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7854
        },
        {
            "id": "05e020f3-a3aa-4a82-bb70-0aa93a7a0492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7856
        },
        {
            "id": "ca3a5bd6-691e-4797-9aa9-aaac6b4bf239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7858
        },
        {
            "id": "722b6ec7-5225-40d1-b1e6-5e22f8e353a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7860
        },
        {
            "id": "5755b0b5-3616-4a56-ae59-90027d3fa644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7862
        },
        {
            "id": "3e4674f9-5e77-4d49-9d05-44d00186e4dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7865
        },
        {
            "id": "1702a604-bd09-430a-af18-4bca98d3947d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7867
        },
        {
            "id": "18bd5b0f-9619-40fd-94ea-cef8f9a2ac11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "16388080-0edf-46d7-bc44-8614ebee3faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "588aa537-7077-48d8-90ad-724c764a628e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "5a271215-3545-49fa-a2b9-d3e3eaf33f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "b6cbd68e-ddeb-4cc8-b9bd-55560f5f7288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "e7824ce4-12f1-472b-a788-b392128aed87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7884
        },
        {
            "id": "12893028-00f7-401d-82aa-dc9f1a433e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7885
        },
        {
            "id": "bcd33152-bbec-417f-a99c-1f3be29d1ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7886
        },
        {
            "id": "d400c5f9-a615-4f82-a338-e76cb9f3a1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7887
        },
        {
            "id": "5bd95564-f775-41e1-87ac-76bb25878fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7888
        },
        {
            "id": "66837868-f362-4b9d-a999-88a80a3384e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7889
        },
        {
            "id": "b859f332-4775-4743-aecc-eb5b02a4accd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7890
        },
        {
            "id": "f3853b8b-4847-4dc4-b91b-72e25cb4d7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7892
        },
        {
            "id": "42a3f0cf-52ac-4b28-8102-75a6b3b40d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7893
        },
        {
            "id": "8f9efac9-3a76-46ad-8ec9-b476a6a9aa86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7894
        },
        {
            "id": "3677e5be-1312-4025-8ae2-34140127985a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7895
        },
        {
            "id": "84b8e3de-191d-44e8-8e53-d3ed332c8b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7896
        },
        {
            "id": "53b75ca9-e0a8-4db8-81d5-c4e0c33bb5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7897
        },
        {
            "id": "f2439f9c-afd4-4786-953c-41738cd91188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7898
        },
        {
            "id": "82fb5a24-a06d-4654-a761-b4e1c0886909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7899
        },
        {
            "id": "c18ffec4-4028-40d0-bc2f-9a6ad4cd5520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7900
        },
        {
            "id": "873fb854-4d56-4862-81b6-ad01b04c54b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7901
        },
        {
            "id": "280e5d78-2266-42cf-9eda-5959d128f708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7902
        },
        {
            "id": "6e9d7ea2-529c-408f-929d-2d46a531c10e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7903
        },
        {
            "id": "23cd0d21-21eb-4dd9-a312-b1d5b8160893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7904
        },
        {
            "id": "14e7a0b9-6ce1-4b9c-a312-a196bf343c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7905
        },
        {
            "id": "b21598aa-33e0-4d12-85f2-315e746e9d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7906
        },
        {
            "id": "3f44af06-0e90-4f72-8979-06a5a98a8c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7907
        },
        {
            "id": "745db8bd-b67c-4de5-998a-045a3b261bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7909
        },
        {
            "id": "fee287e7-06d3-44c3-85c8-49b4782a7c1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7911
        },
        {
            "id": "5ee0a383-91cf-458e-a3e2-02d16ebde639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7913
        },
        {
            "id": "8f074680-5570-4339-b4e9-1b5fbf1f478e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7915
        },
        {
            "id": "7ea45a04-d3de-4c00-bab6-b989562bb55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7917
        },
        {
            "id": "656d77d8-6b2b-48fd-868f-c1c112340c80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7919
        },
        {
            "id": "2944d257-8b4a-4b39-9fb0-13e0228f7b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7921
        },
        {
            "id": "ddb77b16-13cb-4796-95eb-f891f0cbdea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "0f5c6762-fdc9-4a57-ab7c-6e92a7f5dbd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8211
        },
        {
            "id": "c6e69a60-c0e4-4284-866f-70758610bd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8212
        },
        {
            "id": "f5ffaa35-5e02-4a6c-bd16-4554e9a26acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8213
        },
        {
            "id": "a4583a73-1274-4b27-9447-d9359167d266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 8216
        },
        {
            "id": "36afe42f-0981-43a3-b2f4-583304e4508f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 8217
        },
        {
            "id": "725ae3ac-0461-475a-914b-d89b63c41ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 8220
        },
        {
            "id": "caab9816-17a3-420c-9205-556155d4456c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 8221
        },
        {
            "id": "db40d25f-399e-448c-881c-f7b444b98535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "e60745fb-7879-4970-a8fa-8a6377728594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "d7c5b750-49a1-4666-8788-1b7ea210cab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8250
        },
        {
            "id": "c7675851-eeb5-4ccb-867d-66a3eda6bdbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "9bde7e79-7eea-42d3-9179-d8ae44ad7c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "c3f21454-9d62-490c-bb7a-4220a89452cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "c496ade8-a761-40f7-8daf-f40dff0a18cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "90d627c8-89b0-479a-9c4e-35f9a76a228b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "003b9a05-c97d-4864-b22f-f91248c54ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "aa7e174a-1c1e-4d5f-a839-15b22e464493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 67
        },
        {
            "id": "b0f2279d-a6c6-4823-bc3c-9e318a058b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 71
        },
        {
            "id": "90ba7254-1d3b-4aa9-8a4f-04dd4889fa23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 79
        },
        {
            "id": "79091768-8273-48a8-8949-a0215131f12c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 81
        },
        {
            "id": "4de06c03-0a56-4d3b-82a7-3ddec9584aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 84
        },
        {
            "id": "90d513e6-d528-464a-8c15-83147cf16d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "2d6d39cd-31a4-4327-872c-65561d4e4aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 101
        },
        {
            "id": "0a2ff467-1b34-4a50-859e-1dc342b937c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 109
        },
        {
            "id": "3924f122-2ab9-48bb-9d36-5e609b479b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 111
        },
        {
            "id": "ed7bdc5c-e3b3-4b6e-8ed7-1886b94fd904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "742462b3-39c2-4d63-b94a-3d212ffdcef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 116
        },
        {
            "id": "105ca3ef-3aa4-4d47-90f4-d6327b8d7fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "a36fb697-6a39-403a-9dd9-da037e092748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 171
        },
        {
            "id": "b3ec02c9-018b-4c43-9214-5ec839c33c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "1cb2b127-b0f8-45b2-b91f-52a6d916ba6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 187
        },
        {
            "id": "e2bc2467-7599-4f3c-9ae8-4a1fab255628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 192
        },
        {
            "id": "0e53fb4d-7b3b-4d6b-8b94-60064fa07ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 193
        },
        {
            "id": "33edae75-7160-4b6c-a41f-2b4ba9272839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 194
        },
        {
            "id": "c083b1b7-0cde-4ce0-bedc-b191dad75489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 195
        },
        {
            "id": "b4359654-e411-44f7-8bbe-1d6b28df6fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 196
        },
        {
            "id": "c241d8ee-1150-4cb6-b99a-b6e6f4a270e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 197
        },
        {
            "id": "52dbbe97-e8be-46cc-a588-c722db685aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 198
        },
        {
            "id": "0c97df10-4a58-472c-8905-c26813044818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 199
        },
        {
            "id": "f3691968-fda9-4a05-9167-c65bb712d545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 210
        },
        {
            "id": "80346632-9234-4f71-9926-ca6d4e9928df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 211
        },
        {
            "id": "e499ed70-811b-4ed1-8561-db587b35a0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 212
        },
        {
            "id": "1aeeceff-eb99-415e-b895-fcdf9ee33500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 213
        },
        {
            "id": "892b1f34-739c-44e8-93b6-62dd686ca46b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 214
        },
        {
            "id": "f9a07315-b0c2-4408-930c-8cb0d7ab7f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 216
        },
        {
            "id": "3c6f0ca5-40a1-4f1b-a92b-de0f9a8fca63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "0cc2a100-6940-42ea-b60a-27cba320ff42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "59e0e2ee-7177-4af2-9ced-2b9552b5404c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "30638898-4d74-40bf-80f5-381ec034332c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "c80e2ae7-308e-4e73-8488-83a6a26d7674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "6859a293-2002-4034-b5a7-3a394de98390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "56650f85-99ac-4504-a429-83b18880b7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "0dbd0b6b-2254-403b-851a-98bbbc919d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "19b83d47-916a-4bdc-bc51-d886cacec56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 233
        },
        {
            "id": "143a0250-80fa-4e94-ad6d-4eb529ac676f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "408be3f6-7969-4e46-af01-6833510279a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "23583cf3-0507-46da-8d5b-a9afc733b51d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 236
        },
        {
            "id": "83f91fbc-04f0-4309-9d5d-c21be5532d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 239
        },
        {
            "id": "9d99a797-6623-49ca-9d0d-c6b005200d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 242
        },
        {
            "id": "68edc310-957e-4084-89bd-2fb8be936edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 243
        },
        {
            "id": "92652c43-24be-4688-aa6f-f42cd2f0f974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 244
        },
        {
            "id": "14930180-ac86-4037-b917-909aa8ed916d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 245
        },
        {
            "id": "eea4ef87-50ba-49fb-86ae-a60c5101d98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "76d92dbc-065a-4ece-9d86-82b787c3ea05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 248
        },
        {
            "id": "691dc58d-7420-4c2c-848d-7ac0bc5764a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 249
        },
        {
            "id": "e28dca09-f915-4fab-9809-6c46b9182af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 250
        },
        {
            "id": "b881eb18-47f8-4aff-822f-774331815e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 251
        },
        {
            "id": "c26c1de1-3485-4765-9b84-571a51d94c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 252
        },
        {
            "id": "1c015873-29aa-44ad-9d77-3e2acae89506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 256
        },
        {
            "id": "c666c2f1-24bb-4ea3-903d-74af1e19cd09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "14902fd8-39d2-49df-928f-66e5f0db78eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 258
        },
        {
            "id": "ace75b64-c708-43da-b8e3-bde163d19d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "85002e7f-02b3-4f15-9dd1-8f772e93bdfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 260
        },
        {
            "id": "e72873e2-1424-43c8-bdd1-db9f9fc7acc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "d70572da-2697-40d3-be27-262b36738d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 262
        },
        {
            "id": "60ace129-9aed-44d5-8d1c-009952dbd9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 264
        },
        {
            "id": "84bee2dd-29f1-41b4-9d32-591c9ece9c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 266
        },
        {
            "id": "b3983019-2464-4767-a3ff-21809e24895f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 268
        },
        {
            "id": "2cdfc91b-832f-4d95-b2c3-05ddc96ea5a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "a4f84f87-e9cb-4bf1-a0fd-a57ae8161b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "4dfd2084-7d86-450f-a665-cd0e828d95d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 279
        },
        {
            "id": "6fb06898-854b-4def-a253-d170a5d485e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 281
        },
        {
            "id": "d55efcb5-67fc-43d9-b1d0-1b778ae77ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "c95b7a20-c4f1-4ca2-bdcd-85f12cb57146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 284
        },
        {
            "id": "604858f9-a77f-4170-8347-ecc6a7f7d977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 286
        },
        {
            "id": "ea308903-5ac9-46f5-88e7-2c86cb00e1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 288
        },
        {
            "id": "3ed9f243-a88d-4b95-9ed7-c5c7caf585c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 290
        },
        {
            "id": "4c790db6-44e2-4f53-8f70-37f6fc89b555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 297
        },
        {
            "id": "1895ed2b-68d5-40c5-b1d0-98de466d56a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "37e4f7d4-ac34-42ac-8acf-0e7ebb4ae117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 301
        },
        {
            "id": "c9e4ab81-55ff-4b72-be02-4a3b6cb4fd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 305
        },
        {
            "id": "7110711e-ed1f-43ee-95ff-6388bc8d6c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 329
        },
        {
            "id": "ae3c7315-5dd3-4844-8034-b6d36e9cf7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 332
        },
        {
            "id": "15e172a4-3274-4900-901c-749f75f8bde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 333
        },
        {
            "id": "d83ce974-e91d-449f-868f-aa2d2299c9c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 334
        },
        {
            "id": "70ab1624-fe36-49b0-ad15-ea69199c5eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "f0daff74-84aa-4e0d-b836-84b7c48ff2e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 336
        },
        {
            "id": "8f10f771-163b-4680-a294-236840f7213e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 337
        },
        {
            "id": "d4705268-edf2-49c5-81f5-6736d110eff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 338
        },
        {
            "id": "a1e4c1d6-c293-4237-b64f-c2ccab06ca79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 339
        },
        {
            "id": "3957d05a-ec70-4aaa-923e-d0fbe8386557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 341
        },
        {
            "id": "9426bb65-3861-40b0-94ae-f47e32a14fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 343
        },
        {
            "id": "fdebeb05-afb9-492a-810c-20eac67deb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 354
        },
        {
            "id": "3cd7220a-af1c-48e5-b7f5-4eb1217c4e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 355
        },
        {
            "id": "c1fd90ce-c9f5-4551-9930-6cd27b17aa44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 356
        },
        {
            "id": "ef61ac59-ff0e-49e1-bd66-3fd0ce247359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 357
        },
        {
            "id": "fb9aea62-d9f1-43f2-9e91-5eff53baabd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 358
        },
        {
            "id": "b63a757a-ff7e-4dd3-abca-560ae9cfa7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 359
        },
        {
            "id": "b6691ee2-d705-4bfe-bd80-4be82aa121c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 361
        },
        {
            "id": "72c02281-e667-4f0a-a88a-ecb04540973b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 363
        },
        {
            "id": "cc02358f-5ab9-449d-a738-785d4b197619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 365
        },
        {
            "id": "878b27e7-8ecb-46b7-8108-49f04167186d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 367
        },
        {
            "id": "6f78e273-132f-4b20-8fc4-4f0f0d49f272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 369
        },
        {
            "id": "62898eb4-b3fa-4548-91b6-b9707fc960ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 371
        },
        {
            "id": "7e9340c7-6374-4c88-96ce-71158797d512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 416
        },
        {
            "id": "badc7405-1e95-4f75-b43e-0eabcace68c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 417
        },
        {
            "id": "00be5525-70c8-4b4f-be18-7a1110dbdc81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 432
        },
        {
            "id": "1ae2740f-8d23-483b-bbdf-25468a0e443d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 506
        },
        {
            "id": "dad5dd50-6258-45fc-b08d-559b359b13b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "5fd30fdb-f54b-45cd-9f74-c78bc334a2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 508
        },
        {
            "id": "981d5231-8f3e-43bf-a5a5-b584e1add14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "1aaa7598-6484-43e5-b463-21203a54c25a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 510
        },
        {
            "id": "256c56e4-21bd-461d-a8ec-74c06c298047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 511
        },
        {
            "id": "01b72fde-7159-4b12-8477-b1e5a4a6eeb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 538
        },
        {
            "id": "8abffe9a-435d-4367-bf85-3fca7587e847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 539
        },
        {
            "id": "0a875cb1-b979-4acc-b6a7-e7872fc21a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7840
        },
        {
            "id": "f5fad80e-4276-4974-ab50-59bb7874eb80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7841
        },
        {
            "id": "08953b06-6379-4ebc-9041-53c4abb89929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7842
        },
        {
            "id": "0fa06cd8-671a-4a6b-b800-f9fc7c2b21fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7843
        },
        {
            "id": "a9033164-489a-4c6b-a3e6-694081f30f9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7844
        },
        {
            "id": "0c564644-191b-44e8-bfb9-1c38c7ceb37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7845
        },
        {
            "id": "049016df-7047-4c4f-a37a-2f689c84dced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7846
        },
        {
            "id": "cc9ff84d-5e97-421f-a859-ddb4b43ba592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7848
        },
        {
            "id": "28381e2e-57ea-4f81-bc32-9c2234084ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7849
        },
        {
            "id": "f7bf4ed6-1d26-4f02-bc06-588414c65206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7850
        },
        {
            "id": "8bf3d763-b33e-4140-8e0f-38772fa129d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7851
        },
        {
            "id": "9eddc7df-9cd3-4d26-82b3-291d9f035a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7852
        },
        {
            "id": "336c02e9-198f-4b6c-9c73-75c7aa685530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7853
        },
        {
            "id": "52fd435a-a8c5-4380-962a-511442ac76d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7854
        },
        {
            "id": "b363097c-9548-41e3-8355-621832242b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7855
        },
        {
            "id": "50c60f9c-22f0-4314-83b7-ea3256a43508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7856
        },
        {
            "id": "b77f9d69-aa67-4995-9387-e4f7b37a4891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7857
        },
        {
            "id": "a098fb33-7238-4f8d-9545-1b389785f1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7858
        },
        {
            "id": "613d1271-1cf6-454f-b622-7508652bb51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7859
        },
        {
            "id": "fcd86498-264c-44ed-968a-160b88a1be62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7860
        },
        {
            "id": "6cf232a4-3e13-4c0a-b067-7fe8841e2b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7861
        },
        {
            "id": "99cf8849-0d20-47ec-949e-781cd57909b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7862
        },
        {
            "id": "54ed8189-80d3-4c29-8a3c-3e08763dae89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7863
        },
        {
            "id": "828279f4-b160-4e13-844d-952053021148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7865
        },
        {
            "id": "05918c92-2767-43b0-8455-7025ab579a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7867
        },
        {
            "id": "f8eb000e-8b2f-422f-a217-1d29f5e15ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "7fb533c3-2e1e-4eee-aa6b-4778d37f4bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "73ea86f8-8742-4ebd-9018-0f9088f5b0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "ffaa3ea6-2090-4661-ad20-fc5b374eb855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "8d204c0b-ccd9-4a42-9eba-d230f6c80e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "9e0ba1b0-2144-4442-ace5-db0d723aa889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7884
        },
        {
            "id": "fd153009-15db-4717-88db-3fd13939ca3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7885
        },
        {
            "id": "6f50045c-103e-4ed5-bb44-a4a21b76a497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7886
        },
        {
            "id": "88a64cef-b610-4077-9f94-bc71d2a8d259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7887
        },
        {
            "id": "e1d28d9e-604e-43dd-86c3-3978b405dd9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7888
        },
        {
            "id": "3a8f0eae-3211-423f-992f-7ec3a3191f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7889
        },
        {
            "id": "2ac44df8-ef4d-4336-84e2-a88f6d13f3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7890
        },
        {
            "id": "a033aef2-eeb6-4751-a35a-e63a777e3e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7892
        },
        {
            "id": "33dc2a77-64dd-40fe-ab0b-6ed73ef5b897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7893
        },
        {
            "id": "23bcd6af-0ea3-4b97-8f25-05f78912597b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7894
        },
        {
            "id": "55ce7087-8839-4423-a42b-8ea550951c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7895
        },
        {
            "id": "530701a5-0465-4153-930f-fb4958c15df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7896
        },
        {
            "id": "d1d72fe2-53a0-4df7-800f-cad8a06dbf94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7897
        },
        {
            "id": "1537ede0-126d-48c8-bbe3-5bfd0c4ea951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7898
        },
        {
            "id": "4bde0334-15a0-4ac5-8792-14a23326dba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7899
        },
        {
            "id": "feb94b6c-3bc0-447c-8281-d5408045134d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7900
        },
        {
            "id": "03932e59-4e64-4b09-b04d-456263dc1533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7901
        },
        {
            "id": "5d679363-d059-4c47-a470-dcd662c55d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7902
        },
        {
            "id": "dd9f39bf-5e1a-48ac-ba4d-f9c609e76953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7903
        },
        {
            "id": "20c8d240-acd1-4733-a17a-7ce4b61ee249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7904
        },
        {
            "id": "3b4e9035-83e5-470c-bc75-70aac01f5370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7905
        },
        {
            "id": "6cffc487-e279-48b7-9ce5-3720a741b59d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7906
        },
        {
            "id": "c6eaaad1-f1e0-4297-b2b4-1dae591f295f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7907
        },
        {
            "id": "282dbe98-1dfa-4c78-85bc-6c040c8ae8d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7909
        },
        {
            "id": "24379bf8-422a-4824-a451-c0b9417c76d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7911
        },
        {
            "id": "79fef489-35e3-41b5-b488-e60960895966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7913
        },
        {
            "id": "c845915a-445c-40ba-a1fc-139c1dd057f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7915
        },
        {
            "id": "64e61712-a055-449d-9608-b965c7218241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7917
        },
        {
            "id": "c3c0ac2b-0db5-407e-bc53-59ef7599c276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7919
        },
        {
            "id": "30f31903-3eb3-42b6-a569-0669bd6f74b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7921
        },
        {
            "id": "c63d51a7-5d84-4be2-ac59-f3f602ad38c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8208
        },
        {
            "id": "2b1e2fd8-6307-4433-80e1-b1727b4732af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8211
        },
        {
            "id": "f1c1d50b-c767-4666-997f-acb44bc2720a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8212
        },
        {
            "id": "e10a6697-da51-4d7f-ac75-6c19c2db3808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8213
        },
        {
            "id": "515eb00b-c565-419d-9527-7739882cf035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 8216
        },
        {
            "id": "8489cf6a-4676-4400-8d8b-8d399071ab74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 8217
        },
        {
            "id": "0aaf15fb-d020-4dbb-8fbf-35f5a38ffa65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 8220
        },
        {
            "id": "5b5986b3-9a5f-4126-88f6-7b55380ddf35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 8221
        },
        {
            "id": "33544a8b-c5fa-4985-9b7a-ed56e108d09b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8230
        },
        {
            "id": "03de828d-9b11-44cb-be4b-af96012ff2a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8249
        },
        {
            "id": "43c58d03-95b8-44f4-8e38-a12ca4728f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8250
        },
        {
            "id": "3106ff26-962c-4777-955f-a3cedd853eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "811a7f51-386d-4990-9d02-02deec2b75e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "46dc5f1d-261c-4b59-95af-f45bc9375188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "ff6042e1-3102-47d8-b85d-19d2aaa4bf56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "e9ddcd07-c361-43fd-aa33-fdc5e449d7d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "94261590-9ef4-4ec4-bc9a-b46122893384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "8cfd7feb-db68-4d61-b133-f884a922d3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 173
        },
        {
            "id": "cd528d06-fb28-4bc5-bd5a-a8a1069d9a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "51ed747d-e0be-4280-b156-a9e1c06270ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "99d426f8-9217-4f71-ba86-0d85177b40e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "c775384f-7682-471a-801d-a8e149bcc07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "d98d2a4b-a789-456b-bcaf-5985de4b08c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "ecb93583-4b52-4e60-8533-7481af84e87f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "036380e6-a455-4420-85c5-4ce934c62bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "8dab0893-d811-4f91-9717-765cc0e3e2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 253
        },
        {
            "id": "40ef697a-6c50-4b62-9ba8-25aadc4c0082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 255
        },
        {
            "id": "c2e3c7e3-af43-47df-93c9-46c4d68ab75d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "a361d4dc-6527-460d-8071-2e1bc87fb315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "31a1e3dd-5b7d-4a95-a5e6-a64819a47299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "3ddaca8b-3576-458e-9cb1-c67c5e1669f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "da245d4f-6d55-4e73-9b87-fbe603ac0d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "11e6bfc3-4088-4da9-bfeb-d576e966b2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "5e34f2a6-7d98-44c4-9132-2cc5acb4234f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "8b1e52df-564a-459a-989e-73e61fecbf5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "f24fb64d-be9c-4cfd-9e38-d8002265ed8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "2e0f78bd-6f9a-41cf-9e04-47d14b1fedd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "d367ff55-0689-47b2-9459-19f7c8264676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "94629c8d-1f17-412a-81ba-ba11157fd788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "606b9ed5-541d-4070-b360-a001aa42d955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 375
        },
        {
            "id": "71748b36-3c43-496a-8ea2-005c304c0458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 416
        },
        {
            "id": "279536e8-a9d9-4a4a-a69f-478211c26ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "96f4ea56-aeae-4251-b785-f6b530bc1f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7884
        },
        {
            "id": "ef7655ed-2434-4481-8257-d548fb343e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7886
        },
        {
            "id": "19c8ba87-f4a3-41fc-9703-057275312acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7888
        },
        {
            "id": "29ba8f23-eb3a-4cd4-84f7-1e6d7139098a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7890
        },
        {
            "id": "3d687a15-bb02-4dd8-81c0-36db31302ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7892
        },
        {
            "id": "ef19f791-519d-4375-b3f6-429169b539ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7894
        },
        {
            "id": "c098c8fb-6f82-424a-b864-2b95658006ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7896
        },
        {
            "id": "e66ff027-0813-4f5e-a936-59a2d66558f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7898
        },
        {
            "id": "65f1368d-8fff-4653-a735-16ce74499707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7900
        },
        {
            "id": "a12c64fe-5b63-4b00-92b2-e4d8e87b14f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7902
        },
        {
            "id": "9a943115-8bb7-4785-a81a-73ab811f4161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7904
        },
        {
            "id": "155087fc-0e4a-4001-9cc1-21011e72543c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7906
        },
        {
            "id": "417bc054-4493-41d0-92b1-dcd8459d94d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7923
        },
        {
            "id": "1b888816-98cd-43e5-8d09-4d2e315e0e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7925
        },
        {
            "id": "6c1f6df3-4bf4-4460-bde3-a464de081061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7927
        },
        {
            "id": "f8f4f36b-822f-48ca-9c35-f3c86186e4c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7929
        },
        {
            "id": "651cde3c-98ad-484b-b514-db52c1fd2ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8208
        },
        {
            "id": "97d0f5ff-22ad-47cc-8537-9e9e255d9903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8211
        },
        {
            "id": "04919f4e-c3e6-4c2b-84e3-0f658b5e5947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8212
        },
        {
            "id": "71f45695-a16f-4b87-8cb5-571cdc256475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8213
        },
        {
            "id": "5ba11cd4-2b5d-4aaf-ab16-20b4a7234b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "ba7f6868-5bed-4e80-916d-556d677e498d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "24bebd10-d36a-40bd-a9d3-bd32e056880c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "8bbbd841-8d2e-4f02-8d7d-36f12a1deba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "96baf8f3-5ecd-4790-98d8-410217f75d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "d67d4af9-095f-41ca-8e38-57500bee7f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "30b1dca3-9925-4213-8af2-17cc54b737a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "4306a883-2965-48e1-b2e0-6512ba8e93fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "6d26a1cc-dc1a-4903-af61-478f99317136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "5e182cbf-09d1-42af-89f7-50907fbabb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "1761efb6-daea-4ddc-af40-5e703e3dd0a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "98ec4ac1-e63c-4bd4-9bec-be1e7e82b1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "65af6859-f259-440b-89a2-d7b1877fdbaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "9283594b-e4d1-43e9-80dc-f19ea769fa9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "9adf9f6f-286a-4e54-b5e3-1927a7b4cd7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "9a2b4fb4-d952-40ce-bec5-68fdb8ab0747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "f79747ae-9311-4a05-8f4a-a782ce3c4377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "45a0347d-1cc8-4400-b377-1853e5773697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "23d4a22e-1224-47e0-ac8b-c8cbc37976f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "34419139-c65e-44a7-8b2f-11142cda4f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "04ca735d-109c-4483-a739-802161191977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "d364a153-2268-4fa2-bc8f-4180cdf66aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 116
        },
        {
            "id": "6e1371a0-f3c6-4ebe-9a01-945daf24d25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "a60efe25-2505-46eb-bda1-cba9b9ba85d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "5f0affa9-2750-4896-8850-e196baf490fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "e6460015-4aba-425f-ac83-b0bab7fd19ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "81fe8eb9-8173-441a-bcd9-11e0d43d91be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "9f333237-69ba-46e3-998b-436bf82ab24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 187
        },
        {
            "id": "7b4f2034-a17d-470e-b63c-eb7e3c89e979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "2fcd694b-bf63-42f1-87ac-1c3b784348e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "5a28aa30-5c4e-48b6-ae81-10caa3cb25b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "178e4a1c-472a-4d6c-8e16-639c06811601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "73c20c27-e232-4dcd-9832-a91b1b4e3347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "47ec5f9c-d1ba-43a4-bff4-9eae0695304f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "39f8416e-3fd7-4719-964a-c5294c1a8722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 198
        },
        {
            "id": "8996397e-7c21-4563-ab3c-4782e5e42894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "712e47a3-6711-406e-8d3d-06604ed23ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "7291e261-bd07-406e-92f4-63111468ca59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "69a31762-5f68-42cd-b7b3-934c86088987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "c4887465-aa17-4d96-ade4-9e8dd991079e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "c49a6b2d-a380-4d71-bd9c-5f689ff6c5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "cd363a21-61b5-4eba-826e-32c341d8039a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "7361e22b-7c5d-4bbf-beb4-b329b66fdc56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "ed1380a8-3e3f-4d85-9927-3ef38f31aac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "f7e32ea9-674d-4d9e-b969-7457767b2f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "42998ea3-34ff-483b-84db-7807cea1ed42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "8248620d-29fd-4bde-8411-dabdef8f8764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "88832517-bcf5-4da2-9720-7341433d8c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "cb21fc24-af78-415b-a892-1bf241b80205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "352f5938-44cf-498a-be30-cbf521021365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 231
        },
        {
            "id": "8e897596-7f79-4c77-b230-5fd93c1be9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "aedabb20-2d45-4c1a-aed0-8c6ee8e925fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "187a0036-bb1c-4610-a051-003603b8cc7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "c3a287ee-f666-4ffc-824e-e3638214fbc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "ade48e49-cc16-4059-8c54-0a4fd12d2a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 236
        },
        {
            "id": "28ccfb4a-218c-4d2c-a807-8ae7863a5fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 237
        },
        {
            "id": "f470eaee-97e3-4d43-a6ae-be4425a19973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 238
        },
        {
            "id": "4d6344f9-7fa4-4262-beaa-93a9eb575499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 239
        },
        {
            "id": "fcf334f3-077f-4e9c-a017-eee8c443ba8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "7948d10d-755d-4383-bea9-78b8832b57d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "0a3dde03-5b0d-4c8a-8d33-90972411d8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 244
        },
        {
            "id": "e8218d31-f29e-4280-b044-329cbcb58234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "a33ddb40-40a8-4b05-b6b7-586d2e3b2a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "c17328f7-8131-4ecc-aec5-97e8b87e1db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 248
        },
        {
            "id": "ba2f9faa-ef3e-4e24-b886-e37600f0d964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "ffb3a715-be84-41e6-bf3a-ab92ae4cacad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "ba32047a-9f86-4096-98e5-cfd31554e71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "454ecaa6-4a0a-4d50-afb2-6486ed276cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "e5918064-2216-4cf0-b505-e204b4f2f3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "a9d86a42-bb29-4786-99d8-d93f1b1a265d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "c391fb01-3e93-48a6-b40c-bfb5f4bdd258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "aad5596c-3c09-440b-9998-5900a07e116d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "144db2fe-2ec6-401f-a402-987f74a848f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "a36d2e0f-1861-401c-8359-7477e639eee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "7b7314ac-6196-4a93-a160-098b0d3c9e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "562f730f-3421-4dad-9cf4-dee200d7dcc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "55f75a63-cd04-4a0c-a6af-2dd73fc64930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "84d1e3a9-566d-4c41-b05a-59653b682073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 266
        },
        {
            "id": "dbe36825-5042-4e14-8727-1e39661e49e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 267
        },
        {
            "id": "ad944a94-0f41-4bcd-b6e8-2aa0dac51c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "5e4f917a-62f4-4ebb-805c-0d83e599a684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "6ebf04f4-2e86-4f23-aabd-92295fef5f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 271
        },
        {
            "id": "62fda661-e9a6-4029-a500-2645819a9bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 273
        },
        {
            "id": "c5798362-27cd-4bfd-8dac-9d1455d749fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "e1845ace-3d71-4ee6-a977-8ca5ea4c6706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "51dad56c-b176-4530-a551-9f33286ceeae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "2b695056-07e5-4939-a161-f06b26dccc29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "f8c7eb4f-a11e-4509-8137-953ea4608b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "6cfd998a-6e81-4966-9c86-0742b8285831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "50faff85-7a06-4aa1-935a-71aaa6aaa9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "65c3194a-82b5-4cbe-b1ed-fa815e9e332e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "e2bee31f-73c8-482b-ae8d-4256d7fdc64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "f00885b8-0169-4847-8768-6ca8228c0f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 297
        },
        {
            "id": "4c6376a9-aa46-460e-aa27-8b3dc743f44b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "aa4cf7ec-6a35-4b77-90fd-927fc0a3477d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 301
        },
        {
            "id": "9c105161-1d05-4b97-9c06-0f563995609b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 305
        },
        {
            "id": "8105c105-595d-4fc4-b76d-4518faaf8273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 329
        },
        {
            "id": "9c6f4f43-5558-44ef-a7b7-f3c0a1d9bca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "419034c0-6dc4-4339-aa02-9ad69e7176b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "e764f7c8-daaf-419c-a581-767edf47681e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "a8d9ae4f-4646-46fa-a742-ec12c0c28063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "fc7b5337-aedd-4e88-9d3e-0e9e7a0f0e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "0f831aa1-cb7c-469b-84f9-aed0553b6f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "63893e80-d313-4ae1-933b-21da7903c3ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "689aa9b4-62f3-402b-9692-fa7b93c349e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 339
        },
        {
            "id": "1e039d79-6624-4470-9eb7-dcbc4f34e37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "8575ae67-ca9b-496e-9fa4-ed6dcfc40673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "4538878a-6904-454f-b24b-219e623e82a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "069e20d6-56ca-4a1b-ae98-4841083a2a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 349
        },
        {
            "id": "2a2bdaa9-41ef-4916-ad6e-054c1d4b0459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "ff6394f5-ddc9-4771-9ab8-bf5314b8efd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 354
        },
        {
            "id": "eccdde6d-4b86-4c4e-ac8d-57c2e51daa35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 355
        },
        {
            "id": "4d66657e-663b-4f77-9995-4e5c274d68c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 356
        },
        {
            "id": "bba8c780-cf76-4122-a20d-c4115c841a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 357
        },
        {
            "id": "6325316c-3430-4ac2-9156-26bae0349e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 358
        },
        {
            "id": "483d0a4c-7488-4413-8797-2ebf46449b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 359
        },
        {
            "id": "c5b00d38-0753-42a3-b6c2-dc3f8bef0c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "91a1be9d-f94e-4822-9c06-4b5d7017e103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "77c9e0c3-ff4c-4a4b-9fc5-469fae7041b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "6bd149a0-2da1-47ef-9e27-63d35b3c1eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "e844cdeb-157a-4782-9230-ec27889d8f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "582f2efe-8d0f-4eb3-8e6c-4dc79f5dd1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "a617cd15-b78b-4f4e-9884-b579c45a0ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 416
        },
        {
            "id": "3e0d81ec-2475-48d6-935b-2ed5507a12d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 417
        },
        {
            "id": "aaf47332-6906-4b39-a8b1-af9a650f52e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 432
        },
        {
            "id": "5955d573-1136-4400-9fa2-04192b14f150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 506
        },
        {
            "id": "a91000b1-8ebc-41c8-8c4b-85aff56c0240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "1d92742e-9233-49cb-a010-6930819c0fc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 508
        },
        {
            "id": "79eedad7-511e-4a44-bc30-739029ffeea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "864b3a73-6107-470c-9393-3d7ce4430708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "d02e7194-1329-4317-8876-b4a556cadb04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "25193a8c-1069-4784-b2ce-0d2f7b3bb85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "0eddeed5-dcd5-458f-82f6-a42c983af008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 538
        },
        {
            "id": "6875b6b0-4c47-4630-8250-8042a2b8c6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 539
        },
        {
            "id": "0de6646a-0aa9-47ea-972d-11d16b1767c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7840
        },
        {
            "id": "0be420a8-a5c4-406d-a599-99134eebf281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7841
        },
        {
            "id": "7ad69237-fbd6-422b-9419-82c6ec433fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7842
        },
        {
            "id": "a04e8739-184e-4634-bbb3-3218f7f5b49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7843
        },
        {
            "id": "712a2396-8ac2-4125-8798-c41af4db39d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7844
        },
        {
            "id": "dca32c5d-ee34-49c3-aa47-8a2efe295119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7845
        },
        {
            "id": "d26f2052-b34d-4280-91ef-0960f0b96285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7846
        },
        {
            "id": "ecc6feae-8cc6-4aae-9881-f8a2e7b800c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7848
        },
        {
            "id": "6b23f87a-c7be-454c-af51-1d6c4c6aeedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7849
        },
        {
            "id": "d30d7cb5-e5cc-4aab-ad09-95c10af928e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7850
        },
        {
            "id": "3e60bf0e-cd46-470a-b4f9-62932d757d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7851
        },
        {
            "id": "df149f46-222a-45cb-ab65-ce99ef5f5e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7852
        },
        {
            "id": "ce31c824-6687-4ae6-aa94-eb8ede5a54be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7853
        },
        {
            "id": "b5a110e3-d651-4811-8844-6f6d446bbbbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7854
        },
        {
            "id": "3fe18a00-e27a-49fe-ac25-fd33fa571425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7856
        },
        {
            "id": "c464ad71-8a1e-437f-a3c8-dd2f24c1cd7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7858
        },
        {
            "id": "dbd83be2-adff-4d72-811a-49cb70d8bfea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7860
        },
        {
            "id": "8a9f0975-a277-40a2-9174-be63d58b55fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7862
        },
        {
            "id": "e09436d6-d2cf-414a-a0df-9595c6ba3862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7865
        },
        {
            "id": "f355f900-6eb5-43cf-8a7b-9c629f323be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7867
        },
        {
            "id": "d65ddd74-73b4-4820-8b06-62867c0434ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "c3fc35a0-dd86-4e6e-993c-8fcfb497864b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7871
        },
        {
            "id": "2125bd25-c03a-4651-82ce-c525fbe99246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7875
        },
        {
            "id": "ebf7264c-7c43-412f-9050-78b6c0f547b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7877
        },
        {
            "id": "5796c556-014f-4964-a6ad-2d3fe51fb8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7879
        },
        {
            "id": "a14aeb66-b840-4281-9813-8065ffabca16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7881
        },
        {
            "id": "f4b33727-8218-487f-bc92-2badde0917e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7884
        },
        {
            "id": "d33f03a3-d4d1-4feb-aab7-3fa9f352d382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7885
        },
        {
            "id": "7765c1c8-67f8-4c01-b976-0d0491ebb405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7886
        },
        {
            "id": "6ffa384a-6a08-4011-808f-983f4f03bf70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7887
        },
        {
            "id": "63b8de56-bd3a-4acf-92c2-f554685fcf39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7888
        },
        {
            "id": "ee4a7a0f-692a-4b8c-86b2-4b8a8bdb8935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7889
        },
        {
            "id": "21fa5c2e-e36d-4506-b237-f1a356a982db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7890
        },
        {
            "id": "2fbe4191-62c9-48ef-b724-8db13f09a9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7892
        },
        {
            "id": "d28e5a4d-800e-47b0-a081-56b1817d2bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7893
        },
        {
            "id": "e5f61549-15ee-4ae4-a287-8aa54be0ae48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7894
        },
        {
            "id": "5e2c2133-02c1-4467-bfc6-b67984b3ffe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7895
        },
        {
            "id": "c3f8db31-acbc-48ce-a00d-b46058258932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7896
        },
        {
            "id": "889f23ee-937e-4165-910e-e54c12854974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7897
        },
        {
            "id": "995f1457-633d-41a0-8efc-9c48d96dcf80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7898
        },
        {
            "id": "c822d811-aafd-465e-bca3-dddccf42ece5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7899
        },
        {
            "id": "d2a596d2-f864-4c5f-82b1-f06fa65b49c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7900
        },
        {
            "id": "da636eb9-de01-4a86-ba33-1232a37b7b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7901
        },
        {
            "id": "50684b72-85a9-4eb6-98de-a438a8046754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7902
        },
        {
            "id": "c0351380-f196-4d90-9157-e78d2c3a7cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7903
        },
        {
            "id": "e6d97527-6044-48b2-9371-086ef2728e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7904
        },
        {
            "id": "dc69bc39-dae4-4eb8-89b4-8f26c5667abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7905
        },
        {
            "id": "0328a9e4-9153-4b95-aa86-efafeab9cec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7906
        },
        {
            "id": "4a3277a0-81ec-4252-adc4-58050ab54121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7907
        },
        {
            "id": "f9fe748e-ebeb-4a17-9997-db901249bb6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7909
        },
        {
            "id": "0da31a1c-b8b6-44b9-9bf3-4b2f667f30cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7911
        },
        {
            "id": "55221f0c-76a0-45d5-9e84-173a3ba60064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7913
        },
        {
            "id": "3c021e24-c1dc-4d82-be68-a162bf609b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7915
        },
        {
            "id": "32336e15-9c42-4f0b-90b6-003e57b88e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7917
        },
        {
            "id": "9a3a92f0-4806-43e6-9a6e-c7106cc2dc72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7919
        },
        {
            "id": "b9f94ce4-dbe3-406d-9fb0-0a07eb831e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7921
        },
        {
            "id": "24b21b63-7129-4601-8f65-3ec842e31853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "c2d3f9b4-d9c7-4ff7-a71b-51996450f1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "2d4c8dc1-dc8c-41cf-b04a-aba6394bd5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "8dfb9e14-8d5b-4002-883a-fb19dc894edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8213
        },
        {
            "id": "da6bf9bd-5a73-4409-b90a-d53d983075ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 8216
        },
        {
            "id": "a29d6f5a-e267-4973-bea9-ee2457cad7b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 8217
        },
        {
            "id": "ac23e06c-f5c4-4219-abd5-91e09635ac83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 8220
        },
        {
            "id": "33a12477-f5a0-483c-8dde-a6d318ab2939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 8221
        },
        {
            "id": "6d965c24-2224-45b7-8b4d-bcb4fc118856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "d72bbe55-055e-4507-b4eb-9f800b2fc696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8249
        },
        {
            "id": "e2201912-7214-4894-849b-4eadd840e87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8250
        },
        {
            "id": "d1902354-b724-43b9-bd7a-6543bc254f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "adf26639-9b32-4313-9ef3-3879ffd1379c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "60c82974-d058-4fd8-815c-a649d12292a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 253
        },
        {
            "id": "1be403f2-d102-4da4-acca-7ac95fad469e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 255
        },
        {
            "id": "6241e723-ee1b-4d8e-bf0a-464b4e7ea928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 373
        },
        {
            "id": "72b6e16b-478d-45ae-9429-91c4da2c3b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 375
        },
        {
            "id": "789522c4-6dca-4726-b30b-b15e67c360d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7809
        },
        {
            "id": "ad1d14b8-a362-4305-8f40-149ef2f43442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7811
        },
        {
            "id": "279355f2-d484-463f-907c-6e9450d6d24a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7813
        },
        {
            "id": "a90b2d30-e311-4263-b240-8d263568c76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7923
        },
        {
            "id": "88ab5289-43b3-436d-bad1-2eafb73c938a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7925
        },
        {
            "id": "7e4d0a0a-7528-4247-a700-33acb813ce00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7927
        },
        {
            "id": "7a5da577-ac15-41d5-ac4c-a2022c2ccd07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7929
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}