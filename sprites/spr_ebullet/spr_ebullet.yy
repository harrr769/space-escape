{
    "id": "25b8156b-2ee3-47c8-8ae6-8deaa3f5e671",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ebullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c85c719-ea72-4f2c-ad9f-68b5390fd501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25b8156b-2ee3-47c8-8ae6-8deaa3f5e671",
            "compositeImage": {
                "id": "2bfd6a73-2373-4009-8eb6-bc11a7bcc437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c85c719-ea72-4f2c-ad9f-68b5390fd501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddea1dbe-9fd2-4212-a3c0-ef9610bc6bf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c85c719-ea72-4f2c-ad9f-68b5390fd501",
                    "LayerId": "301cd3bb-4480-4ade-a4dd-d339324501a7"
                }
            ]
        },
        {
            "id": "c2b0e305-5a36-401b-ae34-056a165c2c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25b8156b-2ee3-47c8-8ae6-8deaa3f5e671",
            "compositeImage": {
                "id": "05026159-c604-4ddf-9480-7d74a5e3c455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b0e305-5a36-401b-ae34-056a165c2c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46375d30-54da-4947-9a0c-ca0eab05afe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b0e305-5a36-401b-ae34-056a165c2c5a",
                    "LayerId": "301cd3bb-4480-4ade-a4dd-d339324501a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "301cd3bb-4480-4ade-a4dd-d339324501a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25b8156b-2ee3-47c8-8ae6-8deaa3f5e671",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}