{
    "id": "33041efb-e6c3-4838-bc20-12cf455c7c67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 224,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a649a145-04cd-4566-8d45-c7c4ef32a9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33041efb-e6c3-4838-bc20-12cf455c7c67",
            "compositeImage": {
                "id": "f74734b3-d1aa-4fb5-a78b-0ce09e03524c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a649a145-04cd-4566-8d45-c7c4ef32a9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86b9b24-810b-4aac-b2ac-90504b62eb99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a649a145-04cd-4566-8d45-c7c4ef32a9b0",
                    "LayerId": "12f09f45-a1de-430a-aae4-4b10e27c64e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "12f09f45-a1de-430a-aae4-4b10e27c64e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33041efb-e6c3-4838-bc20-12cf455c7c67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 225,
    "xorig": 0,
    "yorig": 0
}