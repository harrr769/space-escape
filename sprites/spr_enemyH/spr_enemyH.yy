{
    "id": "ba0749c3-5da9-4ed6-ba7b-9cc818828d50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyH",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 9,
    "bbox_right": 38,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2565b63e-f979-4d35-bb12-c21b3a1ae70a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba0749c3-5da9-4ed6-ba7b-9cc818828d50",
            "compositeImage": {
                "id": "f920c63e-ecd5-431f-b502-a6aa40150657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2565b63e-f979-4d35-bb12-c21b3a1ae70a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e7fe39f-bdde-4531-87fd-100bed57bc94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2565b63e-f979-4d35-bb12-c21b3a1ae70a",
                    "LayerId": "5c525225-cf68-4cfc-95e2-54f5fc25c9bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5c525225-cf68-4cfc-95e2-54f5fc25c9bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba0749c3-5da9-4ed6-ba7b-9cc818828d50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}