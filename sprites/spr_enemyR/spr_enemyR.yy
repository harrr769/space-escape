{
    "id": "e6d7e06d-97ca-4113-ae15-ad2dc65819c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 9,
    "bbox_right": 38,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b39de93-2f11-4f9a-b6cf-4b97aed6d330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6d7e06d-97ca-4113-ae15-ad2dc65819c1",
            "compositeImage": {
                "id": "92fdb77f-64fb-412b-83b2-90264ad5fbd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b39de93-2f11-4f9a-b6cf-4b97aed6d330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62584761-ded4-4730-9a5c-6867df3b384d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b39de93-2f11-4f9a-b6cf-4b97aed6d330",
                    "LayerId": "daafae95-47d2-4c07-91dd-17e07a5f4f39"
                }
            ]
        },
        {
            "id": "55e6d76e-c3d4-41db-a23d-c85c27047207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6d7e06d-97ca-4113-ae15-ad2dc65819c1",
            "compositeImage": {
                "id": "375f364b-2e5f-404f-8e65-d791493d9c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e6d76e-c3d4-41db-a23d-c85c27047207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e191d3e9-8fcf-46d3-913c-c5fcae4ab50e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e6d76e-c3d4-41db-a23d-c85c27047207",
                    "LayerId": "daafae95-47d2-4c07-91dd-17e07a5f4f39"
                }
            ]
        },
        {
            "id": "a00c7e53-6a84-4783-b0d6-c5608b4db827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6d7e06d-97ca-4113-ae15-ad2dc65819c1",
            "compositeImage": {
                "id": "160c471a-5fd3-456d-9809-5045082c70e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00c7e53-6a84-4783-b0d6-c5608b4db827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb283b9e-a63b-405b-b0b9-1360cc40e9e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00c7e53-6a84-4783-b0d6-c5608b4db827",
                    "LayerId": "daafae95-47d2-4c07-91dd-17e07a5f4f39"
                }
            ]
        },
        {
            "id": "1d553616-0dd8-4b22-9958-e1ccff6926af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6d7e06d-97ca-4113-ae15-ad2dc65819c1",
            "compositeImage": {
                "id": "9f9087d6-c1d2-4f3e-90bf-18907bb7c9c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d553616-0dd8-4b22-9958-e1ccff6926af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c587bf5e-49e7-4329-bb02-e20b3d3d1a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d553616-0dd8-4b22-9958-e1ccff6926af",
                    "LayerId": "daafae95-47d2-4c07-91dd-17e07a5f4f39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "daafae95-47d2-4c07-91dd-17e07a5f4f39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6d7e06d-97ca-4113-ae15-ad2dc65819c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}