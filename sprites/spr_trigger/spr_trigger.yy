{
    "id": "17f3a276-5af4-4dde-9ac9-b5dd9130ca26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7daa9c5-44c0-4554-bafb-5e58089f26e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f3a276-5af4-4dde-9ac9-b5dd9130ca26",
            "compositeImage": {
                "id": "70970778-b8f0-4e06-ba80-31b9c64a409a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7daa9c5-44c0-4554-bafb-5e58089f26e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b6bfe5a-68fc-4b9e-a38b-3677c4e5ece2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7daa9c5-44c0-4554-bafb-5e58089f26e0",
                    "LayerId": "7f35cf8f-ec53-43a7-bce0-3bd28063466c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7f35cf8f-ec53-43a7-bce0-3bd28063466c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17f3a276-5af4-4dde-9ac9-b5dd9130ca26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 48
}