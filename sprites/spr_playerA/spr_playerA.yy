{
    "id": "0278fa7c-2045-4388-aa7e-3f475d7bf8fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 21,
    "bbox_right": 27,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a99a7ff-0d22-46a6-acfc-14e416b8d114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0278fa7c-2045-4388-aa7e-3f475d7bf8fc",
            "compositeImage": {
                "id": "b17af355-0d49-435d-b22c-be3bf12d53bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a99a7ff-0d22-46a6-acfc-14e416b8d114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa7984d1-a3fb-42ff-b8e4-458a47b9614e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a99a7ff-0d22-46a6-acfc-14e416b8d114",
                    "LayerId": "6204cacb-7e51-48a7-ac2c-f0637066f992"
                }
            ]
        },
        {
            "id": "06df0923-4c9b-40e0-a7cb-06663bc52343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0278fa7c-2045-4388-aa7e-3f475d7bf8fc",
            "compositeImage": {
                "id": "babb6820-5c03-45ab-8c2f-b8f8bf8ea0d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06df0923-4c9b-40e0-a7cb-06663bc52343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b04fd251-aa20-4f39-8d00-31813593f9da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06df0923-4c9b-40e0-a7cb-06663bc52343",
                    "LayerId": "6204cacb-7e51-48a7-ac2c-f0637066f992"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6204cacb-7e51-48a7-ac2c-f0637066f992",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0278fa7c-2045-4388-aa7e-3f475d7bf8fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}