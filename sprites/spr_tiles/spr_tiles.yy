{
    "id": "62b82d52-6069-4e39-b748-6a44c3655507",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1f1d8b6-6b36-4b30-94bf-bca414f31d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62b82d52-6069-4e39-b748-6a44c3655507",
            "compositeImage": {
                "id": "bde31834-3c93-45af-bf33-3f92e90c4878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f1d8b6-6b36-4b30-94bf-bca414f31d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c6fd71f-24b7-4b45-864f-7ed356b2904b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f1d8b6-6b36-4b30-94bf-bca414f31d3e",
                    "LayerId": "346e6904-f0a0-4257-89f7-c1cf1df77236"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "346e6904-f0a0-4257-89f7-c1cf1df77236",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62b82d52-6069-4e39-b748-6a44c3655507",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 96
}