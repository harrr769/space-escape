{
    "id": "3be78492-13a6-41f0-b896-c4837f756fdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06890fc9-30af-4787-9046-b536aa39ce32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be78492-13a6-41f0-b896-c4837f756fdd",
            "compositeImage": {
                "id": "cff971c1-a62c-4a1d-9acf-b0591dd72a9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06890fc9-30af-4787-9046-b536aa39ce32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50df020e-2168-44df-a895-4402b54e6ac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06890fc9-30af-4787-9046-b536aa39ce32",
                    "LayerId": "5c7d7ef8-7a44-401f-b28e-f13cb60f75ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "5c7d7ef8-7a44-401f-b28e-f13cb60f75ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3be78492-13a6-41f0-b896-c4837f756fdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 10,
    "yorig": 4
}