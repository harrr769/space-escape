{
    "id": "85a80af4-c801-447f-a2fb-6e11f747da75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5702f139-0464-4487-85ea-0a60c03bb39f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a80af4-c801-447f-a2fb-6e11f747da75",
            "compositeImage": {
                "id": "e15c28b4-7d62-4785-a6bd-5c9971782dad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5702f139-0464-4487-85ea-0a60c03bb39f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bdac27b-a5d2-4dbb-98b5-0601b5a1db6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5702f139-0464-4487-85ea-0a60c03bb39f",
                    "LayerId": "e30eb44f-f63f-450d-86a2-248a04569f71"
                }
            ]
        },
        {
            "id": "e28c15ed-8777-4dd3-aaab-0eb522f9d125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a80af4-c801-447f-a2fb-6e11f747da75",
            "compositeImage": {
                "id": "d2f37608-c934-4dc1-bb03-8a99137c4fae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e28c15ed-8777-4dd3-aaab-0eb522f9d125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "302bed1e-da56-49c4-9652-41d7e18f5865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28c15ed-8777-4dd3-aaab-0eb522f9d125",
                    "LayerId": "e30eb44f-f63f-450d-86a2-248a04569f71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "e30eb44f-f63f-450d-86a2-248a04569f71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85a80af4-c801-447f-a2fb-6e11f747da75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}